before we can use this application make sure you have nodejs and mongodb installed in computer.
run mongodb if needed --config /usr/local/etc/mongod.conf
open terminal and type
step:
1. mongo
2. use brgydata
3. db.dropDatabase()

open the root directory and open your terminal.

/ICES_BETA_VERSION3

copy and paste the command line below into your terminal.

mongorestore -d brgydata dump/brgydata

->this will import all your data from brgydata database.

To start the Server:
run the server: node server or npm start.


open your googlechrome browser.

copy this url: localhost:7777

=============================
username: admin@cvisnet.com
password: admin
=============================

Choose the REGION for type of access and login.

Done!

 To export mongo database follow the command line below:
 mongodump -d <our database name> -o <directory_backup>

Meaning of ICeS

A Cloud-Based Integrated Community e-System for the Barangay (ICeS), at its most basic, must have a Resident Clearance, Business Clearance and Residents Profile.  An LGU integrated system will allow data sharing between barangays.

Barangay Clearance Application System - allows the storage, cross-reference & retrieval of data for various purpose (business license, employment, travel, etc.).

Judicial System - automates the daily operation of the Lupong Tagapamayapa or the barangay justice system and maintains a wide snaring of information, including patterns of domestic violence and protection orders.

Integrated Residents Profile - allows the profiling of individuals and households for the purpose of easier attainment of household-directed government services, better disaster management, keeping peace and order and easier matching between locally available skills and employment opportunities.

Online Barangay ID - using a smart phone, an online ID showing the person's name, address, picture and captured signature.

LGU Integration - integration of barangay data and services at LGU level, this also allows sharing in-between barangay information
  - Municipal Residents Information System
  - Integrated database for all barangays
  - Municipal Wide Residents profile system
