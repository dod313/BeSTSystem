var express 		= require('express'),
	bodyparser  	= require('body-parser'),
	cors 		= require('cors'),
	bourne 	= require('bourne'),
	session 	= require('express-session'),
	mongo 	= require('./dbconnection/dbconnect'),
	db 		= mongo.dbbestconnect();
	router 		= express.Router(),
	uuid            	= require('node-uuid'),
	methodOverride = require('method-override'),
	residentviews = require('./modelfunctions/residentviews'),
	shortid 	   	= require('shortid');


	// get only the selected items.
	function filterthis(data){

		var array = data.map(function(key) {
		   datafiltered = {

		    	_id : key._id,
		    	prof_gender : key.prof_gender,
		    	prof_status : key.prof_status,
		    	fullname : key.fullname,
		    	b_date : key.b_date,
		    	prof_sitio : key.prof_sitio,
		    	employmentstatus : key.employmentstatus,
		    	employmentype : key.employmentype,
		    	Pregnant : key.Pregnant,
		    	PersonWithDisability : key.PersonWithDisability
		    }
		    return datafiltered;
		});

		return array;
	}
	//create the Qr code ID
	function createQrcodeId(mzipcode, name, birthvalue, birthmonth, birthyear){

		var residentQrID;
		var shortID = shortid.generate();
		var d = new Date();
		var m = d.getMinutes();
		var h = d.getHours();
		var s = d.getSeconds();
		var getm;
		var geth;
		var gets;

		var year = d.getFullYear();
		var month = d.getMonth(); // beware: January = 0; February = 1, etc.
		var day = d.getDate();

		var dayOfWeek = d.getDay(); // Sunday = 0, Monday = 1, etc.
		var milliSeconds = d.getMilliseconds();

		if(m < 10){
			getm = "0"+m;
		}else{
			getm = m;
		};

		if(h < 10){
			geth = "0"+h;
		}else{
			geth = h;
		};

		if(s < 10){
			gets = "0"+s;
		}else{
			gets = s;
		};

		var getfirstL = name.charAt(0);
		var getsecondL = name.charAt(1);
		var stringLength = name.length;
		var lastChar = name.charAt(stringLength - 1);

		return residentQrID = mzipcode+"-"+"d"+dayOfWeek+"h"+geth+"m"+getm+"s"+gets+"ml"+milliSeconds+"-"+getfirstL+getsecondL+lastChar+stringLength+"-"+birthmonth+birthyear+shortID+birthvalue;

	}
	// create resident unique id
	function createresidentID(brgyid, mzipcode, name, birthvalue, birthmonth, birthyear){

		var residentID;
		var shortID = shortid.generate();
		var d = new Date();
		var m = d.getMinutes();
		var h = d.getHours();
		var s = d.getSeconds();
		var getm;
		var geth;
		var gets;

		var year = d.getFullYear();
		var month = d.getMonth(); // beware: January = 0; February = 1, etc.
		var day = d.getDate();

		var dayOfWeek = d.getDay(); // Sunday = 0, Monday = 1, etc.
		var milliSeconds = d.getMilliseconds();

		if(m < 10){
			getm = "0"+m;
		}else{
			getm = m;
		};

		if(h < 10){
			geth = "0"+h;
		}else{
			geth = h;
		};

		if(s < 10){
			gets = "0"+s;
		}else{
			gets = s;
		};

		var getfirstL = name.charAt(0);
		var getsecondL = name.charAt(1);
		var stringLength = name.length;
		var lastChar = name.charAt(stringLength - 1);

		var brgyID = brgyid.length;

		return residentID = mzipcode+"-"+brgyid+"-"+"d"+dayOfWeek+"h"+geth+"m"+getm+"s"+gets+"ml"+milliSeconds+"-"+getfirstL+getsecondL+lastChar+stringLength+"-"+birthmonth+birthyear+shortID+birthvalue;


	}
//=========================================================================
	//RESIDENT POST COLLECTION MIDDLEWARE
	//=========================================================================
	function postdata (req, res, colname, accesscontrol){


			var shortID = shortid.generate();
			var imagevalue;

			if(accesscontrol === 'barangayaccesscontrol'){
				var brgyresident 		= req.body;
				brgyresident.shortIds  		= shortID;
				brgyresident.usercurrentId 	= req.user;
				brgyresident.barangay 	= req.barangay;
				brgyresident.municipality 	= req.municipality;
				brgyresident.region 		= req.region;
				brgyresident.province 		= req.province;
				brgyresident.judicial 		= 'false';
				brgyresident.createdby 	= req.byusername;
				brgyresident.captain 		= req.captain;
				brgyresident.sk_chairman 	= req.sk_chairman;
				brgyresident.kagawad 	= req.kagawad;
				brgyresident.treasurer 	= req.treasurer;

				    var today  			= new Date();
				    var stringsitio                     = req.body.prof_sitio;
			                var Interviewer                     = req.body.Interviewer;

			                if(stringsitio == null){
					return res.send(301, {
					message: 'Please select the setio/purok.'});

				   }else if(Interviewer == null){
				   	return res.send(301, {
					message: 'Please select the the Interviewer name.'});
				   }

			                if(stringsitio === undefined || Interviewer === undefined){
			                    return false;
			                }

			                var returnId         		 = stringsitio.split(',');
			                var returnInterviewerid         = Interviewer.split(',');


	                		brgyresident.sitioId           	= returnId[0];
	                		brgyresident.InterviewerId       = returnInterviewerid[0];
	                		brgyresident.dateCreated 	= today;
	                		brgyresident.dateUpdated 	= today;

				var fname 			= req.body.prof_firstname,
					lname 			= req.body.prof_lastname,
					mname 		= req.body.prof_middle_name,
					region_bplace 		= req.body.region_bplace,
					baranggay_bplace 	=  req.body.baranggay_bplace,
					province_bplace 	= req.body.province_bplace,
					municipality_bplace 	= req.body.municipality_bplace,
					barangay_bplace 	= req.body.barangay_bplace,
					imageuri 		= req.body.imageuri,
					prof_status 		= req.body.prof_status,
					prof_gender 		= req.body.prof_gender,
					b_date 		= req.body.b_date;

			 		if(fname == null){
						return res.send(301, {
						message: 'First name is empty.'});

					}else if(mname == null){
						return res.send(301, {
						message: 'Middle name is empty.'});

					}else if(lname == null){
						return res.send(301, {
						message: 'Last name is empty.'});

					}else{

					var generateresidentid = createresidentID(
								req.barangay._id,
								req.municipality.zip_code,
								req.body.prof_firstname,
								req.body.birthvalue,
								req.body.birthmonth,
								req.body.birthyear
							  );
					var generatedQrvalue = createQrcodeId(
								req.municipality.zip_code,
								req.body.prof_firstname,
								req.body.birthvalue,
								req.body.birthmonth,
								req.body.birthyear
							 );


					brgyresident.residentbrgyid 	= generateresidentid;
					brgyresident.Qrcodevalue 	= generatedQrvalue;

						db.collection(colname).find({

							prof_firstname : req.body.prof_firstname,
							prof_lastname: req.body.prof_lastname,
							prof_middle_name: req.body.prof_middle_name

						}).toArray(function (err, data){
								if(data.length){
									res.send(301, {

									message: 'Sorry, cannot save "'+
									fname +' '+
									mname +' '+
									lname +'". Data already exist.' });

								}else if(baranggay_bplace == null){
									res.send(301, {
									message: 'Birth Place is empty.'});
								}else if(prof_status == null){
									res.send(301, {
									message: 'Please Select the status.'});

								}else if(prof_gender == null){
									res.send(301, {
									message: 'Please Select the gender.'});

								}else if(imageuri == null){
									res.send(301, {
									message: 'Please upload profile picture.'});

								}else if(b_date == null){
									res.send(301, {
									message: 'Birth Date is empty.'});

								}else{
									delete req.body.imageuri;
									db.collection(colname).insert(brgyresident, function(err, data){
									  return res.json(data);
									});
								}
							});
					}


			}else if(accesscontrol === 'municipalitycontrol'){

				var brgyresident = req.body;
				brgyresident.usercurrentId = req.user;
				brgyresident.municipality = req.municipality;
				brgyresident.region = req.region;
				brgyresident.province = req.province;
				brgyresident.judicial = 'false';
				brgyresident.createdby = req.byusername;

			    db.collection(colname).insert(brgyresident, function (err, data){
					return res.json(data);
				});

			}else if(accesscontrol === 'regionadmin'){
				var brgyresident = req.body;
				brgyresident.usercurrentId = req.user;
				brgyresident.judicial = 'false';
				brgyresident.createdby = req.byusername;

				var fname = req.body.prof_firstname,
					lname = req.body.prof_lastname,
					mname = req.body.prof_middle_name

				db.collection('brgyresident').count(
				{
					prof_firstname : req.body.prof_firstname,
					prof_lastname: req.body.prof_lastname,
					prof_middle_name: req.body.prof_middle_name
				},
				function (err, count) {
					if (count !== 0) {
						res.send(301, {

							message: 'Sorry, cannot save '+
							fname +' '+
							mname +' '+
							lname +'. Data already exist with different records.' });

					} else {

					    delete req.body.imageuri;
					    db.collection('brgyresident').insert(brgyresident, function (err, data){
						res.json(data);
						});
					}
				})

			}
	};

	//========================================================
	//create a json file combination of the zip code and barangay id
	//========================================================
	function postresidentbase64image(req, res, db){

		var imagevalue = req.body;
		imagevalue.usercurrentId = req.user;

		db.insert(imagevalue, function(err, data){
			res.json(data);
		});

	}

router
.use(cors())
.use(bodyparser.json({limit: '10000mb', extended: true}))
.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
.use(methodOverride())
.route('/gettotal/')
.get(function (req, res, next){

	if(req.accesscontrol === 'regionadmin'){

	    db.collection('brgyresident').count({'region.region_name': req.region}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'provincecontrol'){

	    db.collection('brgyresident').count({province : req.province}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'municipalitycontrol'){

	    db.collection('brgyresident').count({'municipality._id': req.municipality._id}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'barangayaccesscontrol'){

	    db.collection('brgyresident').count({usercurrentId: req.user}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'officialaccesscontrol'){

	    db.collection('brgyresident').count({'barangay._id' : req.barangay._id}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}

})
router
.use(cors())
.use(bodyparser.json({limit: '10000mb', extended: true}))
.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
.use(methodOverride())
.route('/getparamsgender/:gender')
.get(function (req, res, next){

	var getparamsgender = req.params.gender;

	if(req.accesscontrol === 'regionadmin'){

	    db.collection('brgyresident').count({prof_gender: getparamsgender, 'region.region_name': req.region}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'provincecontrol'){

	    db.collection('brgyresident').count({prof_gender: getparamsgender, province : req.province}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'municipalitycontrol'){

	    db.collection('brgyresident').count({prof_gender: getparamsgender, 'municipality._id': req.municipality._id}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'barangayaccesscontrol'){

	    db.collection('brgyresident').count({prof_gender: getparamsgender, usercurrentId: req.user}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'officialaccesscontrol'){

	    db.collection('brgyresident').count({prof_gender: getparamsgender, 'barangay._id' : req.barangay._id}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'Viewer'){

	    db.collection('brgyresident').count({prof_gender: getparamsgender}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}

});

router
.use(cors())
.use(bodyparser.json({limit: '10000mb', extended: true}))
.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
.use(methodOverride())
.route('/bloodtypetotal/:bloodtype')
.get(function (req, res, next){

var bloodtypetotal = req.params.bloodtype;

		if(req.accesscontrol === 'regionadmin'){

		    db.collection('brgyresident').count({'blood_type.blood_type': bloodtypetotal, 'region.region_name': req.region}, function(err, total) {
		      if (err) next(err);
		      return res.json(total);
		    });

		}else if(req.accesscontrol === 'provincecontrol'){

		    db.collection('brgyresident').count({'blood_type.blood_type': bloodtypetotal, province : req.province}, function(err, total) {
		      if (err) next(err);
		      return res.json(total);
		    });

		}else if(req.accesscontrol === 'municipalitycontrol'){

		    db.collection('brgyresident').count({'blood_type.blood_type': bloodtypetotal, 'municipality._id': req.municipality._id}, function(err, total) {
		      if (err) next(err);
		      return res.json(total);
		    });

		}else if(req.accesscontrol === 'barangayaccesscontrol'){

		    db.collection('brgyresident').count({'blood_type.blood_type': bloodtypetotal, usercurrentId: req.user}, function(err, total) {
		      if (err) next(err);
		      return res.json(total);
		    });

		}else if(req.accesscontrol === 'officialaccesscontrol'){

		    db.collection('brgyresident').count({'blood_type.blood_type': bloodtypetotal, 'barangay._id' : req.barangay._id}, function(err, total) {
		      if (err) next(err);
		      return res.json(total);
		    });

		}else if(req.accesscontrol === 'Viewer'){

		    db.collection('brgyresident').count({'blood_type.blood_type': bloodtypetotal}, function(err, total) {
		      if (err) next(err);
		      return res.json(total);
		    });

		}

});

router
.use(cors())
.use(bodyparser.json({limit: '10000mb', extended: true}))
.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
.use(methodOverride())
.route('/healthstotal/:healths')
.get(function (req, res, next){

var healthstotal = req.params.healths;

	if(req.accesscontrol === 'regionadmin'){

	    db.collection('brgyresident').count({healths: healthstotal, 'region.region_name': req.region}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'provincecontrol'){

	    db.collection('brgyresident').count({healths: healthstotal, province : req.province}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'municipalitycontrol'){

	    db.collection('brgyresident').count({healths: healthstotal, 'municipality._id': req.municipality._id}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'barangayaccesscontrol'){

	    db.collection('brgyresident').count({healths: healthstotal, usercurrentId: req.user}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'officialaccesscontrol'){

	    db.collection('brgyresident').count({healths: healthstotal, 'barangay._id' : req.barangay._id}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}else if(req.accesscontrol === 'Viewer'){

	    db.collection('brgyresident').count({healths: healthstotal}, function(err, total) {
	      if (err) next(err);
	      return res.json(total);
	    });

	}

});

//pagination route test

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/getresidentinlimit/:page_no')

	.get(function (req, res, next){
	    var pageLimit = 10;
	    var skipValue = req.params.page_no*pageLimit;
	    db.collection('brgyresident').find({usercurrentId: req.user}, null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}})

	    .toArray(function(err, data) {
	        if (err) {
	            return res.send(400, {
	                message: getErrorMessage(err)
	            });
	        } else {
	            return res.jsonp(data);
	        }
	    });

	})

router
    .use(cors())
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getseventyninemonthsbypage/:page_no')

    .get(function (req, res, next){
        var pageLimit = 10;
        var skipValue = req.params.page_no*pageLimit;

        db.collection('brgyresident').find({usercurrentId: req.user}, null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}})

        .toArray(function(err, data) {
            if (err) {
                return res.send(400, {
                    message: getErrorMessage(err)
                });
            } else {
                return res.json(data);
            }
        });

    });


router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/gethouseholdmember/:id')
	.get(function (req, res){
		db.collection('brgyresident').find({househeadId: req.params.id}).toArray(function (err, data){
			res.json(data);
		});
	})


//=========================================================================
//Queries Routes
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({ limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/getgender/:gender')
	.get(function (req, res){

	    var pageLimit = 10;
	    var getparams = req.params.gender;

		    if(getparams === undefined){
		        return false;
		    }

	               var pagenow  = getparams.split(', ')[0];
	               var gender     = getparams.split(', ')[1];
		   var skipValue = pagenow * pageLimit;

		 if(req.accesscontrol === 'regionadmin'){

			db.collection('brgyresident').find({prof_gender: gender, 'region.region_name': req.region},
				null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
				res.json(filterthis(data));
			});

		}else if(req.accesscontrol === 'provincecontrol'){

			db.collection('brgyresident').find({prof_gender: gender, province : req.province},
				null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
				res.json(filterthis(data));
			});

		}else if(req.accesscontrol === 'municipalitycontrol'){

			db.collection('brgyresident').find({prof_gender: gender, 'municipality._id': req.municipality._id},
				null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
				res.json(filterthis(data));
			});

		}else if(req.accesscontrol === 'barangayaccesscontrol'){

			db.collection('brgyresident').find({prof_gender: gender, usercurrentId: req.user},
				null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
				res.json(filterthis(data));
			});

		}else if(req.accesscontrol === 'officialaccesscontrol'){

			db.collection('brgyresident').find({prof_gender: gender, 'barangay._id' : req.barangay._id},
				null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
				res.json(filterthis(data));
			});

		}else if(req.accesscontrol === 'Viewer'){

			db.collection('brgyresident').find({prof_gender: gender},
				null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
				res.json(filterthis(data));
			});
		}

	});

//=========================================================================
//REGION COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({ limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/region')
	.get(function (req, res){
		db.collection('region').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var region = req.body;
	region.region_name = req.body.region_name;
	region.usercurrentId = req.user;

		if(region.region_name == null){
			res.send(301, {
			   message: 'Region name is empty.'});
		}else{

			db.collection('region').find({region_name : region.region_name}).toArray(function (err, data){
				if(data.length){
				   res.send(301, {
				   message: 'Region name : '+ region.region_name +'  already exist.'});
				}else{
				    db.collection('region').insert(region, function (err, data){
						res.json(data);
				    });
				}
			});

		}

	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/region/:id')
	.get(function (req, res){
		db.collection('region').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var region = req.body;
		region.region_name = req.body.region_name;
		region._id = req.body._id;


		if(region.region_name == null){
			res.send(301, {
			   message: 'Region name is empty.'});
		}else{
			db.collection('region').find({region_name : region.region_name}).toArray(function (err, data){

				if(data.length){

				   if(data[0]._id == region._id){
				   	// console.log('edit mode: data exist with own id');
					delete region._id;
					db.collection('region').updateById(req.params.id, region, function (err, data){
						res.json(data);
					});

				   }else if(data[0]._id !== req.body._id){
					// console.log('edit mode: data exist with another id');
					res.send(301, {
					   message: 'Region name : '+ region.region_name +'  already exist.'});

				   }


				}else{
					delete region._id;
					db.collection('region').updateById(req.params.id, region, function (err, data){
						res.json(data);
					});
				}
			});


		}

	})
	.delete(function (req, res){
		db.collection('region').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//PROVINCE COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/province')
	.get(function (req, res){
		db.collection('province').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var province = req.body;
	province.province_name = req.body.province_name;
	province.regionId = req.body.regionId;
	province.usercurrentId = req.user;

	if(province.province_name == null){
		res.send(301, {
		   message: 'Province name is empty.'});
	}else{

		db.collection('province').find({province_name : province.province_name, regionId:  province.regionId }).toArray(function (err, data){
			if(data.length){
			   res.send(301, {
			   message: 'Province name : '+ province.province_name  +'  already exist.'});
			}else{
			    db.collection('province').insert(province, function (err, data){
				res.json(data);
			    });
			}
		});

	}

      });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/getprovince/:id')
	.get(function (req, res){
		db.collection('province').findById(req.params.id, function (err, data){
			res.json(data);
		});
	});
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/province/:id')
	.get(function (req, res){
		var region = {
			regionId : req.params.id
		}
		db.collection('province').find(region).toArray(function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var province = req.body;
		province.province_name = req.body.province_name;
		province._id = req.body._id;
	// delete province._id;

		if(province.province_name == null){
			res.send(301, {
			   message: 'Province name is empty.'});
		}else{
			db.collection('province').find({province_name : province.province_name}).toArray(function (err, data){

				if(data.length){

				   if(data[0]._id == province._id){
				   	// console.log('edit mode: data exist with own id');
					delete province._id;
					db.collection('province').updateById(req.params.id, province, function (err, data){
						res.json(data);
					});

				   }else if(data[0]._id !== req.body._id){
					// console.log('edit mode: data exist with another id');
					res.send(301, {
					   message: 'Province name : '+ province.province_name +'  already exist.'});

				   }


				}else{
					delete province._id;
					db.collection('province').updateById(req.params.id, province, function (err, data){
						res.json(data);
					});
				}
			});


		}



	})
	.delete(function (req, res){
		db.collection('province').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//MUNICIPALITY COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/municipality')
	.get(function (req, res){
		db.collection('municipality').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var municipality = req.body;
	municipality.usercurrentId = req.user;
	municipality.municipality_name = req.body.municipality_name;
	municipality.zip_code = req.body.zip_code;
	municipality.provinceId = req.body.provinceId;

		if(municipality.municipality_name == null){
			res.send(301, {
			   message: 'Municipality name is empty.'});
		}else if(municipality.zip_code == null){
			res.send(301, {
			   message: 'Municipality zip code is empty.'});
		}else{

			db.collection('municipality').find({municipality_name : municipality.municipality_name, provinceId:  municipality.provinceId}).toArray(function (err, data){
				if(data.length){
				   res.send(301, {
				   message: 'Municipality name : '+ municipality.municipality_name +'  already exist.'});
				}else{
				    db.collection('municipality').insert(municipality, function (err, data){
					res.json(data);
				    });
				}
			});

		}

	})
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/getmunicipality/:id')
	.get(function (req, res){
		db.collection('municipality').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/municipality/:id')
	.get(function (req, res){
		db.collection('municipality').find({provinceId : req.params.id}).toArray(function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var municipality = req.body;
			municipality.municipality_name = req.body.municipality_name;
			municipality.zip_code = req.body.zip_code;
			municipality._id = req.body._id;
		           // delete municipality._id;

			if(municipality.municipality_name == null){
				res.send(301, {
				   message: 'Municipality name is empty.'});
			}else if(municipality.zip_code == null){
				res.send(301, {
				   message: 'Municipality zip code is empty.'});
			}else{

				db.collection('municipality').find({municipality_name : municipality.municipality_name}).toArray(function (err, data){

					if(data.length){

					   if(data[0]._id == municipality._id){
					   	// console.log('edit mode: data exist with own id');
						delete municipality._id;
						db.collection('municipality').updateById(req.params.id, municipality, function (err, data){
							res.json(data);
						});

					   }else if(data[0]._id !== req.body._id){
						// console.log('edit mode: data exist with another id');
						res.send(301, {
						   message: 'Municipality name : '+ municipality.municipality_name +'  already exist.'});
					   }

					}else{
						delete municipality._id;
						db.collection('municipality').updateById(req.params.id, municipality, function (err, data){
							res.json(data);
						});
					}
				});

			}

	})
	.delete(function (req, res){
		db.collection('municipality').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//BARANGAY COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/brgycol')
	.get(function (req, res){
		db.collection('barangay').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var brgydata = req.body;
	brgydata.barangay_name = req.body.barangay_name;
	brgydata.usercurrentId = req.user;
	brgydata.municipalId = req.body.municipalId;
	if(brgydata.barangay_name == null){
		res.send(301, {
		message: 'Barangay name is empty.'});
	}else{

		db.collection('barangay').find({barangay_name : brgydata.barangay_name, municipalId:  brgydata.municipalId}).toArray(function (err, data){
			if(data.length){
			   res.send(301, {
			   message: 'Barangay name : '+ brgydata.barangay_name +'  already exist.'});
			}else{
			    db.collection('barangay').insert(brgydata, function (err, data){
				res.json(data);
			    });
			}
		});

	}


	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/getbrgycol/:id')
	.get(function (req, res){
		var brgy = {
			municipalId : req.params.id
		}
		db.collection('barangay').findById(req.params.id, function (err, data){
			res.json(data);
		});
	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/brgycol/:id')
	.get(function (req, res){
		var brgy = {
			municipalId : req.params.id
		}
		db.collection('barangay').find(brgy).toArray(function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var brgy = req.body;
		brgy.barangay_name = req.body.barangay_name;
		brgy._id = req.body._id;
		// delete brgy._id;
		if(brgy.barangay_name == null){
				res.send(301, {
				   message: 'Barangay name is empty.'});
		}else{

			db.collection('barangay').find({barangay_name : brgy.barangay_name}).toArray(function (err, data){

				if(data.length){

				   if(data[0]._id == brgy._id){
				   	// console.log('edit mode: data exist with own id');
					delete brgy._id ;
					db.collection('barangay').updateById(req.params.id, brgy, function (err, data){
					    res.json(data);
					});

				   }else if(data[0]._id !== req.body._id){
					// console.log('edit mode: data exist with another id');
					res.send(301, {
					   message: 'Barangay name : '+ brgy.barangay_name +'  already exist.'});
				   }

				}else{
					delete brgy._id ;
					     db.collection('barangay').updateById(req.params.id, brgy, function (err, data){
						res.json(data);
					      });
				}
			});


		}

	})
	.delete(function (req, res){
		db.collection('barangay').removeById(req.params.id, function(){
			res.json(null);
		});
	});

// ====================================================
// DATA FILTERING FUNCTION
// ====================================================

function getuserrole (req, res, accesscontrol, colname){

	var datafiltered;

	if(colname === 'brgyresident'){

		if(accesscontrol === 'provincecontrol'){

			db.collection(colname).find({ province: req.province}).toArray(function (err, data){
            			return res.json(filterthis(data));
			});

		}else if(accesscontrol === 'barangayaccesscontrol'){

			db.collection(colname).find({ barangay: req.barangay}).toArray(function (err, data){

				return res.json(filterthis(data));

			});

		}else if(accesscontrol === 'municipalitycontrol'){

			db.collection(colname).find({ 'municipality._id': req.municipality._id}).toArray(function (err, data){
			 	return res.json(filterthis(data));
			});

		}else if(accesscontrol === 'regionadmin'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(filterthis(data));
			});

		}else if(accesscontrol === 'Viewer'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(filterthis(data));
			});

		}else if(accesscontrol === 'officialaccesscontrol'){

			db.collection(colname).find({'barangay._id' : req.barangay._id}).toArray(function (err, data){
			 	return res.json(filterthis(data));
			});

		};

	}else if(colname === 'brgyjudicial'){

		if(accesscontrol === 'barangayaccesscontrol'){

			db.collection(colname).find({ usercurrentId: req.user}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'provincecontrol'){

			db.collection(colname).find({ 'referrerprovince._id': req.province._id}).toArray(function (err, data){
            			return res.json(data);
			});

		}else if(accesscontrol === 'municipalitycontrol'){

			db.collection(colname).find({ 'referrermunicipality._id': req.municipality._id}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'regionadmin'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'Viewer'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(data);
			});
		}else if(accesscontrol === 'officialaccesscontrol'){

			db.collection(colname).find({'referrerbarangay._id' : req.barangay._id}).toArray(function (err, data){
			 	return res.json(data);
			});

		};

	}else if(colname === 'brgyclearance'){

		if(accesscontrol === 'barangayaccesscontrol'){

			db.collection(colname).find({ barangay: req.barangay}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'municipalitycontrol'){

			db.collection(colname).find({ 'municipality._id': req.municipality._id}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'regionadmin'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'Viewer'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(data);
			});
		}else if(accesscontrol === 'officialaccesscontrol'){

			db.collection(colname).find({'barangay._id' : req.barangay._id}).toArray(function (err, data){
			 	return res.json(data);
			});

		};
	};
};

//=========================================================================
//CLEARANCE COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/clearance')
	.get(function (req, res){

		var clearance = 'brgyclearance';
		var role = req.accesscontrol
		getuserrole(req, res, role, clearance);

	})

   .post(function(req, res) {
		var shortID = shortid.generate();
 		var inno_id = uuid.v4();
 		var today = new Date();

		var purpose = req.body.purpose;
		var remark = req.body.remark;
		var jud_issuedON = req.body.jud_issuedON;
		var OR = req.body.OR;
		var clearance_price = req.body.clearance_price;

		var clearance = req.body;
		    clearance.shortids 			= shortID;
		    clearance.residentId 		= clearance.resident._id;
		    clearance.clearance_control_no 	= inno_id;
	               clearance.region 			= req.region;
	               clearance.province 			= req.province;
	               clearance.municipality 		= req.municipality;
	               clearance.barangay 		= req.barangay;
	               clearance.createdby 		= req.byusername;
		    clearance.usercurrentId 		= req.user;
		    clearance.date_of_issue 		= today;

			if(purpose == null){
				res.send(301, {
							message: 'Purpose field is empty.'});
			}else if(jud_issuedON == null){
				res.send(301, {
							message: 'Issued On field is empty.'});
			}else if(remark == null){
				res.send(301, {
							message: 'Remark field is empty.'});
			}else if(OR == null){
				res.send(301, {
							message: 'OR Number field is empty.'});
			}else if(clearance_price == null){
				res.send(301, {
							message: 'Given Price is empty.'});
			}else{
				    db.collection('brgyclearance').insert(clearance, function (err, data){
						res.json(data);
					});
			}

	});
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/getclearance/:id')
	.get(function (req, res){

		db.collection('brgyclearance').findById(req.params.id, function (err, data){
		    res.json(data);
		});

	})
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/clearance/:id')
	.get(function (req, res){
		db.collection('brgyclearance').find({residentId:req.params.id}).toArray(function (err, data){
			res.json(data);
		});

	})
	.put(function (req, res, next){

		var inno_id = uuid.v4();
 		var today = new Date();

		var purpose 		= req.body.purpose;
		var remark 		= req.body.remark;
		var jud_issuedON 	= req.body.jud_issuedON;
		var OR 		= req.body.OR;
		var clearance_price 	= req.body.clearance_price;

		var clearance = req.body;
		      clearance.residentId 		= clearance.resident._id;
		      clearance.clearance_control_no 	= inno_id;
	           	      clearance.region 			= req.region;
	                  clearance.province 		= req.province;
	                  clearance.municipality 		= req.municipality;
	                  clearance.barangay 		= req.barangay;
	                  clearance.createdby 		= req.byusername;
		      clearance.usercurrentId 		= req.user;
		      clearance.date_of_issue 		= today;

			if(purpose == null){
				res.send(301, {
							message: 'Purpose field is empty.'});
			}else if(jud_issuedON == null){
				res.send(301, {
							message: 'Issued On field is empty.'});
			}else if(remark == null){
				res.send(301, {
							message: 'Remark field is empty.'});
			}else if(OR == null){
				res.send(301, {
							message: 'OR Number field is empty.'});
			}else if(clearance_price == null){
				res.send(301, {
							message: 'Given Price is empty.'});
			}else{

				delete clearance._id;
				db.collection('brgyclearance').updateById(req.params.id, clearance, function (err, data){
					res.json(data);
				});

			}


	})
	.delete(function (req, res){
		db.collection('brgyclearance').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//JUDICIAL COLLECTION
//=========================================================================
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/judicial')
	.get(function (req, res){

		var judicial = 'brgyjudicial';
		var role = req.accesscontrol
		getuserrole(req, res, role, judicial);

	})
   .post(function(req, res, next) {
	  var shortID = shortid.generate();
	  var brgyjudicial = req.body;

		  brgyjudicial.shortIDs = shortID;
		  brgyjudicial.usercurrentId = req.user;
		  brgyjudicial.referrerId =	req.currentIdref;
		  brgyjudicial.referrerprovince = req.province;
		  brgyjudicial.referrerregion = req.region ;
		  brgyjudicial.referrerbarangay = req.barangay;
		  brgyjudicial.referrermunicipality = req.municipality;

		  var brgyresident 			= req.body;
				var respondent 				= req.body.respondent;
				var complainantresident 	= req.body.complainant_resident;
				var complainanta 			= req.body.complainant;
				var respondentresident 		= req.body.respondent_resident;
				var casetypea				= req.body.case_type;
				var datee 					= req.body.prof_date;

				db.collection('brgyjudicial').find({

					respondent : req.body.respondent,
					case_type: req.body.case_type,

				}).toArray(function (err,data) {

					if(respondent == null){
						res.send(301, {
							message: 'Respondent field is empty.'});

					}else if(complainantresident == null){
						res.send(301, {
							message: 'Is the respondent a resident of the barangay?'});

					}else if(complainanta == null){
						res.send(301, {
							message: 'Complainant field is empty,'});

					}else if(respondentresident == null){
						res.send(301, {
							message: 'Is the complainant a resident of the barangay?'});

					}else if(casetypea == null){
						res.send(301, {
							message: 'Case type field is empty.'});

					}else if(datee == null){
						res.send(301, {
							message: 'Please select date.'});

					}else{

						db.collection('brgyjudicial').insert(brgyresident, function(err, data){
							  return res.json(data);
						});
					}
				});

});


router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/judicial/:id')
	.get(function (req, res){
		db.collection('brgyjudicial').findById(req.params.id, function (err, data){
			res.json(data);
		})
	})
	.put(function (req, res, next){

		var judicial = req.body;

				var respondent 		= req.body.respondent;
				var complainantresident 	= req.body.complainant_resident;
				var complainanta 		= req.body.complainant;
				var respondentresident 	= req.body.respondent_resident;
				var casetypea			= req.body.case_type;
				var datee 			= req.body.prof_date;

		db.collection('brgyjudicial').find({

					respondent : req.body.respondent,
					case_type: req.body.case_type,

				}).toArray(function (err,data) {

					if(respondent == null){
						res.send(301, {
							message: 'Respondent field is empty.'});

					}else if(complainantresident == null){
						res.send(301, {
							message: 'Is the respondent a resident of the barangay?'});

					}else if(complainanta == null){
						res.send(301, {
							message: 'Complainant field is empty,'});

					}else if(respondentresident == null){
						res.send(301, {
							message: 'Is the complainant a resident of the barangay?'});

					}else if(casetypea == null){
						res.send(301, {
							message: 'Case type field is empty.'});

					}else if(datee == null){
						res.send(301, {
							message: 'Please select date.'});

					}else{

						delete judicial._id;

							db.collection('brgyjudicial').updateById(req.params.id, judicial, function (err, data){
								res.json(data);
							});
					}
				});


	})
	.delete(function (req, res){
		db.collection('brgyjudicial').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//RESIDENT COLLECTION ROUTE AND IMAGES ROUTE
//=========================================================================

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/base64image')
    .get(function(req, res){
    	var base64imagestorage = getbaseimagedb(req, res);
    	getbase64images(req, res, base64imagestorage);
    })
    .post(function(req, res){

	var useraccountID	= req.user;
	var brgyId		= req.barangay._id;
	var municipalitycode 	= req.municipality.zip_code;
	var createjsonfile = municipalitycode+"_"+brgyId;
	var base64imagestorage = new bourne('base64images/'+createjsonfile+'.json');

	postresidentbase64image(req,res, base64imagestorage);

    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/base64image/:residentid')
    .get(function(req, res){
    	getbase64images(req, res);
    })
    .put(function(req, res){

	postresidentbase64image(req,res);
    })
    .delete(function(req, res){

    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb', extended: true}))
	.use(bodyparser.json({limit: '1000mb', extended: true}))
	.use(methodOverride())
	.route('/judicial_resident')
	.get(function (req, res){

			db.collection('brgyresident').find().toArray(function (err, data){
            return res.json(data);
			});



	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/resident')
	.get(function (req, res){

		var resident = 'brgyresident';
		var role = req.accesscontrol
		getuserrole(req, res, role, resident);



	})
   .post(function(req, res) {
   	var resident = 'brgyresident', accesscontrol = req.accesscontrol
		postdata(req, res, resident, accesscontrol);
	});

router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/residentsetdeaceased/:id')
	.put(function (req, res){

	var brgyresident = req.body;

	    delete brgyresident._id;

			db.collection('brgyresident').updateById(req.params.id, brgyresident, function(err, data) {
				   res.json(data);
			});

	});
router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/residentset/:id')
	.put(function (req, res){

	var brgyresident = req.body;
		brgyresident.judicial = 'true';
		brgyresident.badrecord = 'Yes';
		brgyresident.createdby 		= req.byusername;
		brgyresident.captain 			= req.captain;
		brgyresident.sk_chairman 		= req.sk_chairman;
		brgyresident.kagawad 		= req.kagawad;
		brgyresident.treasurer 		= req.treasurer;
		brgyresident.secretary			= req.secretary;

	    delete brgyresident._id;

			db.collection('brgyresident').updateById(req.params.id, brgyresident, function(err, data) {
				   res.json(data[0]);
			});

	});
router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/get_qr_resident/:id')
	.get(function (req, res){
		db.collection('brgyresident').find({shortIds: req.params.id}).toArray(function (err, data){

			if(!data){
			 res.send(301, {
				   message: 'Qr value not found!'});
			}else{
		              return res.json(data);
			}

		})
	});

router
.use(cors())
.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
.use(bodyparser.json({limit: '10000mb', extended: true}))
.use(methodOverride())
	.route('/residentofficials/:id')
	.get(function (req, res){

		db.collection('brgyresident').findById(req.params.id, function (err, data){
			return res.json(data);
		})

	});
router
.use(cors())
.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
.use(bodyparser.json({limit: '10000mb', extended: true}))
.use(methodOverride())
	.route('/residentinfo/:residentid')
	.get(function (req, res){
		var id = req.params.residentid;
		var account_role = req.accesscontrol;
		return residentviews.residentgetinfo(req, res, account_role, id);

	})
router
.use(cors())
.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
.use(bodyparser.json({limit: '10000mb', extended: true}))
.use(methodOverride())
	.route('/resident/:id')
	.get(function (req, res){

	db.collection('brgyresident').findById(req.params.id, function (err, data){

			var getage = calculate_age(data.b_date);
			// calculate the age base on the birthdate given.

		            function calculate_age(dateString){
		                  var birthday = new Date(dateString);
		                  var ageDifMs = Date.now() - birthday.getTime();
		                  var ageDate = new Date(ageDifMs); // miliseconds from epoch
		                  return Math.abs(ageDate.getFullYear() - 1970);
		            };

		            data.current_age = getage;
		            res.json(data);
			})

	})
	.put(function (req, res){

			var brgyresident 		= req.body;
			brgyresident.usercurrentId 	= req.user;
			brgyresident.barangay 	= req.barangay;
			brgyresident.municipality 	= req.municipality;
			brgyresident.region 		= req.region;
			brgyresident.province 		= req.province;
			brgyresident.createdby 	= req.byusername;
			brgyresident.captain 		= req.captain;
			brgyresident.sk_chairman 	= req.sk_chairman;
			brgyresident.kagawad 	= req.kagawad;
			brgyresident.treasurer 	= req.treasurer;
			brgyresident.secretary		= req.secretary;

			    var today  = new Date();
		                var stringsitio                     = req.body.prof_sitio;
		                var Interviewer                     = req.body.Interviewer;

		                if(stringsitio === undefined || Interviewer === undefined){
		                    return false;
		                }

		                var returnId         		 = stringsitio.split(',');
		                var returnInterviewerid         = Interviewer.split(',');

                		brgyresident.sitioId           	= returnId[0];
                		brgyresident.InterviewerId       = returnInterviewerid[0];
                		brgyresident.dateUpdated	= today;

			var fname 			= req.body.prof_firstname,
				lname 			= req.body.prof_lastname,
				mname 		= req.body.prof_middle_name,
				region_bplace 		= req.body.region_bplace,
				baranggay_bplace 	= req.body.baranggay_bplace,
				province_bplace 	= req.body.province_bplace,
				municipality_bplace 	= req.body.municipality_bplace,
				barangay_bplace 	= req.body.barangay_bplace,
				imageuri 		= req.body.imageuri,
				prof_status 		= req.body.prof_status,
				prof_gender 		= req.body.prof_gender,
				b_date 			= req.body.b_date;

			db.collection('brgyresident').find({

				prof_firstname : req.body.prof_firstname,
				prof_lastname: req.body.prof_lastname,
				prof_middle_name: req.body.prof_middle_name

			}).toArray(function (err, data){

					if(data.length){

						if(data[0]._id == req.body._id){

							if(fname == null){
								res.send(301, {
								message: 'First name is empty.'});

							}else if(stringsitio == null){
								return res.send(301, {
								message: 'Please select the setio/purok.'});

							}else if(mname == null){
								res.send(301, {
								message: 'Middle name is empty.'});

							}else if(lname == null){
								res.send(301, {
								message: 'Last name is empty.'});

							}
							else if(baranggay_bplace == null){
								res.send(301, {
								message: 'Birth Place is empty.'});
							}
							else if(prof_status == null){
								res.send(301, {
								message: 'Please Select the status.'});

							}else if(prof_gender == null){
								res.send(301, {
								message: 'Please Select the gender.'});

							}else if(imageuri == null){
								res.send(301, {
								message: 'Please upload profile picture.'});

							}else if(b_date == null){
								res.send(301, {
								message: 'Birth Date is empty.'});

							}else{
								delete brgyresident._id;
								db.collection('brgyresident').updateById(req.params.id, brgyresident, function (err, data){

								  return res.json(data[0]);
								});
							}

						}else if(data[0]._id !== req.body._id){

							res.send(301, {

								message: 'Sorry, cannot save "'+
								fname +' '+
								mname +' '+
								lname +'". Data already exist.' });
						}


					}else if(!data.length){

						if(fname == null){
							res.send(301, {
							message: 'First name is empty.'});
						}
						else if(stringsitio == null){
							return res.send(301, {
							message: 'Please select the setio/purok.'});
						}else if(mname == null){
							res.send(301, {
							message: 'Middle name is empty.'});

						}else if(lname == null){
							res.send(301, {
							message: 'Last name is empty.'});
						}else if(baranggay_bplace == null){
							res.send(301, {
							message: 'Birth Place is empty.'});
						}else if(prof_status == null){
							res.send(301, {
							message: 'Please Select the status.'});

						}else if(prof_gender == null){
							res.send(301, {
							message: 'Please Select the gender.'});

						}else if(imageuri == null){
							res.send(301, {
							message: 'Please upload profile picture.'});

						}else if(b_date == null){
							res.send(301, {
							message: 'Birth Date is empty.'});

						}else{

							delete brgyresident._id;
							db.collection('brgyresident').updateById(req.params.id, brgyresident, function (err, data){
							  return res.json(data[0]);
							});
						}

					}
				});


	})
	.delete(function (req, res){
		db.collection('brgyresident').removeById(req.params.id, function(){
			res.json(null);
		});
	});


//=========================================================================
//MANAGE ACCOUNT COLLECTION
//=========================================================================


var crypto = require('crypto');

function hash (password) {
	return crypto.createHash('sha256').update(password).digest('hex');
}

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/provincelist')
	.get(function (req, res){
		db.collection('province_userlist').find({ createdbyId: req.user }).toArray(function (err, data){
			res.json(data);
		});
	})
router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/provincelist/:id')
	.get(function (req, res){
		db.collection('province_userlist').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res){

		var user 		= req.body;
		user.updateby 		= req.byusername;


			var province = req.body.province,
				region = req.body.region,
				username = req.body.username,
				password = req.body.pswrd,
				verifypassword = req.body.verifypassword

			if(region == null){
				res.send(301, {
						message: 'Please Select the Region.'});
			}else if(province == null){
				res.send(301, {
						message: 'Please Select the Province.'});
			}else if(username == null){
				res.send(301, {
						message: 'Error: Username is empty.'});
			}else if(password){

				db.collection('province_userlist').find({username: username}).toArray(function (err, data){

					if(data.length){

						if(data[0]._id == req.body._id){

						  db.collection('province_userlist').find(
							{
								region : region,
								province : province

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);
											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;

											db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

									delete user.pswrd;
									delete user.verifypassword;
								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						  db.collection('province_userlist').find(
							{
								region : region,
								province : province

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);

											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;
											delete user._id;

											db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}
				});

			}else{

				db.collection('province_userlist').find({username: username}).toArray(function (err, data){

					  if(data.length){

						if(data[0]._id == req.body._id){

						db.collection('province_userlist').find(
							{
								region : region,
								province : province

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						db.collection('province_userlist').find(
							{
								region : region,
								province : province

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('province_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}

				});
			};
	})

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/brgy_userlist')
	.get(function (req, res){
		db.collection('brgy_userlist').find({ createdbyId: req.user }).toArray(function (err, data){
			res.json(data);
		});
	})

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/brgy_userlist/:id')
	.get(function (req, res){
		db.collection('brgy_userlist').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res){

		if(req.accesscontrol === 'regionadmin'){
			var user 		= req.body;
			user.updateby		= req.byusername;

		var barangay 			= req.body.barangay,
			province 		= req.body.province,
			municipality 		= req.body.municipality,
			region 			= req.body.region,
			username 		= req.body.username,
			captain 		= req.body.captain,
			password 		= req.body.pswrd,
			mlogo 			= req.body.mlogo,
			verifypassword 	= req.body.verifypassword

			if(region == null){
				res.send(301, {
						message: 'Please Select the Region.'});
			}else if(mlogo == null){
				res.send(301, {
						message: 'Please Select the upload the logo..'});
			}else if(province == null){
				res.send(301, {
						message: 'Please Select the Province.'});
			}else if(municipality == null){
				res.send(301, {
						message: 'Please Select the municipality.'});
			}else if(barangay == null){
				res.send(301, {
						message: 'Please Select the barangay.'});
			}else if(captain == null){
				res.send(301, {
						message: 'Error: barangay captain is empty.'});
			}else if(username == null){
				res.send(301, {
						message: 'Error: Username is empty.'});
			}else if(password){

				db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){

					if(data.length){

						if(data[0]._id == req.body._id){

						  db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);
											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;

											db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

									delete user.pswrd;
									delete user.verifypassword;
								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						  db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);

											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;
											delete user._id;

											db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}
				});

			}else{

				db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){

					  if(data.length){

						if(data[0]._id == req.body._id){

						db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

									res.send(301, {
											 message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}

				});
			};

		}else if(req.accesscontrol === 'provincecontrol'){

		var user 			= req.body;
			user.updateby		= req.byusername;
		var barangay 			= req.body.barangay,
			province 		= req.province,
			municipality 		= req.body.municipality,
			region 			= req.region,
			username 		= req.body.username,
			captain 		= req.body.captain,
			password 		= req.body.pswrd,
			mlogo 			= req.body.mlogo,
			verifypassword 	= req.body.verifypassword

			if(municipality == null){
				res.send(301, {
						message: 'Please Select the municipality.'});
			}else if(mlogo == null){
				res.send(301, {
						message: 'Please Select the upload the logo..'});
			}else if(barangay == null){
				res.send(301, {
						message: 'Please Select the barangay.'});
			}else if(captain == null){
				res.send(301, {
						message: 'Error: barangay captain is empty.'});
			}else if(username == null){
				res.send(301, {
						message: 'Error: Username is empty.'});
			}else if(password){

				db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){

					if(data.length){

						if(data[0]._id == req.body._id){

						  db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);
											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;

											db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

									delete user.pswrd;
									delete user.verifypassword;
								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						  db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);

											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;


											db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}
				});

			}else{

				db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){

					  if(data.length){

						if(data[0]._id == req.body._id){

						db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 barangay.barangay_name  +', '+
											 municipality.municipality_name  +', '+
											 province.province_name +', '+
											 region.region_name +', '+
											' has already been assigned.'  });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

									res.send(301, {
											message: 'Error: For '+
											 barangay.barangay_name  +', '+
											 municipality.municipality_name  +', '+
											 province.province_name +', '+
											 region.region_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}

				});
			};
		}else if(req.accesscontrol === 'municipalitycontrol'){

		var user 			= req.body;
			user.updateby		= req.byusername;
		var barangay 			= req.body.barangay,
			province 		= req.province,
			municipality 		= req.body.municipality,
			region 			= req.region,
			username 		= req.body.username,
			captain 		= req.body.captain,
			password 		= req.body.pswrd,
			mlogo 			= req.body.mlogo,
			verifypassword 	= req.body.verifypassword

			if(municipality == null){
				res.send(301, {
						message: 'Please Select the municipality.'});
			}else if(mlogo == null){
				res.send(301, {
						message: 'Please Select the upload the logo..'});
			}else if(barangay == null){
				res.send(301, {
						message: 'Please Select the barangay.'});
			}else if(captain == null){
				res.send(301, {
						message: 'Error: barangay captain is empty.'});
			}else if(username == null){
				res.send(301, {
						message: 'Error: Username is empty.'});
			}else if(password){

				db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){

					if(data.length){

						if(data[0]._id == req.body._id){

						  db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);
											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;

											db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

									delete user.pswrd;
									delete user.verifypassword;
								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						  db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 region.region_name +', '+
											 province.province_name +', '+
											 municipality.municipality_name +', '+
											 barangay.barangay_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

	 									if(verifypassword == null){
										res.send(301, {
													message: 'Error: Please verify your password.'});
										}else if(password !== verifypassword){

													res.send(301, {
														message:
														'Error: Password not match!'});
										}else{

											user.password 	= hash(req.body.pswrd);

											delete user.pswrd;
											delete user.verifypassword;
											delete user._id;
											delete user._id;

											db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
											  return res.json(data);

											});
										}
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}
				});

			}else{

				db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){

					  if(data.length){

						if(data[0]._id == req.body._id){

						db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

										res.send(301, {
											message: 'Error: For '+
											 barangay.barangay_name  +', '+
											 municipality.municipality_name  +', '+
											 province.province_name +', '+
											 region.region_name +', '+
											' has already been assigned.'  });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});

						}else if(data[0]._id !== req.body._id){
							//console.log('username is change but password change')
							res.send(301, {
									message:
									'Error: Sorry, cannot create account "'+
									username +'" already taken.'});
						}

					}else{

						db.collection('brgy_userlist').find(
							{
								region : region,
								province : province,
								municipality : municipality,
								barangay : barangay

							}).toArray(

							function (err, count_res) {

								if(count_res.length){

									if (count_res[0]._id != req.body._id) {

									res.send(301, {
											message: 'Error: For '+
											 barangay.barangay_name  +', '+
											 municipality.municipality_name  +', '+
											 province.province_name +', '+
											 region.region_name +', '+
											' has already been assigned.' });

									 }else if(count_res[0]._id == req.body._id){

										delete user._id;
										db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
										  return res.json(data);

										});
									 }

								}else{

								 	//console.log('here you go if no data exist yet.');
							 		delete user._id;
									db.collection('brgy_userlist').updateById(req.params.id, user, function (err, data){
									  return res.json(data);

									});
								}


							});
					}

				});
			};
		}


	})

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/registered')
	.get(function (req, res){
		db.collection('userlist').find({ createdbyId: req.user }).toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {

    if(req.accesscontrol === 'regionadmin'){

    	var accesscontrol = req.body.accesscontrol;

    	if(accesscontrol == null){
		res.send(301, {
				message: 'Please Select the admin role first.'});
    	}else{

			if(accesscontrol === 'provincecontrol'){
		    		// console.log('this is the access province');
				var user 			= req.body;
				user.createdbyId 	= req.user;
				user.updateby 		= req.byusername;
				user.createdbyadmin	= req.byusername;
				user.role 		= 'admin';
				user.active_caption 	= 'Deactivate';
				user.active 		= 'Active';

					var province 		 = req.body.province,
						region 		 = req.body.region,
						username 	 = req.body.username,
						password 	 = req.body.pswrd,
						verifypassword = req.body.verifypassword;

					if(region == null){
						res.send(301, {
								message: 'Please Select the Region.'});
					}else if(province == null){
						res.send(301, {
								message: 'Please Select the Province.'});
					}else if(username == null){
						res.send(301, {
								message: 'Error: Username is empty.'});
					}else if(password == null){
						res.send(301, {
								message: 'Error: Password is empty.'});
					}else if(verifypassword == null){
						res.send(301, {
								message: 'Error: Please verify your password.'});
					}else{

						db.collection('province_userlist').count(
						{
							region : region,
							province : province
						},
						function (err, data) {

							if (data !== 0) {

								res.send(301, {
									message: 'Error: For '+
									 region.region_name +', '+
									 province.province_name +', '+
									' has already been assigned.' });

							 } else {

								db.collection('province_userlist').find({username: username}).toArray(function (err, data){
									if(data.length){
												res.send(301, {
													message:
													'Error: Sorry, cannot create account, username "'+
													username +'" exist!'});

									}else if(password !== verifypassword){

												res.send(301, {
													message:
													'Error: Password not match!'});
									}else{
										user.password 	= hash(req.body.pswrd);
										delete user.pswrd;
										delete user.verifypassword;

										db.collection('province_userlist').insert(user, function(err, data){
										res.json(data);
										});
									}
								});
							}
						})

					};

		    	}else if(accesscontrol === 'municipalitycontrol'){

		    	var user 			= req.body;
				user.createdbyId 	= req.user;
				user.updateby 		= req.byusername;
				user.createdbyadmin	= req.byusername;
				user.role 		= 'admin';
				user.active_caption 	= 'Deactivate';
				user.active 		= 'Active';

					var municipality 	= req.body.municipality,
						region 			= req.body.region,
						province 		= req.body.province,
						username 		= req.body.username,
						password 		= req.body.pswrd,
						verifypassword 	= req.body.verifypassword

					if(region == null){
						res.send(301, {
								message: 'Please Select the Region.'});
					}else if(province == null){
						res.send(301, {
								message: 'Please Select the Province.'});
					}else if(municipality == null){
						res.send(301, {
								message: 'Please Select the Municipality.'});
					}else if(username == null){
						res.send(301, {
								message: 'Error: Username is empty.'});
					}else if(password == null){
						res.send(301, {
								message: 'Error: Password is empty.'});
					}else if(verifypassword == null){
						res.send(301, {
								message: 'Error: Please verify your password.'});
					}else{

						db.collection('userlist').count(
						{
							region : region,
							province : province,
							municipality : municipality
						},
						function (err, data) {

							if (data !== 0) {

								res.send(301, {
									message: 'Error: For '+
									 region.region_name +', '+
									 province.province_name +', '+
									 municipality.municipality_name +' '+
									' has already been assigned.' });

							 } else {

								db.collection('userlist').find({username: username}).toArray(function (err, data){
									if(data.length){
												res.send(301, {
													message:
													'Error: Sorry, cannot create account, username "'+
													username +'" exist!'});

									}else if(password !== verifypassword){

												res.send(301, {
													message:
													'Error: Password not match!'});
									}else{
										user.password 	= hash(req.body.pswrd);
										delete user.pswrd;
										delete user.verifypassword;

										db.collection('userlist').insert(user, function(err, data){
										res.json(data);
										});
									}
								});
							}
						})

					};

		    	}else if(accesscontrol === 'barangayaccesscontrol'){
		    		// console.log('this is the access barangayaccesscontrol');
				var user 		= req.body;

				user.createdbyId 	= req.user;
				user.createdbyadmin	= req.byusername;
				user.role 		= 'admin';
				user.active_caption     = 'Deactivate';
				user.accesscontrol 	= 'barangayaccesscontrol'
				user.active 		= 'Active';

					var barangay 		 = req.body.barangay,
						municipality 	 = req.body.municipality,
						region 		 = req.body.region,
						province 	 = req.body.province,
						username 	 = req.body.username,
						captain 	 = req.body.captain,
						password 	 = req.body.pswrd,
						captain 	 = req.body.captain,
						mlogo 	 	 = req.body.mlogo,
						verifypassword = req.body.verifypassword;

		 			if(username == null){
						res.send(301, {
								message: 'Error: Username is empty.'});
					}else if(mlogo == null){
						res.send(301, {
								message: 'Error: Please upload the municipality logo.'});
					}else if(municipality == null){
						res.send(301, {
								message: 'Error: municipality is empty.'});
					}else if(region == null){
						res.send(301, {
								message: 'Error: region is empty.'});
					}else if(province == null){
						res.send(301, {
								message: 'Error: province is empty.'});
					}else if(barangay == null){
						res.send(301, {
								message: 'Error: Please Select the Barangay.'});
					}else if(password == null){
						res.send(301, {
								message: 'Error: Password is empty.'});
					}else if(verifypassword == null){
						res.send(301, {
								message: 'Error: Please verify your password.'});
					}else if(password !== verifypassword){
						res.send(301, {
								message: 'Error: Password not match.'});
					}else if(captain == null){
								res.send(301, {
										message: 'Error: Barangay Captain is empty.'});
							 }else{

						db.collection('brgy_userlist').count(
						{
							region 			: region,
							province 		: province,
							municipality 	: municipality,
							barangay 		: barangay
						},
						function (err, data) {

							if (data !== 0) {

								res.send(301, {
									message: 'Error: For '+
									 barangay.barangay_name +', '+
									 municipality.municipality_name +', '+
									 province.province_name +', From Region -> '+
									 region.region_name +', '+
									' has already been assigned!' });

							 }else {

								db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){
									if(data.length){
												res.send(301, {
													message:
													'Error: Sorry, cannot create account, username "'+
													username +'" exist!'});

									}else if(password !== verifypassword){

												res.send(301, {
													message:
													'Error: Password not match!'});
									}else{

										user.password 		= hash(req.body.pswrd);
										delete user.pswrd;
										delete user.verifypassword;

										db.collection('brgy_userlist').insert(user, function(err, data){
										res.json(data);
										});
									}
								});
							}
						})

					};
		    	};
    	}


    }else if (req.accesscontrol === 'municipalitycontrol'){

    	var user 			= req.body;

		user.createdbyId 	= req.user;
		user.municipality 	= req.municipality;
		user.region 		= req.region;
		user.province 		= req.province ;
		user.createdbyadmin	= req.byusername;
		user.role 		= 'admin';
		user.active_caption     = 'Deactivate';
		user.accesscontrol 	= 'barangayaccesscontrol'
		user.active 		= 'Active';

			var barangay = req.body.barangay,
				username  	 = req.body.username,
				captain  	 = req.body.captain,
				password 	 = req.body.pswrd,
				captain 	 = req.body.captain,
				mlogo      	 = req.body.mlogo,
				verifypassword = req.body.verifypassword

			if(barangay == null){
				res.send(301, {
						message: 'Error: Please Select the Barangay.'});
			}else if(mlogo == null){
				res.send(301, {
						message: 'Error: Please upload the municipality logo.'});
			}else if(username == null){
				res.send(301, {
						message: 'Error: Username is empty.'});
			}else if(password == null){
				res.send(301, {
						message: 'Error: Password is empty.'});
			}else if(verifypassword == null){
				res.send(301, {
						message: 'Error: Please verify your password.'});
			}else if(password !== verifypassword){
				res.send(301, {
						message: 'Error: Password not match.'});
			}else if(captain == null){
						res.send(301, {
								message: 'Error: Barangay Captain is empty.'});
					 }else{

				db.collection('brgy_userlist').count(
				{
					region 			: req.region,
					province 		: req.province,
					municipality 	: req.municipality,
					barangay 		: barangay
				},
				function (err, data) {

					if (data !== 0) {

						res.send(301, {
							message: 'Error: For '+
							 barangay.barangay_name +', '+
							 req.municipality.municipality_name +', '+
							 req.province.province_name +', From Region -> '+
							 req.region.region_name +', '+
							' has already been assigned!' });

					 }else {

						db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){
							if(data.length){
										res.send(301, {
											message:
											'Error: Sorry, cannot create account, username "'+
											username +'" exist!'});

							}else if(password !== verifypassword){

										res.send(301, {
											message:
											'Error: Password not match!'});
							}else{

								user.password 		= hash(req.body.pswrd);
								delete user.pswrd;
								delete user.verifypassword;

								db.collection('brgy_userlist').insert(user, function(err, data){
								res.json(data);
								});
							}
						});
					}
				})

			}
    	}else if (req.accesscontrol === 'provincecontrol'){

    	var accesscontrol = req.body.accesscontrol;

    	if(accesscontrol == null){
				res.send(301, {
						message: 'Please Select the admin role first.'});
    	}else{

			if(accesscontrol === 'municipalitycontrol'){

		    	var user 			= req.body;
				user.region 		= req.region;
				user.province 		= req.province;
				user.createdbyId 	= req.user;
				user.updateby 		= req.byusername;
				user.createdbyadmin	= req.byusername;
				user.role 		= 'admin';
				user.active_caption     = 'Deactivate';
				user.active 		= 'Active';

					var municipality 	= req.body.municipality,
						region 			= req.region,
						province 		= req.province,
						username 		= req.body.username,
						password 		= req.body.pswrd,
						verifypassword 	= req.body.verifypassword

					if(municipality == null){
						res.send(301, {
								message: 'Please Select the Municipality.'});
					}else if(username == null){
						res.send(301, {
								message: 'Error: Username is empty.'});
					}else if(password == null){
						res.send(301, {
								message: 'Error: Password is empty.'});
					}else if(verifypassword == null){
						res.send(301, {
								message: 'Error: Please verify your password.'});
					}else{

						db.collection('userlist').count(
						{
							region : region,
							province : province,
							municipality : municipality
						},
						function (err, data) {

							if (data !== 0) {

								res.send(301, {
									message: 'Error: For '+
									 region.region_name +', '+
									 province.province_name +', '+
									 municipality.municipality_name +' '+
									' has already been assigned.' });

							 } else {

								db.collection('userlist').find({username: username}).toArray(function (err, data){
									if(data.length){
												res.send(301, {
													message:
													'Error: Sorry, cannot create account, username "'+
													username +'" exist!'});

									}else if(password !== verifypassword){

												res.send(301, {
													message:
													'Error: Password not match!'});
									}else{
										user.password 	= hash(req.body.pswrd);
										delete user.pswrd;
										delete user.verifypassword;

										db.collection('userlist').insert(user, function(err, data){
										res.json(data);
										});
									}
								});
							}
						})

					};

		    	}else if(accesscontrol === 'barangayaccesscontrol'){
		    		// console.log('this is the access barangayaccesscontrol');
				var user 			= req.body;

				user.createdbyId 	= req.user;
				user.region 		= req.region;
				user.province 		= req.province;
				user.createdbyadmin	= req.byusername;
				user.role 		= 'admin';
				user.active_caption 	= 'Deactivate';
				user.accesscontrol 	= 'barangayaccesscontrol'
				user.active 		= 'Active';

					var barangay 		= req.body.barangay,
						municipality 	= req.body.municipality,
						region 			= req.region,
						province 		= req.province,
						username 		= req.body.username,
						captain 		= req.body.captain,
						password 		= req.body.pswrd,
						captain 		= req.body.captain,
						mlogo 			= req.body.mlogo,
						verifypassword 	= req.body.verifypassword;

		 			if(username == null){
						res.send(301, {
								message: 'Error: Username is empty.'});
					}else if(mlogo == null){
						res.send(301, {
								message: 'Error: Please upload the municipality logo.'});
					}else if(barangay == null){
						res.send(301, {
								message: 'Error: Please Select the Barangay.'});
					}else if(password == null){
						res.send(301, {
								message: 'Error: Password is empty.'});
					}else if(verifypassword == null){
						res.send(301, {
								message: 'Error: Please verify your password.'});
					}else if(password !== verifypassword){
						res.send(301, {
								message: 'Error: Password not match.'});
					}else if(captain == null){
								res.send(301, {
										message: 'Error: Barangay Captain is empty.'});
							 }else{

						db.collection('brgy_userlist').count(
						{
							region 			: region,
							province 		: province,
							municipality 	: municipality,
							barangay 		: barangay
						},
						function (err, data) {

							if (data !== 0) {

								res.send(301, {
									message: 'Error: For '+
									 barangay.barangay_name +', '+
									 municipality.municipality_name +', '+
									 province.province_name +', From Region -> '+
									 region.region_name +', '+
									' has already been assigned!' });

							 }else {

								db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){
									if(data.length){
												res.send(301, {
													message:
													'Error: Sorry, cannot create account, username "'+
													username +'" exist!'});

									}else if(password !== verifypassword){

												res.send(301, {
													message:
													'Error: Password not match!'});
									}else{

										user.password 		= hash(req.body.pswrd);
										delete user.pswrd;
										delete user.verifypassword;

										db.collection('brgy_userlist').insert(user, function(err, data){
										res.json(data);
										});
									}
								});
							}
						})

					};
		    	};
    	}

    	}else if (req.accesscontrol === 'barangayaccesscontrol'){

    	var user 			= req.body;

		user.createdbyId 	= req.user;
		user.municipality 	= req.municipality;
		user.region 		= req.region;
		user.province 		= req.province ;
		user.barangay 		= req.barangay ;
		user.createdbyadmin	= req.byusername;
		user.role 		= 'guest';
		user.active_caption 	= 'Deactivate';
		user.accesscontrol 	= 'user'
		user.active 		= 'Active';

			var barangay 	      		= req.body.barangay,
				username  		= req.body.username,
				password    		= req.body.pswrd,
				verifypassword 	= req.body.verifypassword

				db.collection('brgy_userlist').find({username: username}).toArray(function (err, data){
					if(data.length){
								res.send(301, {
									message:
									'Error: Sorry, cannot create account, username "'+
									username +'" exist!'});

					}else if(password !== verifypassword){

								res.send(301, {
									message:
									'Error: Password not match!'});
					}else{

						user.password 		= hash(req.body.pswrd);
						delete user.pswrd;
						delete user.verifypassword;

						db.collection('brgy_userlist').insert(user, function(err, data){
						res.json(data);
						});
					}
				})

    	}

});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/account_deactivate/:id')
	.put(function (req, res){

		var user = req.body;

		if(user.access === 'provincecontrol'){
			db.collection('province_userlist').updateById(req.params.id,
				{$set: user},
				function(err, data){
				res.json(data);
			});
		}else if(user.access === 'municipalitycontrol'){
			db.collection('userlist').updateById(req.params.id,
				{$set: user},
				function(err, data){
				res.json(data);
			});
		}else if(user.access === 'barangayaccesscontrol'){
			db.collection('brgy_userlist').updateById(req.params.id,
				{$set: user},
				function(err, data){
				res.json(data);
			});
		}


	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/edit_own_account/:id')
	.put(function (req, res, next){

	function getmy_access(req, res){

 	    var user = req.body;

		var username 		= req.body.username,
			newpassword 	= req.body.pswrd,
			verifypassword 	= req.body.verifypassword

			if(newpassword){

				if(newpassword !== verifypassword){

					res.send(301, {
						message:
						'Error: Password not match!'});
				}else if(username == null){
					res.send(301, {
									message:
									'Error: Username is empty.'});
				}else{

						var i = 0;

						 db.collection('userlist').find({username: user.username}).toArray(function (err, data){


								if(data.length){

									if(data[0]._id == req.body._id){

										//console.log('username not change but password change')
										user.password = hash(req.body.pswrd);
										delete user._id;
										delete user.pswrd
										delete user.verifypassword
										db.collection('userlist').updateById(req.params.id, user, function(err, data){
										res.json(data);
										});

									}else if(data[0]._id !== req.body._id){
										//console.log('username is change but password change')
										res.send(301, {
												message:
												'Error: Sorry, cannot create account "'+
												username +'" already taken.'});
									}

								}else{

									//console.log('data is change without conflict but password change');

									user.password = hash(req.body.pswrd);
									delete user._id;
									delete user.pswrd
									delete user.verifypassword

									db.collection('userlist').updateById(req.params.id, user, function(err, data){
									res.json(data);
									});

								}

						});

				}

			}else if(username == null){
				res.send(301, {
								message:
								'Error: Username is empty.'});
			}else{

				db.collection('userlist').find({username: user.username}).toArray(function (err, data){

						if(data.length){

							if(data[0]._id == req.body._id){

								//console.log('username not change and without change password')

								delete user._id;

								db.collection('userlist').updateById(req.params.id, user, function(err, data){
								res.json(data);
								});

							}else if(data[0]._id !== req.body._id){

								//console.log('username is change and without change password')
								res.send(301, {
										message:
										'Error: Sorry, cannot create account "'+
										username +'" already taken.'});
							}


						}else{

							//console.log('data is change without conflict and without change password');

							delete user._id;

							db.collection('userlist').updateById(req.params.id, user, function(err, data){
							res.json(data);
							});

						}

				});

			}

		}

		getmy_access(req, res);
	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/municipalityaccount/:id')
	.put(function (req, res, next){

		 	      	var user 				= req.body;
						user.updateby		= req.byusername;


					var municipality = req.body.municipality,
						username = req.body.username,
						newpassword = req.body.pswrd,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){

										res.send(301, {
											message:
											"Error: Password not match."});

									}else if(username == null){
										res.send(301, {
											message:
											"Error: username is empty."});
									}else{

										 db.collection('userlist').find({username: user.username}).toArray(function (err, data){


												if(data.length){

													if(data[0]._id == req.body._id){

														//console.log('username not change but password change')
														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd
														delete user.verifypassword
														db.collection('userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});

													}else if(data[0]._id !== req.body._id){

														//console.log('username is change but password change')
														res.send(301, {
																message:
																'Error: Sorry, cannot create account "'+
																username +'" already taken.'});
													}


												}else{

													//console.log('data is change without conflict but password change');

													user.password = hash(req.body.pswrd);
													delete user._id;
													delete user.pswrd
													delete user.verifypassword

													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}

										});

									}
								}else if(username == null){
									res.send(301, {
										message:
										"Error: username is empty."});
								}else{

									db.collection('userlist').find({username: user.username}).toArray(function (err, data){

											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')

													delete user._id;

													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message:
															'Error: Sorry, cannot create account "'+
															username +'" already taken.'});
												}


											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;

												db.collection('userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});

											}

									});

								}
	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/registered/:id')
	.get(function (req, res){
		db.collection('userlist').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){

		 if(req.accesscontrol === 'regionadmin' || req.accesscontrol === 'provincecontrol'){

		 	      	var user 				= req.body;
						user.updateby		= req.byusername;


					var municipality = req.body.municipality,
						username = req.body.username,
						newpassword = req.body.pswrd,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){

										res.send(301, {
											message:
											"Error: Password not match."});

									}else if(username == null){
										res.send(301, {
											message:
											"Error: username is empty."});
									}else{

										 db.collection('userlist').find({username: user.username}).toArray(function (err, data){


												if(data.length){

													if(data[0]._id == req.body._id){

														//console.log('username not change but password change')
														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd
														delete user.verifypassword
														db.collection('userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});

													}else if(data[0]._id !== req.body._id){

														//console.log('username is change but password change')
														res.send(301, {
																message:
																'Error: Sorry, cannot create account "'+
																username +'" already taken.'});
													}


												}else{

													//console.log('data is change without conflict but password change');

													user.password = hash(req.body.pswrd);
													delete user._id;
													delete user.pswrd
													delete user.verifypassword

													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}

										});

									}
								}else if(username == null){
									res.send(301, {
										message:
										"Error: username is empty."});
								}else{

									db.collection('userlist').find({username: user.username}).toArray(function (err, data){

											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')

													delete user._id;

													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message:
															'Error: Sorry, cannot create account "'+
															username +'" already taken.'});
												}


											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;

												db.collection('userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});

											}

									});

								}

			}else if(req.accesscontrol === 'municipalitycontrol' || req.accesscontrol === 'barangayaccesscontrol' || req.accesscontrol === 'provincecontrol'){


					var user 	= req.body;
					user.updateby	= req.byusername;


					var barangay 		= req.body.barangay,
						username 	= req.body.username,
						captain 	= req.body.captain,
						newpassword   = req.body.pswrd,
						mlogo   	 = req.body.mlogo,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){

										res.send(301, {
											message:
											"Error: Password not match!"});
									}else if(captain == null){
										res.send(301, {
												message: 'Error: Baranggay Captain is empty!'});
									}else if(username == null){
										res.send(301, {
												message: 'Error: Username is empty.'});
									}else{

											 db.collection('brgy_userlist').find({username: user.username}).toArray(function (err, data){


													if(data.length){

														if(data[0]._id == req.body._id){

															//console.log('username not change but password change')
															user.password = hash(req.body.pswrd);
															delete user._id;
															delete user.pswrd
															delete user.verifypassword
															db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
															res.json(data);
															});

														}else if(data[0]._id !== req.body._id){

															//console.log('username is change but password change')
															res.send(301, {
																	message:
																	'Error: Sorry, cannot create account "'+
																	username +'" already taken.'});
														}


													}else{

														//console.log('data is change without conflict but password change');

														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd
														delete user.verifypassword

														db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});

													}

											});

									}

								}else if(captain == null){
										res.send(301, {
												message: 'Error: Baranggay Captain is empty.'});
								}
								else if(mlogo == null){
										res.send(301, {
												message: 'Error: Please upload municipality logo.'});
								}else if(username == null){
										res.send(301, {
												message: 'Error: Username is empty.'});
								}else{

									db.collection('brgy_userlist').find({username: user.username}).toArray(function (err, data){

											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')

													delete user._id;

													db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message:
															'Error: Sorry, cannot create account "'+
															username +'" already taken.'});
												}


											}else if(captain == null){
												res.send(301, {
														message: 'Error: Baranggay Captain is empty!'});
											}else if(mlogo == null){
													res.send(301, {
															message: 'Error: Please upload municipality logo.'});
											}else if(username == null){
													res.send(301, {
															message: 'Error: Username is empty.'});
											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;

												db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});

											}

									});

								}

			}else if(req.accesscontrol === 'barangayaccesscontrol'){


					var user 				= req.body;
						user.createdbyId 	= req.user;
						user.createdbyadmin	= req.byusername;
						user.updateby		= req.byusername;

					var barangay = req.body.barangay,
						username = req.body.username,
						newpassword = req.body.pswrd,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){

										res.send(301, {
											message:
											"Error: Password don't match!"});
									}else{

											 db.collection('brgy_userlist').find({username: user.username}).toArray(function (err, data){


													if(data.length){

														if(data[0]._id == req.body._id){

															//console.log('username not change but password change')
															user.password = hash(req.body.pswrd);
															delete user._id;
															delete user.pswrd
															delete user.verifypassword
															db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
															res.json(data);
															});

														}else if(data[0]._id !== req.body._id){

															//console.log('username is change but password change')
															res.send(301, {
																	message:
																	'Error: Sorry, cannot create account "'+
																	username +'" already taken.'});
														}


													}else{

														//console.log('data is change without conflict but password change');

														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd
														delete user.verifypassword

														db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});

													}

											});

									}
								}else{

									db.collection('brgy_userlist').find({username: user.username}).toArray(function (err, data){

											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')

													delete user._id;

													db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message:
															'Error: Sorry, cannot create account "'+
															username +'" already taken.'});
												}


											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;

												db.collection('brgy_userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});

											}

									});

								}

			}
	})
	.delete(function (req, res){
		db.collection('brgy_userlist').removeById(req.params.id, function(){
			res.json(null);
		});
	});
//Get the user account info when logging in.
router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
	.route('/account')
	.get(function (req, res){

		var data = null;

		if(req.accesscontrol === 'provincecontrol'){

			db.collection('province_userlist').findById(req.user, {password:0}, function (err, data){
				res.json(data);
			})

		}else if(req.accesscontrol === 'municipalitycontrol'){

			db.collection('userlist').findById(req.user, {password:0}, function (err, data){
				res.json(data);
			})

		}else if(req.accesscontrol === 'barangayaccesscontrol'){

			db.collection('brgy_userlist').findById(req.user, {password:0}, function (err, data){
				res.json(data);
			})

		}else if(req.accesscontrol === 'regionadmin'){

			db.collection('userlist').findById(req.user, {password:0}, function (err, data){
				res.json(data);
			})

		}else if(req.accesscontrol === 'Viewer' || req.accesscontrol === 'officialaccesscontrol'){

			db.collection('viewer_userlist').findById(req.user, {password:0}, function (err, data){
				res.json(data);
			})

		}else{

			res.json(data);
		}

	});

module.exports = router;