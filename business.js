var express                 = require('express'),
    bodyparser              = require('body-parser'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    mongo                   = require('./dbconnection/dbconnect'),
    db                          = mongo.dbbestconnect();
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid         = require('shortid');

     var getExactDate  = function(getdate){

            var days,
                month;

            var d = new Date(getdate);
            days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
           var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
            'November', 'December'];

            var today = days[d.getUTCDay()];
            var dd = days[d.getUTCDay()];
            var mm = month[d.getMonth()]; //January is 0!
            var yyyy = d.getFullYear();
            var dy = d.getDate();

            var todays = mm+' '+dy+', '+ yyyy;
            return todays;

    }
//=========================================================================
//barangay business COLLECTION
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/businessowner')
    .get(function (req, res){

        if(req.accesscontrol === 'provincecontrol'){

            db.collection('businessowner').find({ 'province._id': req.province._id}).toArray(function (err, data){
                        return res.json(data);
            });

        }else if(req.accesscontrol === 'barangayaccesscontrol'){

                db.collection('businessowner').find({usercurrentId: req.user}).toArray(function (err, data){
                    return res.json(data);
                });

        }else if(req.accesscontrol === 'municipalitycontrol'){

            db.collection('businessowner').find({ 'municipality._id': req.municipality._id}).toArray(function (err, data){
                return res.json(data);
            });

        }else if(req.accesscontrol === 'regionadmin'){

            db.collection('businessowner').find().toArray(function (err, data){
                return res.json(data);
            });

        }else if(req.accesscontrol === 'Viewer'){

            db.collection('businessowner').find().toArray(function (err, data){
                return res.json(data);
            });

        }else if(req.accesscontrol === 'officialaccesscontrol'){

            db.collection('businessowner').find({'barangay._id' : req.barangay._id}).toArray(function (err, data){
                return res.json(data);
            });

        };

    })
    .post(function (req, res) {

    var businessowner          = req.body;

    var residentownerfullname = '';
    if(businessowner.resident_owner_here === 'Yes'){

       residentownerfullname =  businessowner.business_owner.split(',')[1];

    }else if(businessowner.resident_owner_here === 'No'){

        residentownerfullname = businessowner.prof_firstname +' '+ businessowner.prof_middle_name +' '+ businessowner.prof_lastname;

    }

            var shortID                 = shortid.generate();
            businessowner.shorid         = shortID;
            businessowner.ownerfullname = residentownerfullname;
            businessowner.usercurrentId = req.user;
            var today = new Date();

      businessowner.dateCreated = getExactDate(today);

        db.collection('businessowner').insert(businessowner, function (err, data){
            return res.json(data);
        });

    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/businessowner/:id')
    .get(function (req, res){
        db.collection('businessowner').findById(req.params.id, function (err, data){
            return res.json(data);
        });
    })
    .put(function (req, res, next){
        var businessowner = req.body;

    var residentownerfullname = '';
    if(businessowner.resident_owner_here === 'Yes'){

       residentownerfullname =  businessowner.business_owner.split(',')[1];

    }else if(businessowner.resident_owner_here === 'No'){

        residentownerfullname = businessowner.prof_firstname +' '+ businessowner.prof_middle_name +' '+ businessowner.prof_lastname;

    }
            var shortID                 = shortid.generate();
            businessowner.shorid         = shortID;
            businessowner.ownerfullname = residentownerfullname;
            businessowner.usercurrentId = req.user;
            var today = new Date();
            businessowner.dateUpdated = getExactDate(today);
        delete businessowner._id;

            db.collection('businessowner').updateById(req.params.id, businessowner, function (err, data){
                return res.json(data);
            });

    })
    .delete(function (req, res){
        db.collection('businessowner').removeById(req.params.id, function(){
            res.json(null);
        });
    });

//route for the business permit released

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/businesspermit')
    .get(function (req, res){
        db.collection('businesspermit').find({usercurrentId: req.user}).toArray(function (err, data){
            return res.json(data);
        });
    })

    .post(function (req, res) {
    var businesspermit                              = req.body;
            var shortID                                 = shortid.generate();
            businesspermit.shorid                = shortID;
            businesspermit.usercurrentId     = req.user;
            businesspermit.barangay            = req.barangay;
            var today                                       = new Date();
            businesspermit.dateCreated               = getExactDate(today);

        db.collection('businesspermit').insert(businesspermit, function (err, data){
            return res.json(data);
        });

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/mybusinesspermit/:id')
    .get(function (req, res){
        var mypermit = {
            ownerId : req.params.id
        }
        db.collection('businesspermit').find(mypermit).toArray(function(err, data){
            return res.json(data);
        });
    })

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/businesspermit/:id')
    .get(function (req, res){
        db.collection('businesspermit').findById(req.params.id, function(err, data){
            return res.json(data);
        });
    })

    .put(function (req, res){
                var businesspermit = req.body;
                var shortID                         = shortid.generate();
                businesspermit.shorid                = shortID;
                businesspermit.usercurrentId     = req.user;
                businesspermit.barangay = req.barangay;
                var today                           = new Date();
                businesspermit.dateUpdated = getExactDate(today);

            delete businesspermit._id;
                db.collection('businesspermit').updateById(req.params.id, businesspermit, function (err, data){
                    res.json(data);
                });
    })
    .delete(function (req, res){
        db.collection('businesspermit').removeById(req.params.id, function(){
            res.json(null);
        });
    });
module.exports = router;