var express                 = require('express'),
    bodyparser              = require('body-parser'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    mongo                   = require('./dbconnection/dbconnect'),
    db                          = mongo.dbbestconnect();
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid         = require('shortid');

     var getExactDate  = function(getdate){

            var days,
                month;

            var d = new Date(getdate);
            days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
           var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
            'November', 'December'];

            var today = days[d.getUTCDay()];
            var dd = days[d.getUTCDay()];
            var mm = month[d.getMonth()]; //January is 0!
            var yyyy = d.getFullYear();
            var dy = d.getDate();

            var todays = mm+' '+dy+', '+ yyyy;
            return todays;

    }
//=========================================================================
//barangay Establishments COLLECTION
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/establishments')
    .get(function (req, res){
        db.collection('establishment').find({usercurrentId: req.user}).toArray(function (err, data){
            return res.json(data);
        });
    })

    .post(function (req, res) {
    var establishment                              = req.body;
            var shortID                                 = shortid.generate();
            establishment.shorid                = shortID;
            establishment.usercurrentId     = req.user;
            establishment.barangay            = req.barangay;
            var today                                       = new Date();
            establishment.dateCreated               = getExactDate(today);

        db.collection('establishment').insert(establishment, function (err, data){
            return res.json(data);
        });

    });

router
    .use(cors())
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getestablishmentbypage/:page_no')

    .get(function (req, res, next){
        var pageLimit = 5;
        var skipValue = req.params.page_no*pageLimit;

        db.collection('establishment').find({usercurrentId: req.user}, null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}})

        .toArray(function(err, data) {
            if (err) {
                return res.send(400, {
                    message: getErrorMessage(err)
                });
            } else {
                return res.json(data);
            }
        });

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/myestablishments/:id')
    .get(function (req, res){
        var getmyestablishment = {
            ownerId : req.params.id
        }
        db.collection('establishment').find(getmyestablishment).toArray(function(err, data){
            return res.json(data);
        });
    })

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/establishments/:id')
    .get(function (req, res){
        db.collection('establishment').findById(req.params.id, function(err, data){
            return res.json(data);
        });
    })

    .put(function (req, res){

                var establishment = req.body;
                var shortID                         = shortid.generate();
                establishment.shorid                = shortID;
                establishment.usercurrentId     = req.user;
                establishment.barangay = req.barangay;
                var today                           = new Date();
                establishment.dateUpdated = getExactDate(today);

            delete establishment._id;
                db.collection('establishment').updateById(req.params.id, establishment, function (err, data){
                    res.json(data);
                });
    })
    .delete(function (req, res){
        db.collection('establishment').removeById(req.params.id, function(){
            res.json(null);
        });
    });


module.exports = router;