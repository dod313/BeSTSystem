'use strict';

var config          = require("../configs");
var MongoClient = require('mongodb').MongoClient,
      assert = require('assert');

  // get only the selected items.
  function filterthis(data){

    var array = data.map(function(key) {
       var datafiltered = {

          _id : key._id,
          fullname : key.fullname,
          b_date : key.b_date,
          residentbrgyid : key.residentbrgyid

        }
        return datafiltered;
    });

    return array;
  };

exports.residentsearchthistext = function(req, res, searchtext){

var gettextstring = searchtext.toString();
      var role = req.accesscontrol;

        // Connection URL for the barangay database
         var forsearchdb = config.mongodb.brgydata;
        // Use connect method to connect to the Server
        MongoClient.connect(forsearchdb, function(err, db) {
          assert.equal(null, err);

              findbrgyresident(db, function() {
                db.close();
              });

        });
                var findbrgyresident = function(db, callback) {
                  // Get the documents from barangay resident collection
                  var brgycollection = db.collection( 'brgyresident');

                  var stringpageandtext = gettextstring.split(',');

                  // skip(pageNumber > 0 ? ((pageNumber-1)*nPerPage) : 0).limit(nPerPage)

                        brgycollection.find({fullname:{$regex:stringpageandtext[1], $options:"$i"}, usercurrentId: req.user})
                        .skip(stringpageandtext[0] > 0 ? ((stringpageandtext[0]-1)*10) : 0).limit(10).sort({'dateUpdated': -1})
                        .toArray(function(err, data) {
                            return res.json(filterthis(data));
                        } );

                }

};

