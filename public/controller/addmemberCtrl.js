'use strict';

var app = angular.module('brgyapp');

app.controller('addmemberCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, getresidenthousehead, ResidentServices, ReportService){

$scope.gethousehead = getresidenthousehead.data;
  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

$scope.getbaranggay = [];
$http.get(DbCollection + 'brgycol/')
  .then(function(places) {
     $scope.getbaranggay = places.data;
    }, function(error) {
      alert('Failed: ' + error);
    });

console.log($scope.getbaranggay );
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

 $scope.children_me = [];
  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children = [];

    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });

$scope.children_m = []
  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });

              $scope.$watch('brgyresident.b_date', function(dateString){
                    var birthday = new Date(dateString);
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

              })

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

          $http.get(DbCollection+'/region')
          .then(function(result){
            $scope.r = result.data;
          });

          $scope.$watch('brgyresident.region_bplace._id', function(id){
              $http.get(DbCollection+'/province/'+id)
              .then(function(result){
               $scope.p = result.data;
              })
          });

          $scope.$watch('brgyresident.province_bplace._id', function(id){
              $http.get(DbCollection+'/municipality/'+id)
              .then(function(result){
               $scope.m = result.data;
              })
          });

          $scope.$watch('brgyresident.municipality_bplace._id', function(id){
              $http.get(DbCollection+'/brgycol/'+id)
              .then(function(result){
               $scope.b = result.data;
              })
          });

          $scope.showImageCropper = true;

          var currentmunicipal = $scope.UserAcount.municipality;

          $scope.brgyresident = {
                  upload: []
           };

          $scope.uploadFile = function(){
                var file = $scope.avatar;
                var uploadUrl = "resident/images";
                fileUpload.uploadFileToUrl(file, uploadUrl);
            };

          $scope.brgyresident = {
                    avatar: []
           };

            $scope.purpose                 = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();
            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations       = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                  = ItemsServices.healths();
            $scope.others                   = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.bloodtype               = ItemsServices.bloodtype();
            $scope.relationtype            = ItemsServices.relationtype();

        var img;

                 $scope.take_snapshot = function(data_uri) {
                        Webcam.snap( function(data_uri) {
                            $scope.brgyresident.imageuri = data_uri;
                            document.getElementById('capture_image').innerHTML =
                                '<img id="myImg" src="'+data_uri+'"/>' +
                                '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'+
                                $scope.brgyresident.imageuri +'" >';
                        });

                            var x = document.getElementById("myImg").src;
                            var x_result = x.toString();
               }

              $scope.brgyresident.demography = [{id: 'choice1'}, {id: 'choice2'}];

              $scope.addNewChoice = function() {
                var newItemNo = $scope.brgyresident.demography.length+1;
                 $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

                console.log($scope.brgyresident.demography)

              };

              $scope.removeChoice = function() {
                var lastItem = $scope.brgyresident.demography.length-1;
                $scope.brgyresident.demography.splice(lastItem);
              };

                $scope.saveresident = function() {
                 var bdate = $scope.brgyresident.b_date;
                  var dbmonth = ItemsServices.getmonth(bdate);
                  var bvalue = ItemsServices.getdatevalue(bdate);
                  var byear = ItemsServices.get_issuedYear(bdate);

                  $scope.brgyresident.fullname =  $scope.brgyresident.prof_firstname+' '+
                                                                      $scope.brgyresident.prof_middle_name+' '+
                                                                      $scope.brgyresident.prof_lastname;

                  $scope.brgyresident.househeadId = $scope.gethousehead._id;
                  $scope.brgyresident.birthvalue = bvalue;
                  $scope.brgyresident.birthmonth = dbmonth;
                  $scope.brgyresident.birthyear = byear;
                  $scope.brgyresident.birthString= dbmonth +' '+ bvalue +', '+ byear;

                Restangular.all('resident').post($scope.brgyresident).then(function(brgyresident) {
                  $location.path('/resident');
                }, function (error) {
                        $scope.error = error.data.message;
                    });
              }

})