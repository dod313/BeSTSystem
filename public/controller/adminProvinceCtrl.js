'use strict';

var app = angular.module('brgyapp');

app.controller('adminProvinceCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, SitioPurokService,
  RegionCollections, regionService, ReportService){

// initialize the accordion element, set to true means everytime we click the element the current dom will close
    $scope.oneAtATime = true;

// initialize the number of item to view and current page
    $scope.currentPage = 1;
    $scope.pageSize = 6;

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

// get the municipality list with the current province id
      $http.get(DbCollection+'/municipality/'+ $rootScope.UserAcount.province._id)
      .then(function(resultdata){
       $scope.showmunicipalities = resultdata.data;
      });

      $scope.$watch('showmunicipalities.municipality', function(getmunicipalvalue){
        if(getmunicipalvalue){

        }
      });

$scope.viewresidentprofile = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/adminRegionPortal/viewresidentprofile.html',
        controller: $scope.viewresidentprofileCtrl,
        size: size,
        resolve: {
              getresident: function($http){
                  if(id){
                    return $http.get(DbCollection + '/resident/'+ id);
                  }else{
                    return null;

                  }
                }
              }
      });


      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
        });

};

$scope.viewresidentprofileCtrl = function($scope, getresident, $http, $modalInstance){

        $scope.addNewChoice = function() {
          var newItemNo = $scope.brgyresident.demography.length+1;
           $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

          console.log($scope.brgyresident.demography)

        };

         $scope.children_me = [];
          $http.get(DbCollection + 'judicial_resident/')
                .then(function(resultme){
                $scope.children_me = resultme.data;
          });

        SitioPurokService.GetAllSitioPurok()
        .then(function(getlistresult){
          $scope.GetAllSitioPuroklist = getlistresult.data;
        })

      $scope.languagesfamily    = ItemsServices.familylanguage();
      $scope.languages              = ItemsServices.languages();
      $scope.languagesdialect   = ItemsServices.languagesdialect();
      $scope.immunizations       = ItemsServices.immunizations();
      $scope.nutritions               = ItemsServices.nutritions();
      $scope.healths                   = ItemsServices.healths();
      $scope.others                     = ItemsServices.others();
      $scope.waters                    = ItemsServices.waters();
      $scope.supplies                  = ItemsServices.supplies();
      $scope.facilities                  = ItemsServices.facilities();
      $scope.otherones               = ItemsServices.otherones();
      $scope.bloodtype               = ItemsServices.bloodtype();

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.brgyresident = getresident.data;

}

    // get municipalit list of resident
    $scope.getmunicipalresidentlist = function(id){
    regionService.GetResidentMunicipal(id)
    .then(function(resultdata){
            $scope.getresidentregion = resultdata.data;
    })

           $http.get(DbCollection+'/brgycol/'+id)
            .then(function(resuletdata){
              $scope.barangays = resuletdata.data;
            });
    };

    // get barangay list of resident
    $scope.getbarangaylistresident = function(id, municipality_name){

            regionService.GetResidentBarangay(id)
            .then(function(resultdata){
                    $scope.getresidentregion = resultdata.data;
                    $scope.municipality_name = municipality_name;

            })

    };

});