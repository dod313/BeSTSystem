'use strict';

var app = angular.module('brgyapp');

app.controller('barangayreportCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, SurverReportCollections,
  SitioPurokService, ReportService, transFactory,
  householdservice, SurveyReportServices, EstablishmentsFactory){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

        var d = new Date();
        $scope.SYear = d.getFullYear();


              SitioPurokService.GetAllSitioPurok()
              .then(function(getlistresult){
                return $scope.GetAllSitioPuroklist = getlistresult.data;

              })

            // get all house hold info.
            householdservice.GetAllHousehold()
            .then(function(resultdata){
                  $scope.gethouseholdsetiopurok = resultdata.data;
            });

            // get all Trancients info.
            transFactory.GetAllTranscends()
            .then(function(resultdata){
                $scope.GetAllTranscients = resultdata.data;
            });

            //get all seventynine months old child
              ReportService.getseventyninemonths()
              .then(function(resultchild){
                $scope.getchild = resultchild;
              });


            // get all house hod info.
            householdservice.GetAllHousehold()
            .then(function(resultdata){
                  $scope.gethouseholdsetiopurok = resultdata.data;
            });

            householdservice.GetAllHousehold()
            .then(function(resultdata){
                 var allfamiies = resultdata.data;

            // get all Establishments
              EstablishmentsFactory.GetAllEstablishments()
              .then(function(resultdata){
                 $scope.getallestablishment =  resultdata.data;
              })

              var CR = [];
              angular.forEach(allfamiies, function(value, key) {

                if(value.CR === 'No'){
                            this.push({
                              id: value._id,
                              brgy_Sitiopurok : value.brgy_Sitiopurok,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, CR);

                  $scope.eCR = CR;

              var Water = [];
              angular.forEach(allfamiies, function(value, key) {

                if(value.Water === 'No'){
                            this.push({
                              id: value._id,
                              brgy_Sitiopurok : value.brgy_Sitiopurok,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, Water);

                  $scope.eWater = Water;

              var Electricity = [];
              angular.forEach(allfamiies, function(value, key) {

                if(value.Electricity === 'No'){
                            this.push({
                              id: value._id,
                              brgy_Sitiopurok : value.brgy_Sitiopurok,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, Electricity);

                  $scope.eElectricity = Electricity;

            });

            $http.get(DbCollection + 'resident/')
             .then(function(result){
              var residents = result.data;
              $scope.residents = result.data;

              var SelfEmployed = [];
              angular.forEach(residents, function(value, key) {

                if(value.employmentstatus === 'SelfEmployed'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, SelfEmployed);

                  $scope.SelfEmployed = SelfEmployed;


              var Unemployed = [];
              angular.forEach(residents, function(value, key) {

                if(value.employmentstatus === 'Unemployed'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, Unemployed);

                  $scope.Unemployed = Unemployed;

              var OutofSchoolYouth = [];
              angular.forEach(residents, function(value, key) {

                if(value.employmentstatus === 'OutofSchoolYouth'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, OutofSchoolYouth);

                  $scope.OutofSchoolYouth = OutofSchoolYouth;

              var Retired = [];
              angular.forEach(residents, function(value, key) {

                if(value.employmentstatus === 'Retired'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, Retired);

                  $scope.Retired = Retired;

              var rgovernmnet = [];
              angular.forEach(residents, function(value, key) {

                if(value.employmentype === 'Government'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, rgovernmnet);

                  $scope.rgovernmnet = rgovernmnet;

              var rPrivate = [];
              angular.forEach(residents, function(value, key) {

                if(value.employmentype === 'Private'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, rPrivate);

                  $scope.rPrivate = rPrivate;

              var rmale = [];
              angular.forEach(residents, function(value, key) {

                if(value.prof_gender === 'Male'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, rmale);

                  $scope.maleresident = rmale;

              var rfemale = [];
              angular.forEach(residents, function(value, key) {

                if(value.prof_gender === 'Female'){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, rfemale);

                  $scope.femaleresident = rfemale;


            });

      // return the name of the house hold member
        $scope.getresidentname= function(getString){

          if(getString === undefined){
            return false;
          }
                var returnname = getString.split(',');
                return returnname[1];
        }

        $scope.generateBarangaySurvey = function() {

          // function to be used from table 1 until last table
          $scope.getTotalPerSitio = function(sitioObject) {

            var profSitioArray = [];

            $.each(sitioObject, function(key, value) {

              //check if prof_sitio exists in object, if none use brgy_Sitiopurok or sitiopurok
              var profSitio = (this.prof_sitio) ? this.prof_sitio : this.brgy_Sitiopurok || this.sitiopurok || this.business_address;

              // for some prof_sitio with no ID
              if(profSitio.indexOf(',') > -1) {
                profSitio = profSitio.slice(26);
              }

              profSitioArray.push(profSitio);

            });

            // will merge all Sitio with same name and count how many values (ex. Bitoon: 7)
            profSitioArray = profSitioArray.reduce( function (prev, item) {
              if ( item in prev ) prev[item] ++;
              else prev[item] = 1;
              return prev;
            }, {} );

            return profSitioArray;

          }

          // object to be compared to data - default value 0 if no sitio found (not used for now)
          // var completeSitio = { bidlisiw: 0, bitoon: 0, mangostan: 0, poblacion: 0, seaside: 0, taloot: 0};

          var columns = [];
          var data = [];

          // loop through the 13 tables and assign columns and data for each (tables initiated at the very bottom "doc.autoTable")
          for(var i = 1; i <= 15; i++) {

            switch(i) {

              case 1:
                data[i] =  [
                  { "name": "Population" }
                ];
                var obj = $scope.residents;
                break;

              case 2:
                data[i] =  [
                  { "name": "Households" }
                ];
                var obj = $scope.gethouseholdsetiopurok;
                break;

              case 3:
                data[i] =  [
                  { "name": "Transcients" }
                ];
                var obj = $scope.GetAllTranscients;
                break;

              case 4:
                data[i] =  [
                  { "name": "Male" },
                  { "name": "Female" }
                ];
                var obj1 = $scope.maleresident;
                var obj2 = $scope.femaleresident;
                break;

              case 5:
                data[i] =  [
                  { "name": "0-79 months old" }
                ];
                var obj = $scope.getchild;
                break;

              case 6:
                data[i] =  [
                  { "name": "W/out C.R." }
                ];
                var obj = $scope.eCR;
                break;

              case 7:
                data[i] =  [
                  { "name": "W/out Water" }
                ];
                var obj = $scope.eWater;
                break;

              case 8:
                data[i] =  [
                  { "name": "W/out Electricity" }
                ];
                var obj = $scope.eElectricity;
                break;

              case 9:
                data[i] =  [
                  { "name": "Self Employed" }
                ];
                var obj = $scope.SelfEmployed;
                break;

              case 10:
                data[i] =  [
                  { "name": "Unemployed" }
                ];
                var obj = $scope.Unemployed;
                break;

              case 11:
                data[i] =  [
                  { "name": "Out of School Youth" }
                ];
                var obj = $scope.OutofSchoolYouth;
                break;

              case 12:
                data[i] =  [
                  { "name": "Retired" }
                ];
                var obj = $scope.Retired;
                break;

              case 13:
                data[i] =  [
                  { "name": "Government" },
                  { "name": "Private" }
                ];
                var obj1 = $scope.rgovernmnet;
                var obj2 = $scope.rPrivate;
                break;

              case 14:
                data[i] =  [
                  { "name": "Total No. Of Establishments" }
                ];
                var obj = $scope.getallestablishment;
                break;

              case 15:
                data[i] =  [
                  { "name": "0-79 MONTHS OLD CHILD" }
                ];
                var obj = $scope.getchild;
                break;

            }

            if(i == 4) {
              columns[i] = [
                { title: "Gender", key: "name", width: 100 },
              ];

              $.each($scope.getTotalPerSitio(obj1), function(sitio, value) {

                var sitioName = sitio;
                sitio =  sitio.toLowerCase();

                columns[i].push(
                  { title: "Sitio " + sitioName, key: sitio, width: 100 }
                );

                // add properties to current object in data
                data[i][0][sitio] = value;

              });

              $.each($scope.getTotalPerSitio(obj2), function(sitio, value) {

                sitio =  sitio.toLowerCase();
                data[i][1][sitio] = value;

              });

              columns[i].push(
                { title: "TOTAL", key: "total", width: 100 }
              );

              data[i][0]["total"] = obj1.length;
              data[i][1]["total"] = obj2.length;
            }

            else if(i == 13) {

              columns[i] = [
                { title: "Employed", key: "name", width: 100 },
              ];

              $.each($scope.getTotalPerSitio(obj1), function(sitio, value) {

                var sitioName = sitio;
                sitio =  sitio.toLowerCase();

                columns[i].push(
                  { title: "Sitio " + sitioName, key: sitio, width: 100 }
                );

                // add properties to current object in data
                data[i][0][sitio] = value;

              });

              $.each($scope.getTotalPerSitio(obj2), function(sitio, value) {

                sitio =  sitio.toLowerCase();
                data[i][1][sitio] = value;

              });

              columns[i].push(
                { title: "TOTAL", key: "total", width: 100 }
              );

              data[i][0]["total"] = obj1.length;
              data[i][1]["total"] = obj2.length;
            }

            else {
              columns[i] = [
                { title: "Label", key: "name", width: 100 },
              ];

              $.each($scope.getTotalPerSitio(obj), function(sitio, value) {

                var sitioName = sitio;
                sitio =  sitio.toLowerCase();

                columns[i].push(
                  { title: "Sitio " + sitioName, key: sitio, width: 100 }
                );

                // add properties to current object in data
                data[i][0][sitio] = value;

              });

              columns[i].push(
                { title: "TOTAL", key: "total", width: 100 }
              );

              data[i][0]["total"] = obj.length;
            }

          }

          var doc = new jsPDF('l', 'pt', 'legal');
          doc.setFontSize(14);

          // for custom style
          var header = function (x, y, width, height, key, value, settings) {
            doc.setFont("helvetica");
            doc.setFillColor(217, 237, 247);
            doc.setTextColor(49, 112, 159);
            doc.setFontSize(14);
            doc.rect(x, y, width, height, 'F');
            y += settings.lineHeight / 2 + doc.internal.getLineHeight() / 2 - 2.5;
            doc.text('' + value, x + settings.padding, y);
          };

          var barangayName = $('#barangay-name-span').text();
          var sYear = $('#sYear-span').text();
          doc.text("BARANGAY " + barangayName + " SURVEY AS OF " + sYear, 350, 70);

          // 1st table initialize (POPULATION)
          doc.autoTable(columns[1], data[1], {
            startY: 85,
            renderHeaderCell: header,
          });

          // 2nd table initialize (HOUSEHOLDS)
          doc.autoTable(columns[2], data[2], {
            startY: 145,
            renderHeaderCell: header,
          });

          // 3rd table initialize (TRANSCIENTS)
          doc.autoTable(columns[3], data[3], {
            startY: 205,
            renderHeaderCell: header,
          });

          // 4th table initialize (GENDER)
          doc.autoTable(columns[4], data[4], {
            startY: 265,
            renderHeaderCell: header,
          });

          // 5th table initialize (0-79 MONTHS OLD)
          doc.autoTable(columns[5], data[5], {
            startY: 345,
            renderHeaderCell: header,
          });

          // 6th table initialize (W/OUT C.R.)
          doc.autoTable(columns[6], data[6], {
            startY: 405,
            renderHeaderCell: header,
          });

          // 7th table initialize (W/OUT WATER)
          doc.autoTable(columns[7], data[7], {
            startY: 465,
            renderHeaderCell: header,
          });

          doc.addPage();

          // 8th table initialize (W/OUT ELECTRICITY)
          doc.autoTable(columns[8], data[8], {
            startY: 85,
            renderHeaderCell: header,
          });

          // 9th table initialize (SELF EMPLOYED)
          doc.autoTable(columns[9], data[9], {
            startY: 145,
            renderHeaderCell: header,
          });

          // 10th table initialize (UNEMPLOYED)
          doc.autoTable(columns[10], data[10], {
            startY: 205,
            renderHeaderCell: header,
          });

          // 11th table initialize (OUT OF SCHOOL YOUTH)
          doc.autoTable(columns[11], data[11], {
            startY: 265,
            renderHeaderCell: header,
          });

          // 12th table initialize (RETIRED)
          doc.autoTable(columns[12], data[12], {
            startY: 325,
            renderHeaderCell: header,
          });

          // 13th table initialize (GOVERNMENT OR PRIVATE)
          doc.autoTable(columns[13], data[13], {
            startY: 385,
            renderHeaderCell: header,
          });

          // 14th table initialize (ESTABLISHMENTS)
          doc.autoTable(columns[14], data[14], {
            startY: 475,
            renderHeaderCell: header,
          });
          doc.addPage();
          // 15th table initialize (0-79 MONTHS OLD CHILD)
          doc.autoTable(columns[15], data[15], {
            startY: 25,
            renderHeaderCell: header,
          });

          doc.save(barangayName + '.pdf'); //generate PDF

        }

});