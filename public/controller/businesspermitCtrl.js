'use strict';

var app = angular.module('brgyapp');

app.controller('businesspermitCtrl', function($scope,  $location, $http,
  ItemsServices, $rootScope, $window, $modal,
  DbCollection, BusinessCollections, BusinessPermitFactory,
  BusinessFactory, Restangular, SitioPurokService, EstablishmentsFactory){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

    $scope.page = 0;
    var pagenow = 0;
    var startPage = 0;

    EstablishmentsFactory.GetAllEstablishments()
    .then(function(resultdata){
        if(resultdata.data.length < 5){
          $scope.pagehide = true;
        }else{
          $scope.pagehide = false;
        }
        $scope.getestablishments = resultdata.data;
    });

    function getthelist(pagenow){

        var init = pagenow;
        return EstablishmentsFactory.getestablishmentlimit(init)
        .then(function(resultlimit){
           $scope.getlimitestablishment = resultlimit.data;
        });

    }

    getthelist(pagenow);

    if($scope.page == 0){
      $scope.prevdisable = true;

    }

    $scope.previouspage = function(pagedown, pagetotal){

        $scope.datalengthnow = pagedown * 10;

          $scope.page = pagedown;

          if(pagedown == 0){

            $scope.prevdisable = true;

          }

          if(pagenow < pagetotal / 10){

            $scope.nextdisable = false;

          }

          return getthelist(pagedown);

      }

    $scope.nextpage = function(pagenow, pagetotal){

      $scope.datalengthnow = pagenow * 10;

      $scope.page = pagenow;

      var lastpage = pagetotal / 10;

      if(lastpage % 1 != 0) {
        lastpage = Math.round(lastpage) + 1;
      }

        if(pagenow + 1 == lastpage){

          $scope.nextdisable = true;

        }

        if(pagenow > 0){

          $scope.prevdisable = false;

        }

        return getthelist(pagenow);

    }

    function business_list(){
          BusinessFactory.GetAllBusiness()
          .then(function(results){
            return $scope.businesslist = results.data;
          })
    }

    business_list();

// return the name of the house hold member
    $scope.getresidentname= function(getString){

      if(getString === undefined){
        return false;
      }
            var returnname = getString.split(',');
            return returnname[1];
    }

// return the id of the house hold member
    $scope.getresidentId= function(getString){

      if(getString === undefined){
        return false;
      }

            var returnId = getString.split(',');
            return returnId[0];
    }

    $scope.addnewowner = function (size) {

          var modalInstance = $modal.open({
            templateUrl: '../views/businesspermit/businessownerform.html',
            controller: $scope.addnewownerCtrl,
            size: size
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {

            });
    };

    $scope.addnewownerCtrl = function($scope, $modalInstance, BusinessFactory, $http){

          $scope.formheading = "BUSINESS FORM"

         $scope.take_snapshot = function(data_uri) {
                Webcam.snap( function(data_uri) {
                    $scope.brgybusiness.imageuri = data_uri;
                    document.getElementById('capture_image').innerHTML =
                        '<img id="myImg" src="'+data_uri+'"/>' +
                        '<input type="hidden" id="resulturi" ng-model="brgybusiness.imageuri" value="'+
                        $scope.brgybusiness.imageuri +'" >';
                });

                    var x = document.getElementById("myImg").src;
                    var x_result = x.toString();
       }

      SitioPurokService.GetAllSitioPurok()
      .then(function(getlistresult){
        $scope.GetAllSitioPuroklist = getlistresult.data;
      })

      $scope.savebusiness = function(){

          $scope.brgybusiness.region = $rootScope.UserAcount.region;
          $scope.brgybusiness.province = $rootScope.UserAcount.province;
          $scope.brgybusiness.municipality = $rootScope.UserAcount.municipality;
          $scope.brgybusiness.barangay = $rootScope.UserAcount.barangay;

          BusinessFactory.CreateBusiness($scope.brgybusiness);
          $modalInstance.dismiss('cancel');
          business_list();

      };

       $scope.children_me = [];
        $http.get(DbCollection + 'judicial_resident/')
              .then(function(resultme){
              $scope.children_me = resultme.data;
        });

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

    };

    $scope.deletebusiness = function (id) {

      var con = confirm("Are you sure you want to delete this business permit?");

      if(con) {

         BusinessFactory.DeleteBusiness(id);
          business_list();
      }

      else {
        return false;
      }

    };

    $scope.editbusiness = function (size, id) {


      var modalInstance = $modal.open({
        templateUrl: '../views/businesspermit/businessownerform.html',
        controller: $scope.editbusinessCtrl,
        size: size,
        resolve: {
          Businesspermit: function(BusinessFactory){
            if(id){
              return BusinessFactory.GetBusinessID(id);
            }else{
              return null;
            }
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.editbusinessCtrl = function($scope, $modalInstance, Businesspermit, BusinessFactory, $http){

          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

         $scope.take_snapshot = function(data_uri) {
                Webcam.snap( function(data_uri) {
                    $scope.brgybusiness.imageuri = data_uri;
                    document.getElementById('capture_image').innerHTML =
                        '<img id="myImg" src="'+data_uri+'"/>' +
                        '<input type="hidden" id="resulturi" ng-model="brgybusiness.imageuri" value="'+
                        $scope.brgybusiness.imageuri +'" >';
                });

                    var x = document.getElementById("myImg").src;
                    var x_result = x.toString();
       }

      var id = Businesspermit.data._id;

      $scope.brgybusiness = Businesspermit.data;

      $scope.formheading = "Update Record";

      $scope.updatebusiness = function(){

          $scope.brgybusiness.region = $rootScope.UserAcount.region;
          $scope.brgybusiness.province = $rootScope.UserAcount.province;
          $scope.brgybusiness.municipality = $rootScope.UserAcount.municipality;
          $scope.brgybusiness.barangay = $rootScope.UserAcount.barangay;

          BusinessFactory.UpdateBusiness(id, $scope.brgybusiness);
          $modalInstance.dismiss('cancel');
           business_list();
      };

      $scope.children_me = [];
        $http.get(DbCollection + 'judicial_resident/')
              .then(function(resultme){
              $scope.children_me = resultme.data;
        });

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

    };

    $scope.goestablishments = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/businesspermit/businessestablishments.html',
        controller: $scope.viewestablishmentsCtrl,
        size: size,
        resolve: {
          getowner: function(BusinessFactory){
            if(id){
              return BusinessFactory.GetBusinessID(id);
            }else{
              return null;
            }
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.viewestablishmentsCtrl = function($scope, $modalInstance, getowner, BusinessFactory, $http){

        $scope.currentPage = 1;
        $scope.pageSize = 6;

        var id = getowner.data._id;

        $scope.getowner = getowner.data;

        function getmyestablishments(){
              EstablishmentsFactory.GetMyEstablishmentsID(id)
              .then(function(dataresult){
                $scope.getmyestablishments = dataresult.data;
              })
        };

        getmyestablishments();

      $scope.getendDate = function(getenddate){
        var enddate = new Date(getenddate);
        return enddate;
      }

      $scope.datenow = Date.now();

      // return the name of the house hold member
          $scope.getstioname= function(getString){

            if(getString === undefined){
              return false;
            }
                  var returnname = getString.split(',');
                  return returnname[1];
          }

      // return the id of the house hold member
          $scope.getstiotId= function(getString){

            if(getString === undefined){
              return false;
            }

                  var returnId = getString.split(',');
                  return returnId[0];
          }

    $scope.createpermits = function(size, id, esId){

        var modalInstance = $modal.open({
          templateUrl: '../views/businesspermit/addnewpermit.html',
          controller: $scope.addnewPermits,
          size: size,
          resolve: {
           GetEstablishment: function(EstablishmentsFactory){
              if(id){
                return EstablishmentsFactory.GetEstablishmentsID(id);
              }else{
                return null;
              }
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          }, function () {

        });

    }
     $scope.addnewPermits = function($scope, $modalInstance, GetEstablishment, $http){

                 $scope.savepermit = function(){

                  $scope.businesspermit.ownerId = GetEstablishment.data.ownerId;
                  $scope.businesspermit.ownerfullname = GetEstablishment.data.ownername;
                  $scope.businesspermit.establishmentsId = GetEstablishment.data._id;
                  $scope.businesspermit.business_name = GetEstablishment.data.business_name;
                  $scope.businesspermit.business_type = GetEstablishment.data.business_type;

                      var Exdate = $scope.businesspermit.date_end;
                      var exmonth = ItemsServices.getmonth(Exdate);
                      var exvalue = ItemsServices.getdatevalue(Exdate);
                      var exyear = ItemsServices.get_issuedYear(Exdate);

                      $scope.businesspermit.Exvalue = exvalue;
                      $scope.businesspermit.Exmonth = exmonth;
                      $scope.businesspermit.Exyear = exyear;
                      $scope.businesspermit.ExString= exmonth +' '+ exvalue +', '+ exyear;

                  // post new permit
                  BusinessPermitFactory.CreateBusinessPermit($scope.businesspermit)
                  .then(function(resultdata){
                    var getresult = resultdata.data;
                  }, function(error){
                    var error = error.message;
                  })

                  $scope.establishments = GetEstablishment.data;
                  $scope.establishments.haspermit = $scope.businesspermit;

                  // updating the establishments
                  EstablishmentsFactory.UpdateEstablishments(GetEstablishment.data._id, $scope.establishments)
                  .then(function(resultdata){
                    console.log(resultdata);
                     getmyestablishments();
                    $modalInstance.dismiss('cancel');
                  })

                }

                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
                };

     }

    $scope.editEstablishment = function (size, OwnerId, Esid) {

      var modalInstance = $modal.open({
        templateUrl: '../views/businesspermit/addestablishments.html',
        controller: $scope.editEstablishmentCtrl,
        size: size,
        resolve: {
          BusinessOwner: function(BusinessFactory){
            if(id){
              return BusinessFactory.GetBusinessID(OwnerId);
            }else{
              return null;
            }
          },
          GetEstablishment: function(EstablishmentsFactory){
            if(id){
              return EstablishmentsFactory.GetEstablishmentsID(Esid);
            }else{
              return null;
            }
          },
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
      });

    };

      $scope.editEstablishmentCtrl = function($scope, $modalInstance, GetEstablishment, BusinessOwner, $http){

           $scope.take_snapshot = function(data_uri) {
                Webcam.snap( function(data_uri) {
                    $scope.establishments.imageuri = data_uri;
                    document.getElementById('capture_image').innerHTML =
                        '<img id="myImg" src="'+data_uri+'"/>' +
                        '<input type="hidden" id="resulturi" ng-model="establishments.imageuri" value="'+
                        $scope.establishments.imageuri +'" >';
                });

                    var x = document.getElementById("myImg").src;
                    var x_result = x.toString();
           }

          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

           $scope.establishments = GetEstablishment.data;
           $scope.getBusinessOwner = BusinessOwner.data;
           $scope.establishments.ownername =  BusinessOwner.data._id +', '+ BusinessOwner.data.ownerfullname;
           $scope.establishments.ownerId =  BusinessOwner.data._id;

            $scope.updateEstablishment = function(id){
              EstablishmentsFactory.UpdateEstablishments(id, $scope.establishments)
              .then(function(resultdata){
                console.log(resultdata);
                 getmyestablishments();
                $modalInstance.dismiss('cancel');
              })
            }

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

      }

    $scope.NewEstablishment = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/businesspermit/addestablishments.html',
        controller: $scope.addestablishmentCtrl,
        size: size,
        resolve: {
          BusinessOwner: function(BusinessFactory){
            if(id){
              return BusinessFactory.GetBusinessID(id);
            }else{
              return null;
            }
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
      });

    };



      $scope.addestablishmentCtrl = function($scope, $modalInstance, BusinessOwner, $http){

         $scope.take_snapshot = function(data_uri) {
                Webcam.snap( function(data_uri) {
                    $scope.establishments.imageuri = data_uri;
                    document.getElementById('capture_image').innerHTML =
                        '<img id="myImg" src="'+data_uri+'"/>' +
                        '<input type="hidden" id="resulturi" ng-model="establishments.imageuri" value="'+
                        $scope.establishments.imageuri +'" >';
                });

                    var x = document.getElementById("myImg").src;
                    var x_result = x.toString();
       }

          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

         $scope.getBusinessOwner = BusinessOwner.data;

          $scope.saveestablishment = function(){

           $scope.establishments.ownername =  BusinessOwner.data._id +', '+ BusinessOwner.data.ownerfullname;
             $scope.establishments.ownerId =  BusinessOwner.data._id;
            EstablishmentsFactory.CreateEstablishments($scope.establishments)
            .then(function(resultdata){
              console.log(resultdata.data);
              getmyestablishments();
              $modalInstance.dismiss('cancel');
            })
          }

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

      }

      $scope.generatePermitPDF = function(sid, oid){

      // return the name of the house hold member
          function getstioname(getString){

            if(getString === undefined){
              return false;
            }
                  var returnname = getString.split(',');
                  return returnname[1];
          }

                EstablishmentsFactory.GetEstablishmentsID(sid)
                .then(function(resultdata){
                  var getpermit = resultdata.data;
                      BusinessFactory.GetBusinessID(oid)
                      .then(function(resultdataowner){
                        var owner = resultdataowner.data;

                            var formatDate = new Date(getpermit.haspermit.Issued);
                            var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                            var month = String(month[formatDate.getMonth()]);
                            var day = String(formatDate.getDate());
                            var year = String(formatDate.getFullYear());

                            var barangay = $rootScope.UserAcount.barangay.barangay_name;
                            var municipality = $rootScope.UserAcount.municipality.municipality_name;
                            var province = $rootScope.UserAcount.province.province_name;
                            var region = $rootScope.UserAcount.region.region_name;
                            var mlogo = $rootScope.UserAcount.mlogo;

                            var businesspermitdetails = {
                                business_owner : getstioname(getpermit.ownername),
                                business_name : getpermit.business_name,
                                business_type : getpermit.business_type,
                                business_address : getstioname(getpermit.business_address),
                                optnewrenewal : getpermit.haspermit.optnewrenewal,
                                optpermittype : getpermit.haspermit.optpermittype,
                                controlnumber : getpermit.haspermit.controlnumber.toString(),
                                certificationfee : getpermit.haspermit.certificationfee.toString(),
                                ornumber : getpermit.haspermit.ornumber.toString(),
                                com_tax_no : getpermit.haspermit.com_tax_no.toString(),
                                imageuri : owner.imageuri,
                                dateCreated : getpermit.haspermit.Issued,
                                owner_address : owner.owner_address,
                                captain : $rootScope.UserAcount.captain,
                                address : barangay+', '+municipality+', '+ province,
                                captainbase :  $rootScope.UserAcount.captain,
                                month: month,
                                Qrme : qr.toDataURL(getpermit.shortIds),
                                day: day,
                                year: year
                            };

                                      // generating goes here
                                      // short bond paper format size
                                      // var doc = new jsPDF('p', 'mm', [215.9, 279.4]);

                                      // long bond paper format size
                                      var doc = new jsPDF('p', 'mm', [215.9, 330.2]);

                                      // var doc = new jsPDF('p', 'mm', [215.9, 330.2]);

                                      doc.setFont("Arial");
                                      doc.setFontSize(14);
                                      doc.setDrawColor(0,0,0);
                                      doc.setLineWidth(0.3);
                                      // doc.setFontType("bold");
                                      doc.text(77, 18, "Republic of the Philippines");
                                      doc.text(84, 24, "Province of Siquijor");
                                      doc.text(80, 30, "Municipality of Siquijor");
                                      doc.text(84, 36, "Barangay Poblacion");

                                    doc.addImage(mlogo, 'JPEG', 25, 7, 30, 30);
                                    doc.addImage(businesspermitdetails.imageuri, 'PNG', 160, 5, 35, 35);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.3);
                                    doc.line(80, 43, 135, 43);


                                    doc.setFontSize(17);
                                    doc.text(35, 52, "B A R A N G A Y  B U S I N E S S  C L E A R A N C E");

                                    doc.setFont("Arial");
                                    doc.setFontType("regular");
                                    doc.setFontSize(13);

                                    doc.text(43, 59, "This is to certify that the business or trade activity described below.");


                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(53, 71, 158, 71);

                                    doc.text(89, 75, "(Type of Business)");
                                    doc.text(85, 69, businesspermitdetails.business_type);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(53, 87, 158, 87);

                                    doc.text(75, 92, "(Business Name or Trade Activity)");
                                    doc.text(80, 86, businesspermitdetails.business_name);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(53, 105, 158, 105);

                                    doc.text(95, 110, "(Location)");
                                    doc.text(80, 104, businesspermitdetails.business_address);


                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(53, 120, 158, 120);

                                    doc.text(86, 125, "(Operator/Manager)");
                                    doc.text(80, 119, businesspermitdetails.business_owner);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(53, 135, 158, 135);

                                    doc.text(96, 140, "(Address)");
                                    doc.text(80, 134, businesspermitdetails.owner_address);

                                    doc.text(25, 160, "Being applied for New/Renewal of the corresponding Mayor's permit been found to be.");

                                    // for underline whether New or Renewal (1: New, 2: Renewal)
                                    if(businesspermitdetails.optnewrenewal == '1') {
                                      doc.line(58, 161, 67, 161);
                                    }

                                    else {
                                      doc.line(68, 161, 84, 161);
                                    }

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(25, 170, 55, 170);

                                    doc.text(56, 170, "/Complying with the provision of existing Barangay Ordinance rules and");
                                    doc.text(58, 175, "regulations being enforced in this Barangay.");

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(25, 185, 55, 185);

                                    doc.text(56, 185, "/Partially complying with the provision of existing Barangay Ordinances,");
                                    doc.text(58, 190, "rules and regulations being enforced in this Barangay.");


                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(25, 200, 55, 200);

                                    doc.text(56, 200, "/In view of the foregoing, this Barangay, thru the undersigned-");

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(25, 210, 55, 210);

                                    doc.text(56, 210, "/Interposes No objection for the issuance of the corresponding Mayors");
                                    doc.text(58, 215, "being applied for.");


                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(25, 225, 55, 225);

                                    doc.text(56, 225, "/Recommended only the issuance of a 'Temporary Mayor's Permit' ");
                                    doc.text(58, 230, "for not more than three (3) months and within that the period");
                                    doc.text(58, 235, "requirements under existing Barangay ordinances, rules and ");
                                    doc.text(58, 240, "regulation on that matter should be totally complied with, otherwise");
                                    doc.text(58, 245, "this Barangay would take the Necessary actions, within legal bounds, ");
                                    doc.text(58, 250, "to stop each continued operations.");

                                    // put "x" on blank for optpermittype whether 1,2,3,4, or 5
                                    doc.setFontSize(20);

                                    if(businesspermitdetails.optpermittype == '1') {
                                      doc.text(38, 169, "x");
                                    }

                                    else if(businesspermitdetails.optpermittype == '2') {
                                      doc.text(38, 184, "x");
                                    }

                                    else if(businesspermitdetails.optpermittype == '3') {
                                      doc.text(38, 199, "x");
                                    }

                                    else if(businesspermitdetails.optpermittype == '4') {
                                      doc.text(38, 209, "x");
                                    }

                                    else {
                                      doc.text(38, 224, "x");
                                    }

                                    doc.setFontSize(13);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.5);
                                    doc.line(139, 270, 190, 270);

                                    doc.addImage(businesspermitdetails.Qrme, 'PNG', 25, 260, 20, 20);
                                    doc.text(140, 268, businesspermitdetails.captainbase);
                                    doc.text(148, 274, "Punong Barangay");


                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.3);
                                    doc.line(45, 288, 79, 288);
                                    doc.text(47, 287, businesspermitdetails.ornumber);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.3);
                                    doc.line(45, 295, 79, 295);
                                    doc.text(47, 294, businesspermitdetails.month +' '+businesspermitdetails.day+', '+businesspermitdetails.year);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.3);
                                    doc.line(45, 302, 79, 302);
                                    doc.text(47, 302, "P " + businesspermitdetails.certificationfee + ".00");

                                    doc.text(25, 288, "O.R. No. : ");
                                    doc.text(25, 295, "DATE Pd : ");
                                    doc.text(25, 302, "Amt. Pd : ");


                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.3);
                                    doc.line(155, 288, 190, 288);
                                     doc.text(155, 287, businesspermitdetails.com_tax_no);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.3);
                                    doc.line(155, 295, 190, 295);
                                    doc.text(155, 294, businesspermitdetails.month +' '+businesspermitdetails.day+', '+businesspermitdetails.year);

                                    doc.setDrawColor(0,0,0);
                                    doc.setLineWidth(.3);
                                    doc.line(155, 302, 190, 302);
                                    doc.text(155, 301, businesspermitdetails.business_address);

                                    doc.text(110, 288, "Community Tax No : ");
                                    doc.text(110, 295, "Issued On : ");
                                    doc.text(110, 302, "Issued At. : ");


                                      doc.save(businesspermitdetails.business_owner +'.pdf');

                                      // $modalInstance.dismiss('cancel');

                      })

                });

        }

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

    };

})