'use strict';

var app = angular.module('brgyapp');

app.controller('generalSurveyCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, SurverReportCollections,
  SitioPurokService, transFactory, familiesServices, ReportService,
  householdservice, SurveyReportServices, EstablishmentsFactory){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

var d = new Date();
$rootScope.SYear = d.getFullYear();

        $scope.surveyhead = ItemsServices.SurveyHead();
        $scope.surveyheademployed =  ItemsServices.SurveyResidentEmployedStatus();
        $scope.surveyheadmore =  ItemsServices.SurveyResidentStatus();
        $scope.SurveyResidentMoreStatus = ItemsServices.SurveyResidentMoreStatus();
        $scope.SurveyResidentPhysicalStatus = ItemsServices.SurveyResidentPhysicalStatus();

              SitioPurokService.GetAllSitioPurok()
              .then(function(getlistresult){
                $scope.GetAllSitioPuroks = getlistresult.data;
              })

        householdservice.GetAllHousehold()
        .then(function(resultdata){
             $scope.allfamiies = resultdata.data;
             var  invalidEntries;

               $scope.cr = function(obj) {

                      if ('CR' in obj && obj.CR == 'No' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }
                }

               $scope.Water = function(obj) {

                      if ('Water' in obj && obj.Water == 'No' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }
                }

               $scope.Electricity = function(obj) {

                      if ('Electricity' in obj && obj.Electricity == 'No' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }
                }


               $scope.eWater = $scope.allfamiies.filter($scope.Water);
               $scope.ecr = $scope.allfamiies.filter($scope.cr);
               $scope.eElectricity = $scope.allfamiies.filter($scope.Electricity);


        })

      //get all seventynine months old child
        ReportService.getseventyninemonths()
        .then(function(resultchild){
          $scope.getchild = resultchild;
        });

        // get all house hod info.
        householdservice.GetAllHousehold()
        .then(function(resultdata){
              $scope.gethouseholdsetiopurok = resultdata.data;
        });

        $http.get(DbCollection + 'resident/')
         .then(function(result){
          $scope.residents = result.data;
        });
      // get all Establishments
        EstablishmentsFactory.GetAllEstablishments()
        .then(function(resultdata){
           $scope.getallestablishment =  resultdata.data;
        })

      // return the name of the house hold member
        $scope.getresidentname= function(getString){

          if(getString === undefined){
            return false;
          }
                var returnname = getString.split(',');
                return returnname[1];
        }

      // return the id of the house hold member
        $scope.getresidentId= function(getString){

              if(getString === undefined){
                return false;
              }
        }

        // get all residents info
        $http.get(DbCollection + 'resident/')
         .then(function(result){
          var residents = result.data;

                var invalidEntries = 0;

               $scope.Government = function(obj, sitioid) {

                      if ('employmentype' in obj && obj.employmentype === 'Government' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }

                }

               $scope.Private = function(obj, sitioid) {

                      if ('employmentype' in obj && obj.employmentype === 'Private' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }

                }

               $scope.SelfEmployed = function(obj, sitioid) {

                      if ('employmentstatus' in obj && obj.employmentstatus === 'SelfEmployed' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }

                }

               $scope.Unemployed = function(obj, sitioid) {

                      if ('employmentstatus' in obj && obj.employmentstatus === 'Unemployed' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }

                }

               $scope.OutofSchoolYouth = function(obj, sitioid) {

                      if ('employmentstatus' in obj && obj.employmentstatus === 'OutofSchoolYouth' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }

                }

               $scope.Retired = function(obj, sitioid) {

                      if ('employmentstatus' in obj && obj.employmentstatus === 'Retired' ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }

                }

               $scope.pwd = function(obj, sitioid) {

                      if ('PersonWithDisability' in obj && obj.PersonWithDisability == true ) {
                        return true;
                      } else {
                        invalidEntries++;
                        return false;
                      }

                }

              var log = [];
              angular.forEach(residents, function(value, key) {

                if(ItemsServices.calculate_age(value.b_date)  >= 60){
                            this.push({
                              id: value._id,
                              prof_sitio : value.prof_sitio,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, log);

              $scope.seniorcitizen = log;
               $scope.eGovernment = residents.filter($scope.Government);
               $scope.ePrivate = residents.filter($scope.Private);
               $scope.eSelfEmployed = residents.filter($scope.SelfEmployed);
               $scope.eUnemployed = residents.filter($scope.Unemployed);
               $scope.eOutofSchoolYouth = residents.filter($scope.OutofSchoolYouth);
               $scope.eRetired = residents.filter($scope.Retired);
               $scope.epwd = residents.filter($scope.pwd);

         });

         $scope.getsign = function(){
          var number = 1
              return number;
         }

        // get all Trancients info.
        transFactory.GetAllTranscends()
        .then(function(resultdata){
            $scope.GetAllTranscients = resultdata.data;
        });

        // first table of generation
        $scope.getgeneraltotal = function(indicator){

            if(indicator === 'Households'){
                return $scope.gethouseholdsetiopurok;
            }

            if(indicator === 'Population'){
                return $scope.residents
            }

            if(indicator === 'Trancients'){
                return $scope.GetAllTranscients;
            }

            if(indicator === 'Male'){

              var values = $scope.residents;
              var log = [];

              angular.forEach(values, function(value, key) {

                if(value.prof_gender === 'Male'){
                            this.push({
                              id: value._id,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, log);

                $scope.getresidentresultgendersetiopurok = log;
                $scope.getresidentresultmalesetiopurok = log;
                return $scope.getresidentresultgendersetiopurok;

            }

            if(indicator === 'Female'){

              var values = $scope.residents;
              var log = [];

              angular.forEach(values, function(value, key) {

                if(value.prof_gender === 'Female'){
                            this.push({
                              id: value._id,
                              fullname: value.fullname,
                              bdate: value.b_date
                            });
                      }
                }, log);

                $scope.getresidentresultgendersetiopurok = log;
                $scope.getresidentresultfemalesetiopurok = log;
                return $scope.getresidentresultgendersetiopurok;

            }
        }

        $scope.generateGeneralSurvey = function() {

          // 1st table
          var columns = [
            {title: "", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data = [];

          $.each($scope.surveyhead, function(key, value) {

            data.push(
              { "name": "Total No. of " + value.indicator, "total": $scope.getgeneraltotal(value.indicator).length }
            );

          });

          // // function to be used from table 2 until last table
          $scope.getTotalPerSitio = function(sitioObject) {

            var profSitioArray = [];

            $.each(sitioObject, function(key, value) {
              var profSitio = (this.prof_sitio) ? this.prof_sitio : this.brgy_Sitiopurok || this.sitiopurok || this.business_address; //check if prof_sitio exists in object, if none use brgy_Sitiopurok

              // for some prof_sitio with no ID
              if(profSitio.indexOf(',') > -1) {
                profSitio = profSitio.slice(26);
              }

              profSitioArray.push(profSitio);

            });

            // will merge all Sitio with same name and count how many values (ex. Bitoon: 7)
            profSitioArray = profSitioArray.reduce( function (prev, item) {
              if ( item in prev ) prev[item] ++;
              else prev[item] = 1;
              return prev;
            }, {} );

            return profSitioArray;

          }

          // 2nd table
          var columns2 = [
            {title: "TOTAL NUMBER OF CHILDREN (0-79 MONTHS OLD)", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data2 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.getchild), function(key, value) {
              data2.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          console.log(data2);

          // 3rd table
          var columns3 = [
            {title: "TOTAL NUMBER OF PWD's", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data3 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.epwd), function(key, value) {
              data3.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 4th table
          var columns4 = [
            {title: "TOTAL NUMBER OF FAMILES W/OUT C.R.", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data4 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.ecr), function(key, value) {
              data4.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 5th table
          var columns5 = [
            {title: "TOTAL NUMBER OF FAMILES W/OUT WATER", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data5 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.eWater), function(key, value) {
              data5.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 6th table
          var columns6 = [
            {title: "TOTAL NUMBER OF FAMILES W/OUT ELECTRICITY", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data6 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.eElectricity), function(key, value) {
              data6.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 7th table
          var columns7 = [
            {title: "TOTAL NUMBER OF ELDERLY/SENIOR CITIZEN", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data7 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.seniorcitizen), function(key, value) {
              data7.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 8th table
          var columns8 = [
            {title: "TOTAL NUMBER OF FAMILIES", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data8 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.allfamiies), function(key, value) {
              data8.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 9th table
          var columns9 = [
            {title: "TOTAL NUMBER OF GOVERNMENT EMPLOYED", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data9 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.eGovernment), function(key, value) {
              data9.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 10th table
          var columns10 = [
            {title: "TOTAL NUMBER OF PRIVATE EMPLOYED", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data10 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.ePrivate), function(key, value) {
              data10.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 11th table
          var columns11 = [
            {title: "SELF EMPLOYED", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data11 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.eSelfEmployed), function(key, value) {
              data11.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 12th table
          var columns12 = [
            {title: "UNEMPLOYED", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data12 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.eUnemployed), function(key, value) {
              data12.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 13th table
          var columns13 = [
            {title: "OUT OF SCHOOL YOUTH", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data13 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.eOutofSchoolYouth), function(key, value) {
              data13.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 14th table
          var columns14 = [
            {title: "RETIRED", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data14 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.eRetired), function(key, value) {
              data14.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          // 15th table
          var columns15 = [
            {title: "Total No. Of Establishments", key: "name", width: 600},
            {title: "", key: "total", width: 100}
          ];

          var data15 = [
            { "name": "SITIO/PUROK", "total": "TOTAL" }
          ];

          $.each($scope.getTotalPerSitio($scope.getallestablishment), function(key, value) {
              data15.push(
                { "name": "Purok " + key, "total": value }
              );
          });

          var doc = new jsPDF('p', 'pt', 'letter');
          doc.setFontSize(14);

          // for custom style
          var header = function (x, y, width, height, key, value, settings) {
            doc.setFont("helvetica");
            doc.setFillColor(217, 237, 247);
            doc.setTextColor(49, 112, 159);
            doc.setFontSize(14);
            doc.rect(x, y, width, height, 'F');
            y += settings.lineHeight / 2 + doc.internal.getLineHeight() / 2 - 2.5;
            doc.text('' + value, x + settings.padding, y);
          };

          var barangayName = $('#barangay-name-span').text();
          var sYear = $('#sYear-span').text();
          doc.text("BARANGAY " + barangayName + " SURVEY AS OF " + sYear, 150, 70);

          // 1st table initialize (TOTALS)
          doc.autoTable(columns, data, {
            startY: 85,
            renderHeaderCell: header,
          });

          // 2nd table initialize (CHILDREN)
          doc.autoTable(columns2, data2, {
            startY: 230,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 390);
          doc.setFontStyle("bold");
          doc.text($scope.getchild.length + "", 495, 390);

          // 3rd table initialize (PWD's)
          doc.autoTable(columns3, data3, {
            startY: 410,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 590);
          doc.setFontStyle("bold");
          doc.text($scope.epwd.length + "", 495, 590);

          // 4th table initialize (W/OUT C.R.)
          doc.autoTable(columns4, data4, {
            startY: 610,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 710);
          doc.setFontStyle("bold");
          doc.text($scope.ecr.length + "", 495, 710);

          doc.addPage();

          // 5th table initialize (W/OUT WATER)
          doc.autoTable(columns5, data5, {
            startY: 70,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 210);
          doc.setFontStyle("bold");
          doc.text($scope.eWater.length + "", 495, 210);

          // 6th table initialize (W/OUT ELECTRICITY)
          doc.autoTable(columns6, data6, {
            startY: 230,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 350);
          doc.setFontStyle("bold");
          doc.text($scope.eElectricity.length + "", 495, 350);

          // 7th table initialize (SENIOR CITIZEN)
          doc.autoTable(columns7, data7, {
            startY: 370,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 530);
          doc.setFontStyle("bold");
          doc.text($scope.seniorcitizen.length + "", 495, 530);

          // 8th table initialize (FAMILIES)
          doc.autoTable(columns8, data8, {
            startY: 550,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 710);
          doc.setFontStyle("bold");
          doc.text($scope.allfamiies.length + "", 495, 710);

          doc.addPage();

          // 9th table initialize (GOVERNMENT EMPLOYED)
          doc.autoTable(columns9, data9, {
            startY: 70,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 250);
          doc.setFontStyle("bold");
          doc.text($scope.eGovernment.length + "", 495, 250);

          // 10th table initialize (PRIVATE EMPLOYED)
          doc.autoTable(columns10, data10, {
            startY: 270,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 450);
          doc.setFontStyle("bold");
          doc.text($scope.ePrivate.length + "", 495, 450);

          // 11th table initialize (SELF EMPLOYED)
          doc.autoTable(columns11, data11, {
            startY: 470,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 650);
          doc.setFontStyle("bold");
          doc.text($scope.eSelfEmployed.length + "", 495, 650);

          doc.addPage();

          // 12th table initialize (UNEMPLOYED)
          doc.autoTable(columns12, data12, {
            startY: 70,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 250);
          doc.setFontStyle("bold");
          doc.text($scope.eUnemployed.length + "", 495, 250);

          // 13th table initialize (OUT OF SCHOOL YOUTH)
          doc.autoTable(columns13, data13, {
            startY: 270,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 430);
          doc.setFontStyle("bold");
          doc.text($scope.eOutofSchoolYouth.length + "", 495, 430);

          // 13th table initialize (RETIRED)
          doc.autoTable(columns14, data14, {
            startY: 450,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 610);
          doc.setFontStyle("bold");
          doc.text($scope.eRetired.length + "", 495, 610);

          doc.addPage();

          // 14th table initialize (RETIRED)
          doc.autoTable(columns15, data15, {
            startY: 25,
            renderHeaderCell: header,
          });

          doc.text("TOTAL : ", 45, 190);
          doc.setFontStyle("bold");
          doc.text($scope.getallestablishment.length + "", 495, 195);

          doc.save(barangayName + '.pdf'); //generate PDF

        }


});