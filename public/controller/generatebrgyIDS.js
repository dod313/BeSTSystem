'use strict';
var app = angular.module('brgyapp');

app.controller('genrateIDsCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter, DbCollection, auth, ReportService){

    // check if the UserAcount is exist, if not clear the sessionStorage
       $http.get(DbCollection + 'account/')
        .then(function(result){
          $rootScope.UserAcount = result.data;
          if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
            sessionStorage.clear();
            $location.path('/login');
            window.location.reload();
          }
        });

        $scope.activateloading = false;

        var get_ID_Format5  = 'data:image/jpg;base64,'+ItemsServices.id_format7();
        var get_ph_flag = 'data:image/png;base64,'+ItemsServices.philippinesflag();

        function getids(){

         ReportService.getforresidentIds()
         .then(function(result){
            var residentIDs = result.data;
            var mlogo = $rootScope.UserAcount.mlogo
                        var ID_res = [];
                         for (var i = 0; i < residentIDs.length; i++) {
                             var results = residentIDs[i];

                                  ID_res.push({

                                      _id :  results._id,
                                      birthString: results.birthString,
                                      captain: results.captain,
                                      imageuri: results.imageuri,
                                      prof_gender: results.prof_gender,
                                      fullname: results.fullname,
                                      firstname: results.prof_firstname,
                                      middlename: results.prof_middle_name,
                                      lastname: results.prof_lastname,
                                      barangay_name: results.barangay,
                                      municipality: results.municipality,
                                      province: results.province,
                                      shortids: results.shortIds,
                                      mlogo: mlogo,
                                      idqrcode : qr.toDataURL(results.shortIds)

                                    });

                                    $scope.printID = function(action, truestring){

                                        if(truestring === null){
                                                 $scope.activateloading = true;
                                        }


                                          var doc = new jsPDF();

                                          for (var i = 0; i < residentIDs.length; i++) {

                                            // doc.setFontType("bold");
                                            doc.setFont("Arial");
                                            doc.setFontSize(6.5);

                                            switch(i % 8) {

                                              case 0:

                                                // ID wrapper .. 1st 2 numbers is X and Y (position).. last 2 numbers is for the img size..
                                                doc.addImage(get_ID_Format5, 'JPG', 17, 20, 87, 53);


                                                doc.setTextColor(0);

                                                doc.text(40, 26, "Republic of the Philippines");
                                                doc.text(40, 29, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 32, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'JPEG', 82, 39, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'PNG', 60, 38, 17, 17);

                                                doc.addImage(get_ph_flag, 'PNG', 25, 25, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG', 85, 24, 9, 9);
                                                // doc.text(79, 63, 'ID: '+ID_res[i].shortids);
                                                doc.text(45, 63, ID_res[i].captain);
                                                doc.text(42, 63, ' : ');
                                                doc.text(25, 63, 'Brgy. Captain');

                                                doc.text(23, 41, ' Firstname ');
                                                doc.text(23, 44, ' Middlename ');
                                                doc.text(23, 47, ' Lastname ');
                                                doc.text(23, 50, ' Gender ');
                                                doc.text(23, 53, ' Birthdate ');

                                                doc.text(36, 41, ' : ');
                                                doc.text(36, 44, ' : ');
                                                doc.text(36, 47, ' : ');
                                                doc.text(36, 50, ' : ');
                                                doc.text(36, 53, ' : ');

                                                doc.text(39, 41, ID_res[i].firstname);
                                                doc.text(39, 44, ID_res[i].middlename);
                                                doc.text(39, 47, ID_res[i].lastname);
                                                doc.text(39, 50, ID_res[i].prof_gender);
                                                doc.text(39, 53, ID_res[i].birthString);


                                                break;

                                              case 1:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 20, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 26, "Republic of the Philippines");
                                                doc.text(133, 29, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 32, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 175, 39, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 38, 17, 17);

                                                doc.addImage(get_ph_flag, 'PNG', 118, 25, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG', 178, 24, 9, 9);
                                                // doc.text(172, 63, 'ID: '+ID_res[i].shortids);
                                                doc.text(138, 63, ID_res[i].captain);
                                                doc.text(135, 63, ' : ');
                                                doc.text(118, 63, ' Brgy. Captain ');


                                                doc.text(116, 41, ' Firstname ');
                                                doc.text(116, 44, ' Middlename ');
                                                doc.text(116, 47, ' Lastname ');
                                                doc.text(116, 50, ' Gender ');
                                                doc.text(116, 53, ' Birthdate ');

                                                doc.text(129, 41, ' : ');
                                                doc.text(129, 44, ' : ');
                                                doc.text(129, 47, ' : ');
                                                doc.text(129, 50, ' : ');
                                                doc.text(129, 53, ' : ');

                                                doc.text(132, 41, ID_res[i].firstname);
                                                doc.text(132, 44, ID_res[i].middlename);
                                                doc.text(132, 47, ID_res[i].lastname);
                                                doc.text(132, 50, ID_res[i].prof_gender);
                                                doc.text(132, 53, ID_res[i].birthString);

                                                break;

                                              case 2:

                                                doc.addImage(get_ID_Format5, 'JPG', 17, 75, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(40, 81, "Republic of the Philippines");
                                                doc.text(40, 84, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 87, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 82, 94, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 60, 93, 17, 17);

                                                doc.addImage(get_ph_flag, 'PNG', 25, 80, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG',  85, 79, 9, 9);
                                                // doc.text(79, 118, 'ID: '+ID_res[i].shortids);
                                                doc.text(45, 118, ID_res[i].captain);
                                                doc.text(42, 118, ' : ');
                                                doc.text(25, 118, ' Brgy. Captain ');

                                                doc.text(23, 96, ' Firstname ');
                                                doc.text(23, 99, ' Middlename ');
                                                doc.text(23, 102, ' Lastname ');
                                                doc.text(23, 105, ' Gender ');
                                                doc.text(23, 108, ' Birthdate ');

                                                doc.text(36, 96, ' : ');
                                                doc.text(36, 99, ' : ');
                                                doc.text(36, 102, ' : ');
                                                doc.text(36, 105, ' : ');
                                                doc.text(36, 108, ' : ');

                                                doc.text(39, 96, ID_res[i].firstname);
                                                doc.text(39, 99, ID_res[i].middlename);
                                                doc.text(39, 102, ID_res[i].lastname);
                                                doc.text(39, 105, ID_res[i].prof_gender);
                                                doc.text(39, 108, ID_res[i].birthString);

                                                break;

                                              case 3:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 75, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 81, "Republic of the Philippines");
                                                doc.text(133, 84, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 87, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 175, 94, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 93, 17, 17);

                                                doc.addImage(get_ph_flag, 'PNG', 118, 80, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG',  178, 79, 9, 9);
                                                // doc.text(172, 118, 'ID: '+ID_res[i].shortids);
                                                doc.text(138, 118, ID_res[i].captain);
                                                doc.text(135, 118, ' : ');
                                                doc.text(118, 118, ' Brgy. Captain ');


                                                doc.text(116, 96, ' Firstname ');
                                                doc.text(116, 99, ' Middlename ');
                                                doc.text(116, 102, ' Lastname ');
                                                doc.text(116, 105, ' Gender ');
                                                doc.text(116, 108, ' Birthdate ');

                                                doc.text(129, 96, ' : ');
                                                doc.text(129, 99, ' : ');
                                                doc.text(129, 102, ' : ');
                                                doc.text(129, 105, ' : ');
                                                doc.text(129, 108, ' : ');

                                                doc.text(132, 96, ID_res[i].firstname);
                                                doc.text(132, 99, ID_res[i].middlename);
                                                doc.text(132, 102, ID_res[i].lastname);
                                                doc.text(132, 105, ID_res[i].prof_gender);
                                                doc.text(132, 108, ID_res[i].birthString);

                                                break;

                                              case 4:

                                                doc.addImage(get_ID_Format5, 'JPG', 17, 130, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(40, 136, "Republic of the Philippines");
                                                doc.text(40, 139, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 142, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 82, 149, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 60, 148, 17, 17);

                                                doc.addImage(get_ph_flag, 'PNG', 25, 135, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG',  85, 134, 9, 9);
                                                // doc.text(79, 173, 'ID: '+ID_res[i].shortids);
                                                doc.text(45, 173, ID_res[i].captain);
                                                doc.text(42, 173, ' : ');
                                                doc.text(25, 173, ' Brgy. Captain ');

                                                doc.text(23, 151, ' Firstname ');
                                                doc.text(23, 154, ' Middlename ');
                                                doc.text(23, 157, ' Lastname ');
                                                doc.text(23, 160, ' Gender ');
                                                doc.text(23, 163, ' Birthdate ');

                                                doc.text(36, 151, ' : ');
                                                doc.text(36, 154, ' : ');
                                                doc.text(36, 157, ' : ');
                                                doc.text(36, 160, ' : ');
                                                doc.text(36, 163, ' : ');

                                                doc.text(39, 151, ID_res[i].firstname);
                                                doc.text(39, 154, ID_res[i].middlename);
                                                doc.text(39, 157, ID_res[i].lastname);
                                                doc.text(39, 160, ID_res[i].prof_gender);
                                                doc.text(39, 163, ID_res[i].birthString);

                                                break;

                                              case 5:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 130, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 136, "Republic of the Philippines");
                                                doc.text(133, 139, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 142, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 175, 149, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 148, 17, 17);

                                                doc.addImage(get_ph_flag, 'PNG',  118, 135, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG', 178, 134, 9, 9);
                                                // doc.text(172, 173, 'ID: '+ID_res[i].shortids);
                                                doc.text(138, 173, ID_res[i].captain);
                                                doc.text(135, 173, ' : ');
                                                doc.text(118, 173, ' Brgy. Captain ');

                                                doc.text(116, 151, ' Firstname ');
                                                doc.text(116, 154, ' Middlename ');
                                                doc.text(116, 157, ' Lastname ');
                                                doc.text(116, 160, ' Gender ');
                                                doc.text(116, 163, ' Birthdate ');

                                                doc.text(129, 151, ' : ');
                                                doc.text(129, 154, ' : ');
                                                doc.text(129, 157, ' : ');
                                                doc.text(129, 160, ' : ');
                                                doc.text(129, 163, ' : ');

                                                doc.text(132, 151, ID_res[i].firstname);
                                                doc.text(132, 154, ID_res[i].middlename);
                                                doc.text(132, 157, ID_res[i].lastname);
                                                doc.text(132, 160, ID_res[i].prof_gender);
                                                doc.text(132, 163, ID_res[i].birthString);

                                                break;

                                              case 6:

                                                doc.addImage(get_ID_Format5, 'JPG', 17, 185, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(40, 191, "Republic of the Philippines");
                                                doc.text(40, 194, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(40, 197, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);
                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 82, 204, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 60, 203, 17, 17);

                                               doc.addImage(get_ph_flag, 'PNG', 25, 190, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG',  85, 189, 9, 9);
                                                // doc.text(79, 228, 'ID: '+ID_res[i].shortids);
                                                doc.text(45, 228, ID_res[i].captain);
                                                doc.text(42, 228, ' : ');
                                                doc.text(25, 228, ' Brgy. Captain ');

                                                doc.text(23, 206, ' Firstname ');
                                                doc.text(23, 209, ' Middlename ');
                                                doc.text(23, 212, ' Lastname ');
                                                doc.text(23, 215, ' Gender ');
                                                doc.text(23, 218, ' Birthdate ');

                                                doc.text(36, 206, ' : ');
                                                doc.text(36, 209, ' : ');
                                                doc.text(36, 212, ' : ');
                                                doc.text(36, 215, ' : ');
                                                doc.text(36, 218, ' : ');

                                                doc.text(39, 206, ID_res[i].firstname);
                                                doc.text(39, 209, ID_res[i].middlename);
                                                doc.text(39, 212, ID_res[i].lastname);
                                                doc.text(39, 215, ID_res[i].prof_gender);
                                                doc.text(39, 218, ID_res[i].birthString);

                                                break;

                                              case 7:

                                                doc.addImage(get_ID_Format5, 'JPG', 110, 185, 87, 53);

                                                doc.setTextColor(0);
                                                doc.text(133, 191, "Republic of the Philippines");
                                                doc.text(133, 194, 'Municipality/City of ' + ID_res[i].municipality);
                                                doc.text(133, 197, 'Sangguniang Barangay ng ' + ID_res[i].barangay_name);

                                                doc.addImage(ID_res[i].idqrcode, 'PNG', 175, 204, 17, 17);
                                                doc.addImage(ID_res[i].imageuri, 'JPEG', 153, 203, 17, 17);

                                                doc.addImage(get_ph_flag, 'PNG', 118, 190, 13, 7);

                                                doc.addImage(ID_res[i].mlogo, 'JPEG',  178, 189, 9, 9);
                                                // doc.text(172, 228, 'ID: '+ID_res[i].shortids);
                                                doc.text(138, 228, ID_res[i].captain);
                                                doc.text(135, 228, ' : ');
                                                doc.text(118, 228, ' Brgy. Captain ');

                                                doc.text(116, 206, ' Firstname ');
                                                doc.text(116, 209, ' Middlename ');
                                                doc.text(116, 212, ' Lastname ');
                                                doc.text(116, 215, ' Gender ');
                                                doc.text(116, 218, ' Birthdate ');

                                                doc.text(129, 206, ' : ');
                                                doc.text(129, 209, ' : ');
                                                doc.text(129, 212, ' : ');
                                                doc.text(129, 215, ' : ');
                                                doc.text(129, 218, ' : ');

                                                doc.text(132, 206, ID_res[i].firstname);
                                                doc.text(132, 209, ID_res[i].middlename);
                                                doc.text(132, 212, ID_res[i].lastname);
                                                doc.text(132, 215, ID_res[i].prof_gender);
                                                doc.text(132, 218, ID_res[i].birthString);

                                                doc.addPage(); // add page every time pdf page has 8 ID's already

                                                break;
                                            }

                                          }



                                          // the actual saving of pdf file with file name ID
                                          doc.save('ID.pdf')

                                          window.location.reload();
                                          // $scope.activateloading = false;

                                    };

                        }


         });

        }

getids();

});
