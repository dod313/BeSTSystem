'use strict';

var app = angular.module('brgyapp');

app.controller('houseHoldCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter, SearchItemsFactory,
  DbCollection, $location, $resource, ResidentServices, HouseholdCollections,
  SitioPurokService, householdservice, ReportService){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

    $scope.page = 0;
    var pagenow = 0;
    var startPage = 0;

// initialize the accordion element, set to true means everytime we click the element the current dom will close

    $scope.oneAtATime = true;

    function getallhousehold(){
        householdservice.GetAllHousehold()
        .then(function(resultdata){

            if(resultdata.data.length < 10){
              $scope.pagehide = true;
            }else{
              $scope.pagehide = false;
            }
            $scope.allhousehold = resultdata.data;

        })
    }

    getallhousehold();

    function getthelist(pagenow){

        var init = pagenow;
        return householdservice.gethouseholdlimit(init)
        .then(function(resultlimit){
           $scope.getlimithousehold = resultlimit.data;
        });

    }

    getthelist(pagenow);

    if($scope.page == 0){
      $scope.prevdisable = true;

    }

    $scope.previouspage = function(pagedown, pagetotal){

        $scope.datalengthnow = pagedown * 10;

          $scope.page = pagedown;

          if(pagedown == 0){

            $scope.prevdisable = true;

          }

          if(pagenow < pagetotal / 10){

            $scope.nextdisable = false;

          }

          return getthelist(pagedown);

      }

      $scope.nextpage = function(pagenow, pagetotal){

        $scope.datalengthnow = pagenow * 10;

        $scope.page = pagenow;

        var lastpage = pagetotal / 10;

        if(lastpage % 1 != 0) {
          lastpage = Math.round(lastpage) + 1;
        }

          if(pagenow + 1 == lastpage){

            $scope.nextdisable = true;

          }

          if(pagenow > 0){

            $scope.prevdisable = false;

          }

          return getthelist(pagenow);

      }

// view the house hold member

    $scope.viewmember = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/adminRegionPortal/viewresidentprofile.html',
        controller: $scope.viewresidentprofileCtrl,
        size: size,
        resolve: {
              getresident: function($http){
                  if(id){
                    return $http.get(DbCollection + '/resident/'+ id);
                  }else{
                    return null;

                  }
                }
              }
      });


      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
        });

};

$scope.viewresidentprofileCtrl = function($scope, getresident, $http, $modalInstance){



          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

        $scope.addNewChoice = function() {
          var newItemNo = $scope.brgyresident.demography.length+1;
           $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

          console.log($scope.brgyresident.demography)

        };

        SitioPurokService.GetAllSitioPurok()
        .then(function(getlistresult){
          $scope.GetAllSitioPuroklist = getlistresult.data;
        })

      $scope.languagesfamily    = ItemsServices.familylanguage();
      $scope.languages              = ItemsServices.languages();
      $scope.languagesdialect   = ItemsServices.languagesdialect();
      $scope.immunizations       = ItemsServices.immunizations();
      $scope.nutritions               = ItemsServices.nutritions();
      $scope.healths                   = ItemsServices.healths();
      $scope.others                     = ItemsServices.others();
      $scope.waters                    = ItemsServices.waters();
      $scope.supplies                  = ItemsServices.supplies();
      $scope.facilities                  = ItemsServices.facilities();
      $scope.otherones               = ItemsServices.otherones();
      $scope.bloodtype               = ItemsServices.bloodtype();

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.brgyresident = getresident.data;

}

// return the name of the house hold member
    $scope.gethouseheadname= function(getString){

      if(getString === undefined){
        return false;
      }

            var returnname = getString.split(',');
            return returnname[1];
    }

// return the name of the house hold member
    $scope.getresidentname= function(getString){

      if(getString === undefined){
        return false;
      }
            var returnname = getString.split(',');
            return returnname[1];
    }

// return the id of the house hold member
    $scope.getresidentId= function(getString){

      if(getString === undefined){
        return false;
      }

            var returnId = getString.split(',');
            return returnId[0];
    }

    $scope.addnewhousehold = function(size){
              var modalInstance = $modal.open({
                templateUrl: '../views/household/addnewmember.html',
                controller: $scope.householdCtrlinside,
                size: size
              });

                modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {

                });
    }

$scope.householdCtrlinside = function($modalInstance, $scope){
     $scope.searchhide = true;
    $scope.$watch('household.searchStr', function (tmpStr){

      var currentpage = 1;
      var getthispageandtext = currentpage+","+tmpStr;

      if (!tmpStr || tmpStr.length == 0)
        return 0;

        // if searchStr is still the same..
        // go ahead and retrieve the data

        if (tmpStr === $scope.household.searchStr)
        {
              SearchItemsFactory.SearchthisItext(getthispageandtext)
              .then(function(resultsearch){

              if(resultsearch.length === 0){

              return $scope.listdata = false;

              }else{

                $scope.listdata = true;
                $scope.searchresponse ={
                  message :  "total of data found: "+resultsearch.data.length,
                  status: false
                 };

                $scope.resultsearchfound = {
                  resdata : resultsearch.data,
                  status : true
                };

              }


              });
        }

            $scope.selectthisitem = function(thisobject){


              var getname = thisobject.fullname;
              $scope.household.searchStr = getname;
              return $scope.searchhide = false;

            }

            $scope.showsearch = function(){
              return $scope.searchhide = true;
            }
    });



          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

        $scope.children = [];

        $scope.household = {};

        $scope.household.groupmember = [{id: 'member1'}];

      $scope.addNewChoice = function() {
        var newItemNo = $scope.household.groupmember.length+1;
         $scope.household.groupmember .push({'id':'member'+newItemNo});
      };

      $scope.removeChoice = function() {
        var lastItem = $scope.household.groupmember.length-1;
        $scope.household.groupmember.splice(lastItem);
      };

    // add member not resident in this banrangay

      $scope.household.groupnotresidentmember = [{id: 'membernotresident1'}];

      $scope.addNewfield = function() {
        var newItemNo = $scope.household.groupnotresidentmember.length+1;
         $scope.household.groupnotresidentmember.push({'id':'membernotresident'+newItemNo});
      };

      $scope.removefield= function() {
        var lastItem = $scope.household.groupnotresidentmember.length-1;
        $scope.household.groupnotresidentmember.splice(lastItem);
      };

        $scope.addhouseholdgroup = function(groupnotresidentmember, groupmember){
            return householdservice.CreateHousehold($scope.household)
            .then(function(resultdata){
                   // getallhousehold();
                   getthelist(pagenow);
                 $modalInstance.dismiss('cancel');
            })

        }

     $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.viewgooglemap = function(lat, long) {

        var latitude = Number(lat);
        var longitude = Number(long);

        var mapPosition = {lat: latitude, lng: longitude};

        function initMap() {
          // Create a map object and specify the DOM element for display.
          var map = new google.maps.Map(document.getElementById('google-map-div'), {
            center: mapPosition,
            scrollwheel: true,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.HYBRID
          });

          var marker = new google.maps.Marker({
            position: mapPosition,
            map: map,
            title: 'Hello World!'
          });
        }

        initMap();

        $scope.mapbtnclicked = true;
        // alert("SSS");
      };
}

// edit the house hold member
    $scope.edithousehold = function(size, id){
          var modalInstance = $modal.open({
            templateUrl: '../views/household/addnewmember.html',
            controller: $scope.householdCtrledit,
            size: size,
            resolve: {
                  gethousehold: function($http){
                      if(id){
                        return householdservice.GetHouseID(id)
                      }else{
                        return null;

                      }
                    }
                  }
          });

            modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {

            });
    }

    $scope.householdCtrledit = function($scope, $modalInstance, gethousehold){

          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

        $scope.children = [];
        $scope.household = {};

      $scope.addNewChoice = function() {
        var newItemNo = $scope.household.groupmember.length+1;
         $scope.household.groupmember .push({'id':'member'+newItemNo});
      };

      $scope.removeChoice = function() {
        var lastItem = $scope.household.groupmember.length-1;
        $scope.household.groupmember.splice(lastItem);
      };

      $scope.addNewfield = function() {
        var newItemNo = $scope.household.groupnotresidentmember.length+1;
         $scope.household.groupnotresidentmember.push({'id':'membernotresident'+newItemNo});
      };

      $scope.removefield= function() {
        var lastItem = $scope.household.groupnotresidentmember.length-1;
        $scope.household.groupnotresidentmember.splice(lastItem);
      };

        $scope.updatehousehold = function(id){
               householdservice.UpdateHousehold(id, $scope.household)
               .then(function(resultdata){
                    getallhousehold();
                    $modalInstance.dismiss('cancel');
               })
        }

        $scope.household= gethousehold.data;

            $scope.deletehousehold = function(id){

            var con = confirm("Are you sure you want to delete this business permit?");

             if(con){

                householdservice.DeleteHousehold(id);
                // getallhousehold();
                getthelist(pagenow);
                $modalInstance.dismiss('cancel');
              }else {
                 $modalInstance.dismiss('cancel');
                return false;

              }

            }

         $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

        $scope.viewgooglemap = function(lat, long) {

          var latitude = Number(lat);
          var longitude = Number(long);

          var mapPosition = {lat: latitude, lng: longitude};

          function initMap() {
            // Create a map object and specify the DOM element for display.
            var map = new google.maps.Map(document.getElementById('google-map-div'), {
              center: mapPosition,
              scrollwheel: true,
              zoom: 15,
              mapTypeId: google.maps.MapTypeId.HYBRID
            });

            var marker = new google.maps.Marker({
              position: mapPosition,
              map: map,
              title: 'Hello World!'
            });
          }

          initMap();

          $scope.mapbtnclicked = true;
          // alert("SSS");
        };

    }



});