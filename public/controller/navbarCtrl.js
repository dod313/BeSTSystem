'use strict';

var app = angular.module('brgyapp');

app.controller('navbarCtrl', function($window, $rootScope, ResidentServices,
  ReportService, BrgyOfficialcollections, ItemsServices,
  BrgyOfficial, $modal, DbCollection, $http, $scope,
  Restangular){

var d = new Date();
$rootScope.SYear = d.getFullYear();

      var date_today = new Date();
      var dbmonth = ItemsServices.getmonth(date_today);
      var bvalue    = ItemsServices.getdatevalue(date_today);
      var byear = ItemsServices.get_issuedYear(date_today);

        var passtobirth = {
          dvalue : bvalue,
          dmonth : dbmonth,
          dyear : byear
        };

        ReportService.GetresidentBirthday(passtobirth)
        .then(function(dataresult){

            if(dataresult){
              $scope.residentBirthdays = dataresult.data;
            }else{
              $scope.nobirthday = 'No birthdays found.';
            }

        });

        $scope.get_age = function(d){
             return ItemsServices.calculate_age(d);
        }

       $scope.viewbirthday = function(size, id){

            var modalInstance = $modal.open({
              templateUrl: '../views/residentBirthdays.html',
              controller: $scope.residentbirthdayCtrl,
              size: size,
              resolve: {
                     getbirthwishes: function(){
                          return $http.get(DbCollection+'/resident/'+ id)
                    }
              }

            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {

              });
       }

      $scope.residentbirthdayCtrl = function($scope, getbirthwishes, $modalInstance){

         $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

        // $scope.get_age = function(d){
        //      return ItemsServices.calculate_age(d);
        // }

        $scope.residentbirth = getbirthwishes.data;
      }

       $scope.login_modal = function (size) {

            var modalInstance = $modal.open({
              templateUrl: '../views/login.html',
              controller: $scope.loginCtrl,
              size: size,

            });


            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {

              });

      };
 $scope.loginCtrl = function($scope, $rootScope, $modalInstance, $window,  $timeout, $location, authenticationSvc) {

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

         $scope.userInfo = null;

        $scope.login = function () {
             authenticationSvc.login($scope.account)
            .then(function (result) {
                $scope.dataLoading = true;
                $scope.userInfo = result.data;
                $rootScope.UserAcount = result.data;
                console.log($rootScope.UserAcount);
                $modalInstance.dismiss('cancel');
                $location.path("/home");

            }, function (error) {
                $scope.error = error.data.message;
                $scope.dataLoading = false;
                $location.path("/login");
            });
        };


       $scope.signup = function (size) {

        var modalInstance = $modal.open({
          templateUrl: '../views/signup.html',
          controller: $scope.signupCtrl,
          size: size,
        });


        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
          }, function () {

          });

      };

    $scope.signupCtrl = function($scope, $rootScope, $modalInstance, $http, $window, $timeout, $location, Signupviewers) {

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };


       $scope.signup_viewer = function () {
        $http.post(Signupviewers + '/viewers', $scope.account)
        .then(function (result) {
                $scope.account = result.data[0];
                $modalInstance.dismiss('cancel');
                $rootScope.message_access = "You now have access with your new account '"+ $scope.account.username + "' you can now login as viewer.";

        }, function (error) {
                $scope.error = error.data.message;
            });
        };
    }

 }

$scope.$watch('qrvalue', function(value){

      if(value){
      var l = value.length;
      var string_l = l.toString();

          if(string_l === '8'){

              $http.get(DbCollection + '/get_qr_resident/'+ value)
              .then(function (result) {

                if(result.data.length == 0){
                   $scope.error = 'No Data Found!';
                }else{
                  $scope.viewqr('lg', value);
                }

              }, function (error) {
                    $scope.error = error.data.message;
              });

          }else if( l < 8){

            $scope.error = 'Please enter the 8 characters!';

          }else if(l > 8){

            $scope.error = 'The data length is exceed!';

          }


      }

       $scope.viewqr = function (size, value) {

            var modalInstance = $modal.open({
              templateUrl: '../views/viewresident.html',
              controller: $scope.viewresident,
              size: size,
               resolve: {
                      getvalue: function($http){
                          if(value){
                            return $http.get(DbCollection + '/get_qr_resident/'+ value);
                          }else{
                            return null;

                          }
                        }
                      }
            });

            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 // window.location.reload();
              });

      };

    $scope.viewresident = function($scope, $modalInstance, getvalue, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

     $scope.brgyresident = getvalue.data[0];
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            $scope.qrvalue = '';
            // window.location.reload();
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
            $scope.qrvalue = '';
            // window.location.reload();
          };

    }

})


     $scope.account_modal = function (size, id, access) {

          var modalInstance = $modal.open({
            templateUrl: '../views/my_account.html',
            controller: $scope.editmodal,
            size: size,
             resolve: {
                    getaccount: function($http){


                          if(access === 'regionadmin'){

                              return $http.get(DbCollection + '/registered/'+ id);

                          }else if(access === 'provincecontrol'){

                              return $http.get(DbCollection + '/provincelist/'+ id);

                          }else if(access === 'municipalitycontrol'){

                              return $http.get(DbCollection + '/registered/'+ id);

                          }else if(access === 'barangayaccesscontrol'){

                              return $http.get(DbCollection + '/brgy_userlist/'+ id);

                          }else{

                            return null;

                          };

                      },
                      auth: function ($q, authenticationSvc) {
                          var userInfo = authenticationSvc.getUserInfo();
                          if (userInfo) {
                              return $q.when(userInfo);
                          } else {
                              return $q.reject({ authenticated: false });
                          }
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
              // window.location.reload();
            });

    };

     $scope.editmodal = function($scope, $modalInstance, getaccount, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {



      if(getaccount.data === null){
      }else{
        $scope.account = getaccount.data;
      }

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

     	   $scope.caption_pass = 'Change Password';

          $scope.enable_new = function (){
            $scope.newpass = 'true';
            $scope.caption_pass = 'Click to hide';
          }
          $scope.enable_hide = function (){
            $scope.newpass = 'false';
            $scope.caption_pass = 'Change Password';
          }

          $scope.update_my_account = function (id, access) {

             if(access === 'user'){

                $http.put(DbCollection + '/edit_own_account/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });

             }else if(access === 'provincecontrol'){

                  $http.put(DbCollection + '/provincelist/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });

             }else if(access === 'municipalitycontrol'){

                  $http.put(DbCollection + '/municipalityaccount/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });

             }else{

                $http.put(DbCollection + '/registered/'+id, $scope.account)
                  .then(function (result) {
                        $scope.account = result.data;
                        $modalInstance.dismiss('cancel');
                  }, function (error) {
                          $scope.error = error.data.message;
                  });
             }

  	   };

     };


});