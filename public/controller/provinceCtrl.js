app.controller('provinceCtrl', function($scope, $resource, $window, $modal, $log, $rootScope,
  Province, $http, Restangular, $timeout, $route, $q, $location, $filter,
  ngTableParams, DbCollection) {


// check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

  $scope.currentPage = 1;
  $scope.pageSize = 6;

  $scope.province = Province.data;
  $scope.provinceId = $scope.province._id;

    function getMunicipality_list(){
       $http.get(DbCollection+'/municipality/'+ $scope.provinceId)
       .then(function(result){
        $scope.municipalities_list = result.data;
       });
     }

getMunicipality_list();

var referrerId = $rootScope.UserAcount.createdbyId;

    $scope.municipality_modal = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/region/municipality.html',
        controller: $scope.municipality_modalCtrl,
        size: size,
        resolve: {
              Province: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getprovince/'+ id)
                  }else{
                    return null;

                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
          getMunicipality_list();
        });

    };

    $scope.municipality_modalCtrl = function($scope, $modalInstance, $modal, Province, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {


          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.province = Province.data;

          $scope.savemunicipality = function() {
              $scope.municipality.province = $scope.province.province_name;
              $scope.municipality.provinceId = $scope.province._id;
              $scope.municipality.regionId = $scope.province.regionId;
              $scope.municipality.region = $scope.province.region;
              $http.post(DbCollection+'/municipality', $scope.municipality)
              .then(function(result){
                $scope.result = result.data;
                 $modalInstance.dismiss('cancel');
                getMunicipality_list();
              },function(error){
                $scope.error = error.data.message;
              });
          }


          if(Province){
            return $scope.getprovince = Province.data;
          }else{
            return null;
          }

    }


    $scope.municipality_edit_modal = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/region/municipality.html',
        controller: $scope.municipality_edit_modalCtrl,
        size: size,
        resolve: {
              Municipality: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getmunicipality/'+ id)
                  }else{
                    return null;

                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
          getMunicipality_list();
        });

    };

    $scope.municipality_edit_modalCtrl = function($scope, $modalInstance, $modal, Municipality, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {


          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.municipality = Municipality.data;

          $scope.update_municipal = function(id) {
            console.log(id);
              $http.put(DbCollection+'/municipality/'+ id, $scope.municipality)
              .then(function(result){
                $scope.result = result.data;
                 $modalInstance.dismiss('cancel');
                 getMunicipality_list();
              },function(error){
                $scope.error = error.data.message;
              });
          }

          if(Municipality){
            return $scope.municipality = Municipality.data;
          }else{
            return null;
          }

    }

      $scope.show = function(id){
          $location.path('/viewbrgydata/' + id);
      };

});

app.controller('barangayCtrl', function($scope, $resource, Municipality, $modal, $log, $rootScope, $http, Restangular, $timeout, $route, $q, $location, $filter, ngTableParams, DbCollection) {

      $scope.municipalityresult = Municipality.data;
      $scope.municipalId = $scope.municipalityresult._id;
      $scope.municipalityname = $scope.municipalityresult.municipality_name;
      $scope.province = $scope.municipalityresult.province;
      $scope.region = $scope.municipalityresult.region;

  $scope.currentPage = 1;
  $scope.pageSize = 6;

      function getBarangay_list(){
           $http.get(DbCollection+'/brgycol/'+ $scope.municipalId)
           .then(function(result){
            $scope.barangay_list = result.data;
          });
      }

getBarangay_list();

    $scope.brgy_modal = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/region/brgy.html',
        controller: $scope.brgy_modalCtrl,
        size: size,
        resolve: {
              Municipality: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getmunicipality/'+ id)
                  }else{
                    return null;

                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
          getBarangay_list();
        });

    };

    $scope.brgy_modalCtrl = function($scope, $modalInstance, $modal, Municipality, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {


          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.municipality = Municipality.data;

          $scope.getMunicipal = Municipality.data;


          $scope.savebrgy = function() {

                $scope.brgy.municipality = $scope.getMunicipal.municipality_name;
                $scope.brgy.municipalId = $scope.getMunicipal._id;
                $scope.brgy.province = $scope.getMunicipal.province;
                $scope.brgy.region = $scope.getMunicipal.region;
                $scope.brgy.zip_code = $scope.getMunicipal.zip_code;

                $http.post(DbCollection+'/brgycol', $scope.brgy)
                .then(function(result){
                $scope.result = result.data;
                  $modalInstance.dismiss('cancel');
                  getBarangay_list();
                }, function(error){
                  $scope.error = error.data.message;
                });
            };

          if(Municipality){
            return $scope.municipality = Municipality.data;
          }else{
            return null;
          }

    };


    $scope.brgy_edit = function (size, id) {
      var modalInstance = $modal.open({
        templateUrl: '../views/region/brgy.html',
        controller: $scope.brgy_edit_modalCtrl,
        size: size,
        resolve: {
              Brgy: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getbrgycol/'+ id)
                  }else{
                    return null;

                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
          getBarangay_list();
        });

    };

    $scope.brgy_edit_modalCtrl = function($scope, $modalInstance, $modal, Brgy, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {


          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.brgy = Brgy.data;

          $scope.updatebrgy = function(id) {

                $http.put(DbCollection+'/brgycol/'+id, $scope.brgy)
                .then(function(result){
                $scope.result = result.data;
                  $modalInstance.dismiss('cancel');
                  getBarangay_list();
                }, function(error){
                  $scope.error = error.data.message;
                });
            };

          if(Brgy){
            return $scope.brgy = Brgy.data;
          }else{
            return null;
          }

    };

});
