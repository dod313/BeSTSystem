'use strict';

var app = angular.module('brgyapp');

app.controller('surveyReport', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, SurverReportCollections,
  SitioPurokService, ReportService, EstablishmentsFactory, SurveyReportServices){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

// console.log(SurverReportCollections+'/getdataresidentsitio/');

        $scope.currentPage = 1;
        $scope.pageSize = 6;

        $scope.surveyhead = ItemsServices.SurveyHead();
        $scope.surveyheademployed =  ItemsServices.SurveyResidentEmployedStatus();
        $scope.surveyheadmore =  ItemsServices.SurveyResidentStatus();
        $scope.SurveyResidentMoreStatus = ItemsServices.SurveyResidentMoreStatus();
        $scope.SurveyResidentPhysicalStatus = ItemsServices.SurveyResidentPhysicalStatus();

          function getallsitiopuroklist(){
              SitioPurokService.GetAllSitioPurok()
              .then(function(getlistresult){
                $scope.getinitialsitioid = getlistresult.data[0]._id;
                $scope.getinitialsitioname = getlistresult.data[0].sitioPurokname;
                return $scope.GetAllSitioPuroklist = getlistresult.data;

              })
          }

          getallsitiopuroklist();

              SitioPurokService.GetAllSitioPurok()
              .then(function(getlistresult){
                $scope.getinitialsitioid = getlistresult.data[0]._id;
                 var sitiopurokId = $scope.getinitialsitioid;

                    $scope.getsitiopurok_survey = function(bId, sitiopurokId, sitiopurokname){

                              var objectIDs = {
                                  usertID : bId,
                                  sitiopurokID : sitiopurokId,
                                  sitiopurokName : sitiopurokname
                              };

                            SurveyReportServices.GetResidentSurveyReport(sitiopurokId)
                            .then(function(resultdata){
                                $scope.getresidentsetiopurok = resultdata.data;
                                $scope.getresidentgendersetiopurok = resultdata.data;
                            })

                            SurveyReportServices.GetHouseHoldSurveyReport(sitiopurokId)
                            .then(function(resultdata){
                                  $scope.gethouseholdsetiopurok = resultdata.data;
                            })

                            SurveyReportServices.GetTranscientSurveyReport(sitiopurokId)
                            .then(function(resultdata){
                                  $scope.gettranscientsetiopurok = resultdata.data;
                            })

                            EstablishmentsFactory.GetAllEstablishments()
                            .then(function(resultdata){

                                var GetAllEstablishments =  resultdata.data;
                                var log = [];

                                  angular.forEach(GetAllEstablishments, function(value, key) {

                                    if(value.business_address.split(',')[0] === sitiopurokId){

                                                this.push({
                                                  _id: value._id,
                                                  business_type: value.business_type,
                                                  business_name: value.business_name,
                                                  business_address: value.business_address,
                                                  ownername : value.ownername

                                                });
                                          }

                                    }, log);

                                    $scope.getallestablishment  = log;

                            })

                            //get all seventynine months old child
                              ReportService.getseventyninemonths()
                              .then(function(resultchild){

                                var seventyninemonth = resultchild;
                                var log = [];

                                  angular.forEach(seventyninemonth, function(value, key) {

                                    if(value.prof_sitio.split(',')[0] === sitiopurokId){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date,
                                                  prof_sitio: value.prof_sitio
                                                });
                                          }
                                    }, log);

                                    $scope.seventyninemonth = log;

                              });

                            $scope.getsitiopuroktotal = function(getIndicator){



                                if(getIndicator === 'Households'){
                                  $scope.gethouseholdsetiopurokLabel = getIndicator;
                                  return $scope.gethouseholdsetiopurok;
                                }

                                if(getIndicator === 'Population'){
                                  $scope.getresidentsetiopurokLabel = getIndicator;
                                  return $scope.getresidentsetiopurok;
                                }

                                if(getIndicator === 'Trancients'){
                                  $scope.gettranscientsetiopurokLabel = getIndicator;
                                  return $scope.gettranscientsetiopurok;
                                }

                                if(getIndicator === 'Male'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.prof_gender === 'Male'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultgendersetiopurok = log;
                                    $scope.getresidentresultmalesetiopurok = log;
                                    $scope.getresidentresultmalesetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultgendersetiopurok;

                                }

                                if(getIndicator === 'Female'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.prof_gender === 'Female'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultgendersetiopurok = log;
                                    $scope.getresidentresultfemalesetiopurok = log;
                                    $scope.getresidentresultfemalesetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultgendersetiopurok;

                                }

                                if(getIndicator === 'Self Employed'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.employmentstatus === 'SelfEmployed'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultSelfEmployedsetiopurok = log;
                                    $scope.getresidentresultSelfEmployedsetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultSelfEmployedsetiopurok;

                                }

                               if(getIndicator === 'Unemployed'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.employmentstatus === 'Unemployed'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultUnemployedsetiopurok = log;
                                    $scope.getresidentresultUnemployedsetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultUnemployedsetiopurok;

                                }

                                if(getIndicator === 'Out of School Youth'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.employmentstatus === 'OutofSchoolYouth'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultOutofSchoolYouthsetiopurok = log;
                                    $scope.getresidentresultOutofSchoolYouthsetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultOutofSchoolYouthsetiopurok;

                                }

                                if(getIndicator === 'Retired'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.employmentstatus === 'Retired'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultRetiredsetiopurok = log;
                                    $scope.getresidentresultRetiredsetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultRetiredsetiopurok;

                                }

                                if(getIndicator === 'Government Employed'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.employmentype === 'Government'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultGovernmentsetiopurok = log;
                                    $scope.getresidentresultGovernmentsetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultGovernmentsetiopurok;

                                }

                               if(getIndicator === 'Private Employed'){

                                  var values = $scope.getresidentgendersetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.employmentype === 'Private'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getresidentresultPrivatesetiopurok = log;
                                    $scope.getresidentresultPrivatesetiopurokLabel = getIndicator;
                                    return $scope.getresidentresultPrivatesetiopurok;

                                }

                                if(getIndicator === 'PWD'){

                                  var values = $scope.getresidentsetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.PersonWithDisability === true){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getHouseholdresultPWDsetiopurok = log;
                                    $scope.getHouseholdresultPWDsetiopurokLabel = getIndicator;
                                    return $scope.getHouseholdresultPWDsetiopurok;

                                }

                                if(getIndicator === 'Pregnant'){

                                  var values = $scope.getresidentsetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.Pregnant === true){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.fullname,
                                                  bdate: value.b_date
                                                });
                                          }
                                    }, log);

                                    $scope.getHouseholdresultPregnantsetiopurok = log;
                                    $scope.getHouseholdresultPregnantsetiopurokLabel = getIndicator;
                                    return $scope.getHouseholdresultPregnantsetiopurok;

                                }

                                if(getIndicator === 'Without Electricity'){

                                  var values = $scope.gethouseholdsetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.Electricity === 'No'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.headmember
                                                });
                                          }
                                    }, log);

                                    $scope.getHouseholdresultElectricitysetiopurok = log;
                                    $scope.getHouseholdresultElectricitysetiopurokLabel = getIndicator;
                                    return $scope.getHouseholdresultElectricitysetiopurok;

                                }

                                if(getIndicator === 'Without C.R.'){

                                  var values = $scope.gethouseholdsetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.CR === 'No'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.headmember
                                                });
                                          }
                                    }, log);

                                    $scope.getHouseholdresultCRsetiopurok = log;
                                    $scope.getHouseholdresultCRsetiopurokLabel = getIndicator;
                                    return $scope.getHouseholdresultCRsetiopurok;

                                }

                                if(getIndicator === 'Without Water'){

                                  var values = $scope.gethouseholdsetiopurok;
                                  var log = [];

                                  angular.forEach(values, function(value, key) {

                                    if(value.Water === 'No'){
                                                this.push({
                                                  id: value._id,
                                                  fullname: value.headmember
                                                });
                                          }
                                    }, log);

                                    $scope.getHouseholdresultWatersetiopurok = log;
                                    $scope.getHouseholdresultWatersetiopurokLabel = getIndicator;
                                    return $scope.getHouseholdresultWatersetiopurok;

                                }

                            }

                           $scope.sitiopurokname = sitiopurokname;
                    }

                    $scope.generatePurokSurvey = function(id) {
                      console.log($scope.gethouseholdsetiopurokLabel);
                      console.log($scope.gethouseholdsetiopurok.length);

                      // console.log($scope.getinitialsitioname);
                      // console.log($scope.sitiopurokname);

                      if(!$scope.sitiopurokname) {
                        $scope.sitiopurokname = $scope.getinitialsitioname;
                      }

                      var sitiopurokname = $scope.sitiopurokname;

                      // 1st table
                      var totalHouseholdsLabel = $scope.gethouseholdsetiopurokLabel;
                      var totalHouseholds = $scope.gethouseholdsetiopurok.length;
                      var totalPopulationLabel = $scope.getresidentsetiopurokLabel;
                      var totalPopulation = $scope.getresidentsetiopurok.length;
                      var totalTrancientsLabel = $scope.gettranscientsetiopurokLabel;
                      var totalTrancients = $scope.gettranscientsetiopurok.length;
                      var totalMaleLabel = $scope.getresidentresultmalesetiopurokLabel;
                      var totalMale = $scope.getresidentresultmalesetiopurok.length;
                      var totalFemaleLabel = $scope.getresidentresultfemalesetiopurokLabel;
                      var totalFemale = $scope.getresidentresultfemalesetiopurok.length;

                      var columns = [
                          {title: sitiopurokname, key: "name", width: 600},
                          {title: "", key: "total", width: 100},
                      ];

                      var data = [
                          {"name": "Total No. of " + totalHouseholdsLabel, "total": totalHouseholds},
                          {"name": "Total No. of " + totalPopulationLabel, "total": totalPopulation},
                          {"name": totalTrancientsLabel, "total": totalTrancients},
                          {"name": totalMaleLabel, "total": totalMale},
                          {"name": totalFemaleLabel, "total": totalFemale},
                      ];

                      // 2nd table
                      var totalSelfEmployedLabel = $scope.getresidentresultSelfEmployedsetiopurokLabel;
                      var totalSelfEmployed = $scope.getresidentresultSelfEmployedsetiopurok.length;
                      var totalUnemployedLabel = $scope.getresidentresultUnemployedsetiopurokLabel;
                      var totalUnemployed = $scope.getresidentresultUnemployedsetiopurok.length;
                      var totalOutOfSchoolYouthLabel = $scope.getresidentresultOutofSchoolYouthsetiopurokLabel;
                      var totalOutOfSchoolYouth = $scope.getresidentresultOutofSchoolYouthsetiopurok.length;
                      var totalRetiredLabel = $scope.getresidentresultRetiredsetiopurokLabel;
                      var totalRetired = $scope.getresidentresultRetiredsetiopurok.length;

                      var columns2 = [
                          {title: "Status", key: "name", width: 600},
                          {title: "", key: "total", width: 100},
                      ];

                      var data2 = [
                          {"name": totalSelfEmployedLabel, "total": totalSelfEmployed},
                          {"name": totalUnemployedLabel, "total": totalUnemployed},
                          {"name": totalOutOfSchoolYouthLabel, "total": totalOutOfSchoolYouth},
                          {"name": totalRetiredLabel, "total": totalRetired},
                      ];

                      // 3rd table
                      var totalGovernmentEmployedLabel = $scope.getresidentresultGovernmentsetiopurokLabel;
                      var totalGovernment = $scope.getresidentresultGovernmentsetiopurok.length;
                      var totalPrivateEmployedLabel = $scope.getresidentresultPrivatesetiopurokLabel;
                      var totalPrivateEmployed = $scope.getresidentresultPrivatesetiopurok.length;

                      var columns3 = [
                          {title: "Employed", key: "name", width: 600},
                          {title: "", key: "total", width: 100},
                      ];

                      var data3 = [
                          {"name": totalGovernmentEmployedLabel, "total": totalGovernment},
                          {"name": totalPrivateEmployedLabel, "total": totalPrivateEmployed},
                      ];

                      // 4th table
                      var totalPWDLabel = $scope.getHouseholdresultPWDsetiopurokLabel;
                      var totalPWD = $scope.getHouseholdresultPWDsetiopurok.length;
                      var totalPregnantLabel = $scope.getHouseholdresultPregnantsetiopurokLabel;
                      var totalPregnant = $scope.getHouseholdresultPregnantsetiopurok.length;

                      var columns4 = [
                          {title: "Physical Status", key: "name", width: 600},
                          {title: "", key: "total", width: 100},
                      ];

                      var data4 = [
                          {"name": totalPWDLabel, "total": totalPWD},
                          {"name": totalPregnantLabel, "total": totalPregnant},
                      ];

                      // 5th table
                      var totalWithoutElectricityLabel = $scope.getHouseholdresultElectricitysetiopurokLabel;
                      var totalWithoutElectricity = $scope.getHouseholdresultElectricitysetiopurok.length;
                      var totalWithoutCRLabel = $scope.getHouseholdresultCRsetiopurokLabel;
                      var totalWithoutCR = $scope.getHouseholdresultCRsetiopurok.length;
                      var totalWithoutWaterLabel = $scope.getHouseholdresultWatersetiopurokLabel;
                      var totalWithoutWater = $scope.getHouseholdresultWatersetiopurok.length;

                      var columns5 = [
                          {title: "", key: "name", width: 600},
                          {title: "", key: "total", width: 100},
                      ];

                      var data5 = [
                          {"name": totalWithoutElectricityLabel, "total": totalWithoutElectricity},
                          {"name": totalWithoutCRLabel, "total": totalWithoutCR},
                          {"name": totalWithoutWaterLabel, "total": totalWithoutWater},
                      ];

                      var getstablishment = $scope.getallestablishment.length;

                      var columns6 = [
                          {title: "", key: "name", width: 600},
                          {title: "", key: "total", width: 100},
                      ];

                      var data6 = [
                          {"name": 'Total No. Of Establishments', "total": getstablishment}
                      ];


                      var seventyninemonth = $scope.seventyninemonth.length;

                      var columns7 = [
                          {title: "", key: "name", width: 600},
                          {title: "", key: "total", width: 100},
                      ];

                      var data7 = [
                          {"name": '0-79 MONTHS OLD CHILD', "total": seventyninemonth}
                      ];

                      var doc = new jsPDF('p', 'pt', 'letter');

                      // for custom style
                      var header = function (x, y, width, height, key, value, settings) {
                        doc.setFont("helvetica");
                        doc.setFillColor(217, 237, 247);
                        doc.setTextColor(49, 112, 159);
                        doc.setFontSize(14);
                        doc.rect(x, y, width, height, 'F');
                        y += settings.lineHeight / 2 + doc.internal.getLineHeight() / 2 - 2.5;
                        doc.text('' + value, x + settings.padding, y);
                      };

                      doc.text("Sitio/Purok: " + sitiopurokname, 40, 70);
                      doc.text("Interviewer: ", 40, 90);

                      // 1st table initialize
                      doc.autoTable(columns, data, {
                        startY: 115,
                        renderHeaderCell: header,
                      });

                      // 2nd table initialize
                      doc.autoTable(columns2, data2, {
                        startY: 250,
                        renderHeaderCell: header
                      });

                      // 3rd table initialize
                      doc.autoTable(columns3, data3, {
                        startY: 365,
                        renderHeaderCell: header
                      });

                      // 4th table initialize
                      doc.autoTable(columns4, data4, {
                        startY: 440,
                        renderHeaderCell: header
                      });

                      // 5th table initialize
                      doc.autoTable(columns5, data5, {
                        startY: 515,
                        renderHeaderCell: header
                      });

                      // 6th table initialize
                      doc.autoTable(columns7, data7, {
                        startY: 615,
                        renderHeaderCell: header
                      });

                     doc.addPage();

                      // 6th table initialize
                      doc.autoTable(columns6, data6, {
                        startY:25,
                        renderHeaderCell: header
                      });
                      doc.save(sitiopurokname + '.pdf');

                    }


                    $scope.getsitiopurok_survey(null, sitiopurokId, null);
              });

});