'use strict';

var app = angular.module('EstablishmentsFactories', []);

app.factory("EstablishmentsFactory",  function($http,  EstablishmentsCollections){

return{

    CreateEstablishments:function(data){
        return $http.post(EstablishmentsCollections+'/establishments', data);
    },

    GetAllEstablishments:function(){
        return $http.get(EstablishmentsCollections+'/establishments');
     },

    GetEstablishmentsID:function(id){
        return $http.get(EstablishmentsCollections+'/establishments/'+id);
    },

    GetMyEstablishmentsID:function(id){
        return $http.get(EstablishmentsCollections+'/myestablishments/'+id);
    },

    UpdateEstablishments:function(id,data){
        return $http.put(EstablishmentsCollections+'/establishments/'+id, data);
    },

    DeleteEstablishmentst:function(id){

        return $http.delete(EstablishmentsCollections+'/establishments/'+id);
    },

    getestablishmentlimit:function(currentpage){
        return $http.get(EstablishmentsCollections+'/getestablishmentbypage/'+currentpage);
    },

}

});