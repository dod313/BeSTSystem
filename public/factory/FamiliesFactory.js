'use strict';

var app = angular.module('FamiliesFactory', []);

app.factory("familiesServices",  function($http,  FamiliesCollections){

    return{

        CreateFamily:function(data){
            return $http.post(FamiliesCollections+'/families/', data);
        },

        GetAllFamily:function(){
            return $http.get(FamiliesCollections+'/families/');
         },

        GetFamilyID:function(id){
            return $http.get(FamiliesCollections+'/families/'+id);
        },

        UpdateFamily:function(id,data){
            return $http.put(FamiliesCollections+'/families/'+id, data);
        },

        DeleteFamily:function(id){

            return $http.delete(FamiliesCollections+'/families/'+id);
        },

        getfamilylimit:function(currentpage){
            return $http.get(FamiliesCollections+'/getfamilybypage/'+currentpage);
        },

    }

});