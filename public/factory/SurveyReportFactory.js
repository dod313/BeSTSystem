'use strict';

var app = angular.module('SurveyReportFactory', []);

app.factory("SurveyReportServices",  function($q, $http, SearchBaseurl, SurverReportCollections){

return{

    GetResidentSurveyReport:function(id){
        return $http.get(SurverReportCollections+'/getdataresidentsitio/'+id)
     },

    GetHouseHoldSurveyReport:function(id){
        return $http.get(SurverReportCollections+'/getdatahouseholdsitiopurok/'+id);
     },

    GetTranscientSurveyReport:function(id){
        return $http.get(SurverReportCollections+'/getdatatranscientsitiopurok/'+id);
     },

    GetGovernmentSurveyReport:function(id){
        return $http.get(SurverReportCollections+'/getdataresidentgovernmentemployed/'+id);
     },

    getmonthreport_res:function(data){
          var deferred = $q.defer();
          var report_res=[];
          return $http.post(SearchBaseurl+'getspecificmonth/', data);
    },

    GetresidentBirthday:function(data){
        return $http.post(SearchBaseurl+'getbirthtoday/', data);
     },

    getforresidentIds:function(){
        return $http.get(SearchBaseurl+'getIDs/');
     }
}

});