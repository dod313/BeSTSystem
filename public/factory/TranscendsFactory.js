'use strict';

var app = angular.module('TranscendsFactory', []);

app.factory("transFactory",  function($http,  TranscendsCollections){

return{

    CreateTranscends:function(data){
        return $http.post(TranscendsCollections+'/transcend/', data);
    },

    GetAllTranscends:function(){
        return $http.get(TranscendsCollections+'/transcend/');
     },

    GetTranscendsID:function(id){
        return $http.get(TranscendsCollections+'/transcend/'+id);
    },

    UpdateTranscends:function(id,data){
        return $http.put(TranscendsCollections+'/transcend/'+id, data);
    },

    DeleteTranscends:function(id){

        return $http.delete(TranscendsCollections+'/transcend/'+id);
    },

    gettransientlimit:function(currentpage){
        return $http.get(TranscendsCollections+'/gettransientbypage/'+currentpage);
    },

}

});