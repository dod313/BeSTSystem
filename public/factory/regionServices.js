'use strict';

var app = angular.module('RegionServices', []);

app.factory("regionService",  function($http,  RegionCollections){

return{

    CreateRegion:function(data){
        return $http.post(RegionCollections+'/viewregion', data);
    },
    GetAllRegion:function(){
        return $http.get(RegionCollections+'/viewregion');
     },
    GetResidentRegion:function(id){
        return $http.get(RegionCollections+'/view_resident_region/'+id);
    },
    GetProvincesRegion:function(id){
        return $http.get(RegionCollections+'/view_provinces_region/'+id);
    },
    GetResidentProvince:function(id){
        return $http.get(RegionCollections+'/view_residents_province/'+id);
    },
    GetResidentMunicipal:function(id){
        return $http.get(RegionCollections+'/view_resident_municipal/'+id);
    },
    GetResidentBarangay:function(id){
        return $http.get(RegionCollections+'/view_resident_barangay/'+id);
    },
    GetProvinceMunicipalities:function(id){
        return $http.get(RegionCollections+'/view_provinces_region/'+id);
    },
}

});