'use strict';

var app = angular.module('SitioPurokFactory', []);

app.factory("SitioPurokService",  function($http,  SitioPurokCollections){

        return{
            CreateSitioPurok:function(data){
                return $http.post(SitioPurokCollections+'/sitiopurokroutes/', data);
            },
            GetAllSitioPurok:function(){
                return $http.get(SitioPurokCollections+'/sitiopurokroutes/');
             },
            GetSitioPurokID:function(id){
                return $http.get(SitioPurokCollections+'/sitiopurokroutes/'+id);
            },
            UpdateSitioPurok:function(id,data){
                return $http.put(SitioPurokCollections+'/sitiopurokroutes/'+id, data);
            },
            DeleteSitioPurok:function(id){
                return $http.delete(SitioPurokCollections+'/sitiopurokroutes/'+id);
            },
            getgovernmentEmployedinsitio:function(id){
                return $http.get(SitioPurokCollections+'/getdataresidentgovernmentemployed/'+id);
            },
            getallemployed:function(id){
                return $http.get(SitioPurokCollections+'/getalltotal/');
            }
        }

});