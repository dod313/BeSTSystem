var app = angular.module('brgyapp');

app.factory('Tax', ['$resource', function($resource) {
	return $resource('/api/taxcollection/:id', {},
    {
        update: { method:'PUT'}
    });
}]);