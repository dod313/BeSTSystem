'use strict';
var app = angular.module('brgyapp',
	[
		'ngResource',
		'ngRoute',
		'restangular',
		'ui.bootstrap',
		'angularUtils.directives.dirPagination',
		'monospaced.qrcode',
		'ngMessages',
		'ngTable',
		'ui.timepicker',
            'ResidentService',
            'brgyOfficials',
            'BusinessFactories',
            'viewReportServices',
            'RegionServices',
            'houseFactory',
            'EstablishmentsFactories',
            'angular.filter',
            'TranscendsFactory',
            'FamiliesFactory',
            'SitioPurokFactory',
            'BusinessPermitFactories',
            'SurveyReportFactory',
            'SearchItemFactories',
		'ngUpload',
		'ngSanitize',
		'ui.select',
		'angularFileUpload',
		'ngCookies',
		'ngTagsInput',
		'checklist-model',
            'angular-loading-bar'
	]);

// get the hostname and port from the server
// ==========================================

var hostname 	= window.location.hostname;
var port 	      = window.location.port;
var api 	             = hostname + ':' + port;

//===========================================

app.constant('BrgyOfficialcollections', 'http://'+ api +'/brgyofficials/');
app.constant('SearchBaseurl', 'http://'+ api +'/search/');
app.constant('DbCollection', 'http://'+ api +'/api/');
app.constant('loginCollection', 'http://'+ api +'/users');
app.constant('Signupviewers', 'http://'+ api +'/viewer');
app.constant('BaranggayCollections', 'http://'+ api +'/brgycol');
app.constant('BusinessCollections', 'http://'+ api +'/businesspermits');
app.constant('RegionCollections', 'http://'+ api +'/getregions');
app.constant('HouseholdCollections', 'http://'+ api +'/houseroute');
app.constant('TranscendsCollections', 'http://'+ api +'/transcend');
app.constant('FamiliesCollections', 'http://'+ api +'/familiesroute');
app.constant('SitioPurokCollections', 'http://'+ api +'/sitiopurokroute');
app.constant('SurverReportCollections', 'http://'+ api +'/getdataresidentsitiopurok/');
app.constant('EstablishmentsCollections', 'http://'+ api +'/establishment/');


app.config(function($routeProvider, $locationProvider, cfpLoadingBarProvider,  RestangularProvider, DbCollection){
    // cfpLoadingBarProvider.includeSpinner = false;
    // cfpLoadingBarProvider.includeBar = false;
    // cfpLoadingBarProvider.latencyThreshold = 500;
    $locationProvider.html5Mode(true);

	$routeProvider

	.when('/login', {
		templateUrl: 'main.html',
		controller: 'navbarCtrl'
	})
	.when('/logout', {
		controller: 'logoutCtrl',
		resolve:{
			 auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})

	.when('/', {
		templateUrl: 'main.html',
		controller: 'navbarCtrl'
	})

	.when('/home', {
		templateUrl: 'main2.html',
		controller: 'residentCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}

	})
    // region portal
        .when('/regionPortal', {
        templateUrl: 'views/adminRegionPortal/adminregionportal.html',
        controller: 'adminRegionCtrl',
        resolve:{
                    auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                    }
                }

            })

// province portal

        .when('/adminprovinceresidentportal', {
        templateUrl: 'views/adminProvincePortal/provinceview.html',
        controller: 'adminProvinceCtrl',
        resolve:{
                    auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                    }
                }

            })

    // municipal portal

        .when('/adminmunicipalresident', {
        templateUrl: 'views/adminMunicipalPortal/adminMunicipalview.html',
        controller: 'adminMunicipalCtrl',
        resolve:{
                    auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                    }
                }

            })

            // manage household

                .when('/household', {
                    templateUrl: 'views/household/householdlist.html',
                    controller: 'houseHoldCtrl',
                    resolve:{
                        auth: function ($q, authenticationSvc) {
                                      var userInfo = authenticationSvc.getUserInfo();
                                      if (userInfo) {
                                          return $q.when(userInfo);
                                      } else {
                                          return $q.reject({ authenticated: false });
                                      }
                                  }
                    }
                })


            // manage families

                .when('/families', {
                    templateUrl: 'views/familiesportal/familylist.html',
                    controller: 'familiesCtrl',
                    resolve:{
                        auth: function ($q, authenticationSvc) {
                                      var userInfo = authenticationSvc.getUserInfo();
                                      if (userInfo) {
                                          return $q.when(userInfo);
                                      } else {
                                          return $q.reject({ authenticated: false });
                                      }
                                  }
                    }
                })

            // manage transcends

                .when('/transcends', {
                    templateUrl: 'views/transcends/transcendPortal.html',
                    controller: 'TranscendsCtrl',
                    resolve:{
                        auth: function ($q, authenticationSvc) {
                                      var userInfo = authenticationSvc.getUserInfo();
                                      if (userInfo) {
                                          return $q.when(userInfo);
                                      } else {
                                          return $q.reject({ authenticated: false });
                                      }
                                  }
                    }
                })

            // manage seventyninemonths

                .when('/seventyninemonths', {
                    templateUrl: 'views/seventyninemonth/seventynineMonth.html',
                    controller: 'SeventynineMonthCtrl',
                    resolve:{
                        auth: function ($q, authenticationSvc) {
                                      var userInfo = authenticationSvc.getUserInfo();
                                      if (userInfo) {
                                          return $q.when(userInfo);
                                      } else {
                                          return $q.reject({ authenticated: false });
                                      }
                                  }
                    }
                })


            // manage businessstablishment

                .when('/businessstablishment', {
                    templateUrl: 'views/businesspermit/list_establishment.html',
                    controller: 'businesspermitCtrl',
                    resolve:{
                        auth: function ($q, authenticationSvc) {
                                      var userInfo = authenticationSvc.getUserInfo();
                                      if (userInfo) {
                                          return $q.when(userInfo);
                                      } else {
                                          return $q.reject({ authenticated: false });
                                      }
                                  }
                    }
                })

            // manage citizen

            .when('/seniorcitizen', {
                templateUrl: 'views/seniorcitizen/citizenPortal.html',
                controller: 'seniorCitizenCtrl',
                resolve:{
                    auth: function ($q, authenticationSvc) {
                                  var userInfo = authenticationSvc.getUserInfo();
                                  if (userInfo) {
                                      return $q.when(userInfo);
                                  } else {
                                      return $q.reject({ authenticated: false });
                                  }
                              }
                }
            })

            // manage Sitio/Purok

            .when('/SitioPurok', {
                templateUrl: 'views/sitioPurok/sitioPuroklist.html',
                controller: 'sitioPurokCtrl',
                resolve:{
                    auth: function ($q, authenticationSvc) {
                                  var userInfo = authenticationSvc.getUserInfo();
                                  if (userInfo) {
                                      return $q.when(userInfo);
                                  } else {
                                      return $q.reject({ authenticated: false });
                                  }
                              }
                }
            })

            // Sitio/Purok Report Survey

            .when('/surveyreport', {
                templateUrl: 'views/surveyReport/surveyReport.html',
                controller: 'surveyReport',
                resolve:{
                    auth: function ($q, authenticationSvc) {
                                  var userInfo = authenticationSvc.getUserInfo();
                                  if (userInfo) {
                                      return $q.when(userInfo);
                                  } else {
                                      return $q.reject({ authenticated: false });
                                  }
                              }
                }
            })

            // Barangay Report Survey

            .when('/barangaysurvey', {
                templateUrl: 'views/barangayreport/barangayreport.html',
                controller: 'barangayreportCtrl',
                resolve:{
                    auth: function ($q, authenticationSvc) {
                                  var userInfo = authenticationSvc.getUserInfo();
                                  if (userInfo) {
                                      return $q.when(userInfo);
                                  } else {
                                      return $q.reject({ authenticated: false });
                                  }
                              }
                }
            })


            // manage Barangay Report Survey

            .when('/generalsurvey', {
                templateUrl: 'views/generalSurvey/generalSurvey.html',
                controller: 'generalSurveyCtrl',
                resolve:{
                    auth: function ($q, authenticationSvc) {
                                  var userInfo = authenticationSvc.getUserInfo();
                                  if (userInfo) {
                                      return $q.when(userInfo);
                                  } else {
                                      return $q.reject({ authenticated: false });
                                  }
                              }
                }
            })

            // manage regions

                .when('/region', {
                    templateUrl: 'views/region/regionlist.html',
                    controller: 'regionCtrl',
                    resolve:{
                        auth: function ($q, authenticationSvc) {
                                      var userInfo = authenticationSvc.getUserInfo();
                                      if (userInfo) {
                                          return $q.when(userInfo);
                                      } else {
                                          return $q.reject({ authenticated: false });
                                      }
                                  }
                    }
                })

	.when('/region/create', {
		templateUrl: 'views/region/region.html',
		controller: 'addregionCtrl',
		resolve:{
			 auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})

	.when('/region/:id', {
		templateUrl: 'views/region/region.html',
		controller: 'editregionCtrl',
		    resolve: {
	          Region: function(Restangular, $route){
	            return Restangular.one('region', $route.current.params.id).get();
	          }
	        }
	})


            .when('/addhouseholdmember/:id', {
                templateUrl: 'views/addmember/addmember.html',
                controller: 'addmemberCtrl',
                    resolve: {

                    getresidenthousehead: function($http, $route){

                          return $http.get(DbCollection + '/resident/'+ $route.current.params.id);

                      },
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                            if (userInfo) {
                                return $q.when(userInfo);
                            } else {
                                return $q.reject({ authenticated: false });
                            }
                        }
                    }

            })


// end of regions

// ===========================================

// manage provinces

	.when('/viewprovinces/:id', {
		templateUrl: 'views/region/viewprovince.html',
		controller: 'editregionCtrl',
		    resolve: {
	          Region: function(Restangular, $route){
	            return Restangular.one('region', $route.current.params.id).get();
	          },
                    getRegionprovince: function(Restangular, $route, $http){
                        return $http.get(DbCollection+'/province/'+$route.current.params.id);
                      },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})

	.when('/region/createprovince/:id', {
		templateUrl: 'views/region/province.html',
		controller: 'editregionCtrl',
		    resolve: {
	          Region: function(Restangular, $route){
	            return Restangular.one('region', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})

	.when('/viewmunicipality/:id', {
		templateUrl: 'views/region/municipalitylist.html',
		controller: 'provinceCtrl',
		    resolve: {
	          Province: function(Restangular, $route, $http){
	            return $http.get(DbCollection+'getprovince/'+ $route.current.params.id);
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})
// end of provinces

// ==================================

// manage barangay
   .when('/viewbrgydata/:id', {
		templateUrl: 'views/region/brgylist.html',
		controller: 'barangayCtrl',
		    resolve: {
	          Municipality: function(Restangular, $route, $http){
	            return $http.get(DbCollection+'/getmunicipality/'+ $route.current.params.id);
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
	            }
	        }
	})

   .when('/addbrgydata/:id', {
		templateUrl: 'views/region/brgy.html',
		controller: 'addbarangayCtrl',
		    resolve: {
	          getMunicipality: function(Restangular, $route, $http){
	            return $http.get(DbCollection+'/getmunicipality/'+ $route.current.params.id);
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})
// manage barangay

// =================================

// manage user
	.when('/user', {
		templateUrl: 'views/accountlist.html',
		controller: 'registerCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})


	.when('/user/create', {
		templateUrl: 'views/register.html',
		controller: 'addAccountCtrl',
		resolve: {
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})


	.when('/user/:id', {
		templateUrl: 'views/register.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
	        }
	})

// manage user
//===========================
	.when('/myaccount/:id', {
		templateUrl: 'views/editaccount.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})


	.when('/regionadmin/:id', {
		templateUrl: 'views/regionAccount.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})


	.when('/useraccount/:id', {
		templateUrl: 'views/edituseraccount.html',
		controller: 'editaccountCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('registered', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
	            }
	        }
	})


	.when('/clearance', {
		templateUrl: 'views/clearancelist.html',
		controller: 'clearanceCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})


	.when('/clearance/create', {
		templateUrl: 'views/clearance.html',
		controller: 'addclearanceCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})
	.when('/clearance/:id', {
		templateUrl: 'views/clearance.html',
		controller: 'editclearanceCtrl',
		    resolve: {
	          Clearance: function(Restangular, $route){
	            return Restangular.one('clearance', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})
	.when('/viewclearance/:id', {
		templateUrl: 'views/viewclearance.html',
		controller: 'editclearanceCtrl',
		    resolve: {
	          Clearance: function(Restangular, $route){
	            return Restangular.one('clearance', $route.current.params.id).get();
	          },
	           auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})

	.when('/resident', {
		templateUrl: 'views/residentlist.html',
		controller: 'residentCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})

	.when('/viewresident/:id', {
		templateUrl: 'views/viewresident.html',
		controller: 'viewresidentCtrl',
		 resolve: {
	          getbrgyresident: function(Restangular, $route){
	            return Restangular.one('resident', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})

	.when('/judicial', {
		templateUrl: 'views/judicialist.html',
		controller: 'judicialCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})
	.when('/judicial/create', {
		templateUrl: 'views/judicial.html',
		controller: 'addjudicialCtrl',
		resolve:{
			auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
            }
		}
	})
	.when('/judicial/:id', {
		templateUrl: 'views/judicial.html',
		controller: 'editjudicialCtrl',
		resolve: {
	          Judicial: function(Restangular, $route){
	            return Restangular.one('judicial', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
	                var userInfo = authenticationSvc.getUserInfo();
	                if (userInfo) {
	                    return $q.when(userInfo);
	                } else {
	                    return $q.reject({ authenticated: false });
	                }
            	}
	        }
	})
	.when('/viewjudicial/:id', {
		templateUrl: 'views/viewjudicial.html',
		controller: 'editjudicialCtrl',
		resolve: {
	          Judicial: function(Restangular, $route){
	            return Restangular.one('judicial', $route.current.params.id).get();
	          },
	          auth: function ($q, authenticationSvc) {
                var userInfo = authenticationSvc.getUserInfo();
                if (userInfo) {
                    return $q.when(userInfo);
                } else {
                    return $q.reject({ authenticated: false });
                }
             }
	        }
	})

            .when('/official-account', {
                templateUrl: 'views/officials/officialsaccountlist.html',
                controller: 'OfficialsaccountCtrl',
                resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                    }
            })

            .when('/viewreport/:id', {
                templateUrl: 'views/reports/viewreport.html',
                controller: 'reportCtrl',
                resolve: {
                      getreport: function(ReportService, $route){
                        return ReportService.GetReport($route.current.params.id);
                      },
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                    }
            })
            .when('/generateIDs', {
                templateUrl: 'views/generateIDs/generateIds.html',
                controller: 'genrateIDsCtrl',
                 resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                    }
            })

            .when('/businessOwner', {
                templateUrl : 'views/businesspermit/businessOwner.html',
                controller: 'businesspermitCtrl',
                resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                }
            })

            .when('/officialsaccountlist', {
                templateUrl : 'views/officials/resident_officials.html',
                controller: 'OfficialsaccountCtrl',
                resolve: {
                      auth: function ($q, authenticationSvc) {
                        var userInfo = authenticationSvc.getUserInfo();
                        if (userInfo) {
                            return $q.when(userInfo);
                        } else {
                            return $q.reject({ authenticated: false });
                        }
                     }
                }
            })

	.otherwise({ redirectTo: '/login' });


	RestangularProvider.setBaseUrl(DbCollection);
      RestangularProvider.setRestangularFields({
        id: '_id'
      });

      RestangularProvider.setRequestInterceptor(function(elem, operation, what) {

        if (operation === 'put') {
          elem._id = undefined;
          return elem;
        }
        return elem;
      });

});

app.run(function($rootScope, $http, $location, loginCollection, DbCollection) {
  // Needed for the loading screen
  $rootScope.$on('$routeChangeStart', function(){
    $(window).load(function(){
      $('.loading-pane .loading-container').fadeOut(function(){
        $('.loading-pane').hide();
        $("#hideload").show();
        $("#hideloadover").show();
      });
    });
  });

    $rootScope.$on("$routeChangeSuccess", function (userInfo) {
    });

    $rootScope.$on("$routeChangeError", function (event, current, previous, eventObj) {
        if (eventObj.authenticated === false) {
        	$rootScope.errornotoken = 'Your access token has been deleted. Please login your account.';
            $location.url("/login");
        }
        // check if the UserAcount is exist, if not clear the sessionStorage
		   $http.get(DbCollection + 'account/')
		    .then(function(result){
		      $rootScope.UserAcount = result.data;
		      if($rootScope.UserAcount == null){
		        sessionStorage.clear();
		        $location.path('/login');
		        // window.location.reload();
		      }
		    });
    });
});

