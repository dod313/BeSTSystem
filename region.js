var express                 = require('express'),
    bodyparser              = require('body-parser'),
    mongo                   = require('./dbconnection/dbconnect'),
    db                          = mongo.dbbestconnect();
    cors                        = require('cors'),
    session                   = require('express-session'),
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid         = require('shortid');

//=========================================================================
//Region COLLECTION
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/viewregion')
    .get(function (req, res){
        db.collection('region').find().toArray(function (err, data){
           return res.json(data);
        });
    })

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/viewregion/:id')
    .get(function (req, res){
        db.collection('region').findById(req.params.id, function (err, data){
            return res.json(data);
        });
    })

//=========================================================================
//  Region list of  ResidentCOLLECTION
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/view_resident_region/:id')
    .get(function (req, res){
        db.collection('brgyresident').find({'region._id': req.params.id}).toArray(function (err, data){

            if(data == null){
                res.send(301, {
                   message: 'No Records found.'});
            }else{
                    return res.json(data);
            }

        })
    })

//=========================================================================
//  Municipal list of  Resident COLLECTION with the given municipal id
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/view_resident_municipal/:id')
    .get(function (req, res){
        db.collection('brgyresident').find({'municipality._id': req.params.id}).toArray(function (err, data){

            if(data == null){
                res.send(301, {
                   message: 'No Records found.'});
            }else{
                    return res.json(data);
            }

        })
    })

//=========================================================================
//  Municipal list of  Resident COLLECTION with the given barangay id
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/view_resident_barangay/:id')
    .get(function (req, res){
        db.collection('brgyresident').find({'barangay._id': req.params.id}).toArray(function (err, data){

            if(data == null){
                res.send(301, {
                   message: 'No Records found.'});
            }else{
                    return res.json(data);
            }

        })
    })

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/view_provinces_region/:id')
    .get(function (req, res){
        db.collection('province').find({regionId: req.params.id}).toArray(function (err, data){
        return res.json(data);

        })
    })

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/view_residents_province/:id')
    .get(function (req, res){
        db.collection('brgyresident').find({"province._id": req.params.id}).toArray(function (err, data){
        return res.json(data);

        })
    })

module.exports = router;