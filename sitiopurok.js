var express                 = require('express'),
    mongo                   = require('./dbconnection/dbconnect'),
    db                          = mongo.dbbestconnect();
    bodyparser              = require('body-parser'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid                     = require('shortid');


    router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/sitiopurokroutes')
    .get(function (req, res){
        db.collection('sitiopurok').find({usercurrentId : req.user}).toArray(function (err, data){
            return res.json(data);
        });
    })
    .post(function (req, res) {
    var sitioPurok          = req.body;
    var shortID                 = shortid.generate();
    sitioPurok.shorid         = shortID;
    sitioPurok.usercurrentId = req.user;
    sitioPurok.barangay = req.barangay;
    sitioPurok.brgyId =  req.barangay._id;
    var today = new Date();
    sitioPurok.Datecreated = today;

        db.collection('sitiopurok').insert(sitioPurok, function (err, data){
            return res.json(data);
        });

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/sitiopurokroutes/:id')
    .get(function (req, res){
        db.collection('sitiopurok').findById(req.params.id, function(err, data){
            return res.json(data);
        });
    })
    .put(function (req, res){
            var sitioPurok          = req.body;
            var shortID                 = shortid.generate();
            sitioPurok.shorid         = shortID;
            sitioPurok.usercurrentId = req.user;
            sitioPurok.barangay = req.barangay;
            sitioPurok.brgyId =  req.barangay._id;
            var today = new Date();
            sitioPurok.Datecreated = today;

            delete sitioPurok._id;
                db.collection('sitiopurok').updateById(req.params.id, sitioPurok, function (err, data){
                    res.json(data);
                });
    })
    .delete(function (req, res){
        db.collection('sitiopurok').removeById(req.params.id, function(){
            res.json(null);
        });
    });

module.exports = router;