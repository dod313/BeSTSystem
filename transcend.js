var express                 = require('express'),
    bodyparser              = require('body-parser'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    mongo             = require('./dbconnection/dbconnect'),
    db                    = mongo.dbbestconnect();
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid                     = require('shortid');

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/transcend')
    .get(function (req, res){
        db.collection('transcend').find({usercurrentId: req.user}).toArray(function (err, data){
            return res.json(data);
        });
    })
    .post(function (req, res) {
    var transcend          = req.body;
    var shortID                 = shortid.generate();
    transcend.shorid         = shortID;
    transcend.usercurrentId = req.user;
    transcend.barangay = req.barangay;
    var fname = req.body.prof_firstname;
    var mname = req.body.prof_middle_name;
    var lname = req.body.prof_lastname;

    var stringsitio                     = req.body.sitiopurok;
    var Interviewer                     = req.body.Interviewer;

    if(stringsitio === undefined || Interviewer === undefined){
        return false;
    }

    var today  = new Date();
    var returnId                    = stringsitio.split(',');
    var returnInterviewerid  = Interviewer.split(',');
    transcend.sitioId           = returnId[0];
    transcend.Interviewerid  = returnInterviewerid[0];
    transcend.dateCreated= today;

      transcend.fullname = fname+' '+mname+' '+lname;
      transcend.dateCreated = today;

        db.collection('transcend').insert(transcend, function (err, data){
            return res.json(data);
        });

    });

router
    .use(cors())
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/gettransientbypage/:page_no')

    .get(function (req, res, next){
        var pageLimit = 10;
        var skipValue = req.params.page_no*pageLimit;

        db.collection('transcend').find({usercurrentId: req.user}, null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}})

        .toArray(function(err, data) {
            if (err) {
                return res.send(400, {
                    message: getErrorMessage(err)
                });
            } else {
                return res.json(data);
            }
        });

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/transcend/:id')
    .get(function (req, res){
        db.collection('transcend').findById(req.params.id, function(err, data){
            return res.json(data);
        });
    })
    .put(function (req, res){
            var transcend = req.body;
            transcend.usercurrentId = req.user;
            transcend.barangay = req.barangay;
            var fname = req.body.prof_firstname;
            var mname = req.body.prof_middle_name;
            var lname = req.body.prof_lastname;

            var stringsitio                     = req.body.sitiopurok;
            var Interviewer                     = req.body.Interviewer;

            if(stringsitio === undefined || Interviewer === undefined){
                return false;
            }

            var today  = new Date();
            var returnId                    = stringsitio.split(',');
            var returnInterviewerid  = Interviewer.split(',');
            transcend.sitioId           = returnId[0];
            transcend.Interviewerid  = returnInterviewerid[0];
            transcend.dateUpdated= today;

              transcend.fullname = fname+' '+mname+' '+lname;
              transcend.dateUpdated = today;

            delete transcend._id;

      transcend.fullname = fname+' '+mname+' '+lname;
      transcend.datecreated = today;
                db.collection('transcend').updateById(req.params.id, transcend, function (err, data){
                    res.json(data);
                });
    })
    .delete(function (req, res){
        db.collection('transcend').removeById(req.params.id, function(){
            res.json(null);
        });
    });
module.exports = router;